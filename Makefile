#
#	D6200 serial product makefile 
#	Usage:  make kernel source bootcode
#
#NOTE: do "make all_clean" if need change ANNEX after compiled

ANNEX=A
#ANNEX=B

ifeq "$(ANNEX)" "B"
PROJECT_NAME=D6200B
else
PROJECT_NAME=D6200
endif

CUR_PATH = $(shell pwd)
RULES=$(CUR_PATH)/Source/Rules.mak

all:
	@echo 'make <kernel/source>';

prepare: creat_rules Kernel/bcm963xx 

kernel:
	@echo "PROFILE=96362GW" > Kernel/bcm963xx/PROFILE.make
ifeq "$(ANNEX)" "B"
	cp -f Kernel/bcm963xx/targets/96362GW/96362BGW Kernel/bcm963xx/targets/96362GW/96362GW
else
	cp -f Kernel/bcm963xx/targets/96362GW/96362AGW Kernel/bcm963xx/targets/96362GW/96362GW	
endif
	@cd Kernel/bcm963xx/; make sc_kernel;

source: prepare
#	if [ -h /opt/toolchains/uclibc-crosstools ]; then\
#		rm -rf /opt/toolchains/uclibc-crosstools;\
#	fi
#	ln -sf uclibc-crosstools-gcc-4.4.2-1/usr/ /opt/toolchains/uclibc-crosstools;
	make -C Source -s

source_clean:
	make -C Source clean_all

kernel_clean:
	@echo "PROFILE=96362GW" > Kernel/bcm963xx/PROFILE.make
	@cd Kernel/bcm963xx/; make sc_clean ;

all_clean: kernel_clean source_clean

creat_rules:
	@echo "PROJECT=$(PROJECT_NAME)"
	@echo "PROJECT=$(PROJECT_NAME)" > $(RULES)
	@echo "export ROOT=$(CUR_PATH)/Source/" >> $(RULES)
	@cat Source/rules.src >> $(RULES)
