
#ifndef SC_BR_IGMP_H
#define SC_BR_IGMP_H

#include <linux/netdevice.h>
#include <linux/if_bridge.h>
#include <linux/igmp.h>
#include <linux/in.h>
#include "br_private.h"


#define BRCTL_ENABLE_SNOOPING 21
#define BRCTL_ENABLE_PROXY_MODE 22
#define BRCTL_SET_PORT_SNOOPING 23
#define BRCTL_SHOW_SNOOPING 24
#define BRCTL_CLEAR_PORT_SNOOPING 25
#define SIOCBRSHOWSNOOPING 24

#define SNOOPING_BLOCKING_MODE 2


union ip_array {
	unsigned int ip_addr;
        unsigned char ip_ar[4];
};

#define TIMER_CHECK_TIMEOUT 10
#define QUERY_TIMEOUT 130
#define IGMPV3_GRP_REC_SIZE(x)  (sizeof(struct igmpv3_grec) + \
                       (sizeof(struct in_addr) * ((struct igmpv3_grec *)x)->grec_nsrcs))
/* these definisions are also there igmprt.h */
#define SNOOP_IN_ADD		1
#define SNOOP_IN_CLEAR		2
#define SNOOP_EX_ADD		3
#define SNOOP_EX_CLEAR		4

/* define for filter port , leon*/
#define MAX_IF_NUM              16 /* 4 lan port, 4 wlan port, 8 pvc */
#define NAS_START_BIT           18

struct filter
{
	/* save the src_info for filter */
	int len;
	u32 src_info[MAX_IF_NUM];
};

struct net_bridge_mc_src_entry
{
	struct in_addr		src;
	unsigned long		tstamp;
        int			filt_mode;
};

struct net_bridge_mc_fdb_entry
{
	struct net_bridge_port		*dst;
	mac_addr			addr;
	mac_addr			host;
	struct net_bridge_mc_src_entry  src_entry;
	unsigned char			is_local;
	unsigned char			is_static;
	unsigned long			tstamp;
	struct list_head 		list;
	struct rcu_head			rcu;
};

extern int snooping;
extern int mc_forward(struct net_bridge *br, struct sk_buff *skb, const unsigned char *dest,int forward, int clone);
extern int br_mc_fdb_add(struct net_bridge *br, struct net_bridge_port *prt, const unsigned char *dest, unsigned char *host, int mode, struct in_addr *src);
extern void br_mc_fdb_cleanup(struct net_bridge *br);
extern void br_mc_fdb_remove_grp(struct net_bridge *br, struct net_bridge_port *prt, const unsigned char *dest);
extern int br_mc_fdb_remove(struct net_bridge *br, struct net_bridge_port *prt, const unsigned char *dest, unsigned char *host, int mode, struct in_addr *src);
extern struct net_bridge_mc_fdb_entry *br_mc_fdb_find(struct net_bridge *br, 
                                               struct net_bridge_port *prt, 
                                               const unsigned char *dest, 
                                               unsigned char *host, 
                                               struct in_addr *src);

#endif
