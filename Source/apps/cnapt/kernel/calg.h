
#ifndef __CALG_H_
#define __CALG_H_

struct calg_t {
	struct list_head list;
	atomic_t refcnt;
	struct cnapt_mapping_t mapping;
};

#endif
