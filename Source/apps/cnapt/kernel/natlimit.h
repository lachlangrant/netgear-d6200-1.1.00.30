
#ifndef __NATLIMIT_H_
#define __NATLIMIT_H_

#include <net/netfilter/nf_nat.h>

struct host_natlimit {
	struct list_head list;
	struct list_head sess_head;
	__be32 addr;
	atomic_t count;
};

struct port_bmp_t {
	struct port_bmp_t *next;
	int id;
	u_int32_t bitmap;
};

#define SESS_TIMEOUT  (600 * HZ)
#define SESS_LIMIT	  50

static inline struct nf_conn_natlimit* nfct_natlimit(struct nf_conn *ct)
{
	struct nf_conn_nat *ct_nat = nfct_nat(ct);
	return (ct_nat ? &ct_nat->ct_natlimit : NULL);
}

static inline struct nf_conn* natlimit_nfct(struct nf_conn_natlimit *natlimit)
{
	return natlimit->ct;
}

#endif
