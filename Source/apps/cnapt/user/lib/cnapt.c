
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <strings.h>

#include "cnapt.h"
#include "common.h"

void cnapt_flush_bypriv(char *lan_ip, char *lan_mask)
{
	struct cui_cmd_t *uc;
	struct cnapt_flush_args_t *fa;
	int alloc_size;
	
	alloc_size = sizeof(struct cui_cmd_t) + sizeof(struct cnapt_flush_args_t);
	uc = malloc(alloc_size);
	if (!uc)
		return ;
	memset(uc, 0, alloc_size);
	uc->cmd = CUI_CNAPT_FLUSH;
	fa = (struct cnapt_flush_args_t*)uc->data;
	if (parse_ip(lan_ip, &fa->lan_ip) < 0 
		|| parse_ip(lan_mask, &fa->lan_mask) < 0)
		goto RET;
	commit_cmd(uc, alloc_size);
RET:
	free(uc);
	return ;
}
