
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "common.h"
#include "cdmz.h"

int cdmz_clean(int enable, const char *dmz_ip)
{
	struct cui_cmd_t *uc;
	struct cdmz_t *cdmz;
	int alloc_size;
	struct in_addr addr;
	int ret;

	alloc_size = sizeof(struct cui_cmd_t) + sizeof(struct cdmz_t);
	uc = malloc(alloc_size);
	if (!uc)
		return -ENOMEM;
	cdmz = (struct cdmz_t*)uc->data;
	cdmz->enable = enable;
	if (inet_pton(AF_INET, dmz_ip, &addr) != 1) {
		ret = -EINVAL;
		goto out_free;
	}
	cdmz->server = addr.s_addr;
	uc->cmd = CUI_DMZ_CLEAN;
	ret = commit_cmd(uc, alloc_size);
out_free:
	free(uc);
	return ret;
}
