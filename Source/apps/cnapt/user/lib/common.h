
#ifndef __COMMON_H
#define __COMMON_H

#include "cui.h"

#define seep(fmt, args...)  do { \
	FILE *fp = fopen("/dev/console", "w"); \
	if (fp) { \
		fprintf(fp, "[ %s - %d ]: ", __FUNCTION__, __LINE__); \
		fprintf(fp, fmt, ## args); \
		fflush(fp); \
		fclose(fp); \
	} \
} while (0)

extern int commit_cmd(struct cui_cmd_t *uc, int size);

extern int parse_ip(char *ipstr, u_int32_t *ipaddr);

#endif
