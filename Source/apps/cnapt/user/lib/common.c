
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <errno.h>

#include "cui.h"

int commit_cmd(struct cui_cmd_t *uc, int size)
{
	int fd;
	int ret;

	fd = open("/proc/cnapt_serv", O_WRONLY);
	if (fd < 0) {
		return -EFAULT;
	}
	ret = write(fd, uc, size);
	close(fd);
	return (ret == size) ? 0 : (ret == -1) ? -errno : ret;
}

int parse_ip(char *ipstr, u_int32_t *ipaddr)
{
	struct in_addr addr;

	if (*ipstr == 'a')
		*ipaddr = 0;
	else if (inet_pton(AF_INET, ipstr, &addr) == 1)
		*ipaddr = addr.s_addr;
	else
		return -1;
	return 0;
}
