/* Shared library add-on to iptables to add masquerade support. */
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <xtables.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sc/cnapt/ipt_CDMZ.h>

/* Function which prints out usage message. */
static void
help(void)
{
	printf("CDMZ options:\n");
}

static struct option opts[] = {
	{ "to", 1, 0, '1' },
	{ 0 }
};

/* Initialize the target. */
static void
init(struct xt_entry_target *t)
{
}


/* Function which parses command options; returns true if it
   ate an option */
static int parse(int c, char **argv, int invert, unsigned int *flags,
		     const void *entry,
		     struct xt_entry_target **target)
{
	struct ipt_cdmz_info_t *info = (struct ipt_cdmz_info_t *)(*target)->data;

	switch (c) {
	case '1':
		info->to = inet_network(optarg);
		if (info->to == -1)
			return 0;
		break;
	default:
		return 0;
	}
	return 1;
}

/* Final check; don't care. */
static void final_check(unsigned int flags)
{
}

/* Prints out the targinfo. */
static void
print(const void *ip,
		      const struct xt_entry_target *target, int numeric)
{
	struct ipt_cdmz_info_t *info = (struct ipt_cdmz_info_t *)target->data;
	struct in_addr addr;

	addr.s_addr = info->to;
	printf("to:%s", inet_ntoa(addr));
}

/* Saves the union ipt_targinfo in parsable form to stdout. */
static void
save(const void *ip,
		 const struct xt_entry_target *target)
{
	struct ipt_cdmz_info_t *info = (struct ipt_cdmz_info_t *)target->data;
	struct in_addr addr;

	addr.s_addr = info->to;
	printf("--to %s", inet_ntoa(addr));
}

static struct xtables_target ipt_CDMZ = { NULL,
	.name		= "CDMZ",
	.version	= XTABLES_VERSION,
	.size		= XT_ALIGN(sizeof(struct ipt_cdmz_info_t)),
	.userspacesize	= XT_ALIGN(sizeof(struct ipt_cdmz_info_t)),
	.help		= &help,
	.init		= &init,
	.parse		= &parse,
	.final_check	= &final_check,
	.print		= &print,
	.save		= &save,
	.extra_opts	= opts
};

void _init(void)
{
	xtables_register_target(&ipt_CDMZ);
}
