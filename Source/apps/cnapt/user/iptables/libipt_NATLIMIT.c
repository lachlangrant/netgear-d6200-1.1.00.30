/* Shared library add-on to iptables to add masquerade support. */
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <linux/types.h>
#include <xtables.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sc/cnapt/ipt_NATLIMIT.h>

static int get_net_prefix(unsigned long ulmask);

static int get_net_prefix(unsigned long ulmask)
{
	int prefix_len;

	ulmask = ntohl(ulmask);
	
	for( prefix_len =0; prefix_len < 32 ; prefix_len ++)
	{
		if( ! (ulmask << prefix_len) )
		        return prefix_len;
	}
	
	return prefix_len;
}

/* Function which prints out usage message. */
static void
help(void)
{
	printf("NATLIMIT options:\n");
}

static struct option opts[] = {
	{"lan", 1, 0, '1'},
	{"dev", 1, 0, '2'},
	{ 0 }
};

/* Initialize the target. */
static void
init(struct xt_entry_target *t)
{
}

/* Function which parses command options; returns true if it
   ate an option */
static int parse(int c, char **argv, int invert, unsigned int *flags,
		     const void *entry,
		     struct xt_entry_target **target)
{
	struct ipt_natlimit_info_t *info = (struct ipt_natlimit_info_t *)(*target)->data;
	char *prefix;
	int  len;
	u_int32_t mask = 0xffffffff;
	char buffer[64];

	switch (c) {
	case '1':
		snprintf(buffer, sizeof(buffer), optarg);
		
		prefix = strchr(buffer, '/');
		if (!prefix) {
			printf("error prefix\n");
			return 0;
		}
		*prefix++ = '\0';
		info->lan_addr = inet_network(buffer);
		if (info->lan_addr == -1) {
			printf("error lan_addr\n");
			return 0;
		}
		len = atoi(prefix);
		if (len > 32)
			return 0;
		mask <<= (32-len);
		info->lan_mask = htonl(mask);
		return 1;
	case '2':
	    if (strlen(optarg) > sizeof(info->dev)-1) {
	        printf("device name is too long\n");
	        return 0;
	    }
	    strcpy(info->dev, optarg);
	    return 1;
	default:
		return 0;
	}
	return 1;
}

/* Final check; don't care. */
static void final_check(unsigned int flags)
{
}

/* Prints out the targinfo. */
static void print(const void *ip,
		      const struct xt_entry_target *target, int numeric)
{
	struct ipt_natlimit_info_t *info = (struct ipt_natlimit_info_t *)target->data;
	struct in_addr addr;

	addr.s_addr = info->lan_addr;
	
	printf("lan:%s/%d ", inet_ntoa(addr), get_net_prefix(info->lan_mask));
	printf("dev:%s ", info->dev);
}

/* Saves the union ipt_targinfo in parsable form to stdout. */
static void save(const void *ip,
		     const struct xt_entry_target *target)
{
	struct ipt_natlimit_info_t *info = (struct ipt_natlimit_info_t *)target->data;
	struct in_addr addr;

	addr.s_addr = info->lan_addr;
	
	printf("--lan %s/%d ", inet_ntoa(addr), get_net_prefix(info->lan_mask));
	printf("--dev %s ", info->dev);
}

static struct xtables_target ipt_NATLIMIT = { NULL,
	.name		= "NATLIMIT",
	.version	= XTABLES_VERSION,
	.size		= XT_ALIGN(sizeof(struct ipt_natlimit_info_t)),
	.userspacesize	= XT_ALIGN(sizeof(struct ipt_natlimit_info_t)),
	.help		= &help,
	.init		= &init,
	.parse		= &parse,
	.final_check	= &final_check,
	.print		= &print,
	.save		= &save,
	.extra_opts	= opts
};

void _init(void)
{
	xtables_register_target(&ipt_NATLIMIT);
}
