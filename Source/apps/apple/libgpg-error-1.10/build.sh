#!/bin/bash

build_dir=build_sc
cd $build_dir
CC=/opt/toolchains/uclibc-crosstools-gcc-4.4.2-1-with-ftw/usr/bin/mips-linux-uclibc-gcc ../configure --host=mips-linux-uclibc --build=i486-linux-gnu prefix=/usr --enable-shared --enable-static --disable-rpath
