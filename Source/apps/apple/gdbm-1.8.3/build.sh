#!/bin/bash
build_dir=build_sc
cd $build_dir
CC=/opt/toolchains/uclibc-crosstools-gcc-4.4.2-1-with-ftw/usr/bin/mips-linux-uclibc-gcc ../configure --target=mips-linux-uclibc --host=mips-linux --build=i486-linux-gnu --enable-shared --enable-static
echo "INSTALL_ROOT := \$(DESTDIR)" >> Makefile
echo "BINGRP := `id -u`" >> Makefile
echo "BINOWN := `id -g`" >> Makefile
