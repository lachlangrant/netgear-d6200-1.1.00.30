/****************************************************************************
 * Copyright (C) 1998 WIDE Project. All rights reserved.
 * Copyright (C) 1999,2000,2001,2002 University of Tromso. All rights reserved.
 * Copyright (C) 2002 Invenia Innovation AS. All rights reserved.
 *
 * Author: Feike W. Dillema, feico@pasta.cs.uit.no.
 *         based on newbie code by Yusuke DOI, Keio Univ. Murai Lab.
 ****************************************************************************/

/*
 * <$Id: totd.h,v 3.37 2003/09/17 11:27:42 dillema Exp $>
 */

#ifndef TOT_H
#define TOT_H

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include <assert.h>
#include <netdb.h>
#include <sys/time.h>
#ifdef Darwin
#include <stdint.h>
#endif
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_FILIO_H
#include <sys/filio.h>
#endif
#ifdef HAVE_SYS_SOCKIO_H
#include <sys/sockio.h>
#endif
#include <sys/termios.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <limits.h>
#include <setjmp.h>
#include <ctype.h>
#include <pwd.h>
#include <grp.h>

#ifdef TIME_WITH_SYS_TIME
#include <time.h>
#endif

#ifdef __FreeBSD__
#include <sys/param.h>
#define socklen_t int
#endif

#ifdef DBMALLOC
#include <malloc.h>
#endif

#include "macros.h"
#include "tot_constants.h"
#include "tot_types.h"

/*
 * TOTD global config and state variables
 */
struct ToT {
	/* filenames of config and pid file */
	char *configfile;
	char *pidfile;
	/* when we do not run as root all the time, we run as: */
	char *user;		/* username */
	char *group;		/* groupname */
	char *rootdir;		/* dir to chroot to */
	uid_t uid;		/* user id  */
	gid_t gid;		/* group id */
	/* numeric values */
	int quiet; 		/* how much to (sys-)log */
	int debug; 		/* debugging level, 0 == no debugging output */
	int port;		/* the port we listen at for requests */
	int retry_interval;	/* how long to wait til retry forwarder */
	/* boolean flags */
	int ip4;		/* do we do IPv4 */
	int ip6;		/* do we do IPv6 */
#ifdef IP6_ONLY
	int ipv6_only;  /* make lan iface socket support ipv4 or not */
#endif
	int use_mapped;		/* do we do IPv4 over IPv6-AF socket */
	int wildcard;		/* do we open wildcard (UDP) socket or one per address? */
	int stf;		/* 6to4 reverse lookup support enable/disable */
	int tcp;		/* TCP fallover from UDP enable/disable */
	int rescan_iflist;	/* do we rescan for new addresses/interfaces??? */
	/* list of prefixes to cycle through (round-robin) */
	int prefixnum;		/* number of configured prefixes */
	u_char prefix[MAXPREFIXES][TOTPREFIXLEN + 1];
	/* list of configured forwarders (`normal' recursive nameservers) */
	G_List *Fwd_list;
	/* current state */
	G_List *current_fwd;		/* nameserver we currently forward our requests to */
	int current_prefix;		/* index into tot_prefix above */
	char *iflist[MAXINTERFACES+1];	/* null terminated list of interface names */
#ifdef SCOPED_REWRITE
	/* list of configured scoped prefixes */
	int scoped_prefixes;
	struct in6_addr scoped_from[MAXPREFIXES];
	struct in6_addr scoped_to[MAXPREFIXES];
	int scoped_plen[MAXPREFIXES];
#endif
	G_List *dnshj_list;			/* dhshj list */
	u_char MSG_Buf[MAX_STREAM];	/* Buffer for TCP/UDP messages */
};

extern struct ToT T;

#include "protos.h"

#ifdef IP6_ONLY
#define IPV6_V6ONLY		26
#endif

#endif				/* TOT_H */
