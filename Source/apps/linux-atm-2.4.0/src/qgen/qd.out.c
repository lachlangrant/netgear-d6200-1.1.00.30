/* THIS IS A MACHINE-GENERATED FILE. DO NOT EDIT ! */

#if HAVE_CONFIG_H
#include <config.h>
#endif

/* (optional) user includes go here */

#include "uni.h"
#include "atmsap.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#include "common.h"
#include "op.h"
#include "qd.out.h"
#include "qlib.h"



static void q_put(unsigned char *table,int pos,int size,unsigned long value);

static unsigned char q_initial[Q_DATA_BYTES];

/*
 * Initialization of constant data. Could also do this in
 * the translator and output the resulting byte stream.
 */

static void q_init_global(void)
{
    memset(q_initial,0,sizeof(q_initial));
    q_put(q_initial,0,8,Q2931_PROTO_DSC); /* _pdsc */
    q_put(q_initial,8,8,3); /* _cr_len */
    q_put(q_initial,55,1,1); /* _ext */
    q_put(q_initial,52,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,48,2,0); /* _action_ind */
    q_put(q_initial,72,8,ATM_IE_AAL); /* _ie_id */
    q_put(q_initial,87,1,1); /* _ext */
    q_put(q_initial,85,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,84,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,80,3,0); /* _action_ind */
    q_put(q_initial,112,8,ATM_AALP_FW_MAX_SDU); /* _id */
    q_put(q_initial,136,8,ATM_AALP_BW_MAX_SDU); /* _id */
    q_put(q_initial,160,8,ATM_AALP_AAL_MODE); /* _id */
    q_put(q_initial,176,8,ATM_AALP_SSCS); /* _id */
    q_put(q_initial,192,8,ATM_IE_TD); /* _ie_id */
    q_put(q_initial,207,1,1); /* _ext */
    q_put(q_initial,205,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,204,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,200,3,0); /* _action_ind */
    q_put(q_initial,224,8,ATM_TD_FW_PCR_0); /* _id */
    q_put(q_initial,256,8,ATM_TD_BW_PCR_0); /* _id */
    q_put(q_initial,288,8,ATM_TD_FW_PCR_01); /* _id */
    q_put(q_initial,320,8,ATM_TD_BW_PCR_01); /* _id */
    q_put(q_initial,352,8,ATM_TD_FW_SCR_0); /* _id */
    q_put(q_initial,384,8,ATM_TD_BW_SCR_0); /* _id */
    q_put(q_initial,416,8,ATM_TD_FW_SCR_01); /* _id */
    q_put(q_initial,448,8,ATM_TD_BW_SCR_01); /* _id */
    q_put(q_initial,480,8,ATM_TD_FW_MBS_0); /* _id */
    q_put(q_initial,512,8,ATM_TD_BW_MBS_0); /* _id */
    q_put(q_initial,544,8,ATM_TD_FW_MBS_01); /* _id */
    q_put(q_initial,576,8,ATM_TD_BW_MBS_01); /* _id */
    q_put(q_initial,608,8,ATM_TD_BEST_EFFORT); /* _id */
    q_put(q_initial,616,8,ATM_TD_TM_OPT); /* _id */
    q_put(q_initial,631,1,ATM_FD_NO); /* fw_fdisc */
    q_put(q_initial,630,1,ATM_FD_NO); /* bw_fdisc */
    q_put(q_initial,625,1,ATM_TAG_NO); /* bw_tag */
    q_put(q_initial,624,1,ATM_TAG_NO); /* fw_tag */
    q_put(q_initial,632,8,ATM_IE_BBCAP); /* _ie_id */
    q_put(q_initial,647,1,1); /* _ext */
    q_put(q_initial,645,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,644,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,640,3,0); /* _action_ind */
    q_put(q_initial,671,1,0); /* _ext */
    q_put(q_initial,679,1,1); /* _ext */
    q_put(q_initial,687,1,1); /* _ext */
    q_put(q_initial,695,1,1); /* _ext */
    q_put(q_initial,693,2,ATM_STC_NO); /* susc_clip */
    q_put(q_initial,688,2,ATM_UPCC_P2P); /* upcc */
    q_put(q_initial,696,8,ATM_IE_BHLI); /* _ie_id */
    q_put(q_initial,711,1,1); /* _ext */
    q_put(q_initial,709,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,708,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,704,3,0); /* _action_ind */
    q_put(q_initial,735,1,1); /* _ext */
    q_put(q_initial,960,8,ATM_IE_BLLI); /* _ie_id */
    q_put(q_initial,975,1,1); /* _ext */
    q_put(q_initial,973,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,972,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,968,3,0); /* _action_ind */
    q_put(q_initial,997,2,1); /* _lid */
    q_put(q_initial,999,1,1); /* _ext */
    q_put(q_initial,1005,2,2); /* _lid */
    q_put(q_initial,1007,1,0); /* _ext */
    q_put(q_initial,1013,2,ATM_IMD_NORMAL); /* l2_mode */
    q_put(q_initial,1008,2,0); /* q933 */
    q_put(q_initial,1015,1,0); /* _ext */
    q_put(q_initial,1023,1,1); /* _ext */
    q_put(q_initial,1031,1,1); /* _ext */
    q_put(q_initial,1039,1,1); /* _ext */
    q_put(q_initial,1047,1,0); /* _ext */
    q_put(q_initial,1055,1,1); /* _ext */
    q_put(q_initial,1063,1,1); /* _ext */
    q_put(q_initial,1069,2,3); /* _lid */
    q_put(q_initial,1071,1,0); /* _ext */
    q_put(q_initial,1077,2,ATM_IMD_NORMAL); /* l3_mode */
    q_put(q_initial,1079,1,0); /* _ext */
    q_put(q_initial,1087,1,0); /* _ext */
    q_put(q_initial,1095,1,1); /* _ext */
    q_put(q_initial,1111,1,1); /* _ext */
    q_put(q_initial,1119,1,1); /* _ext */
    q_put(q_initial,1127,1,1); /* _ext */
    q_put(q_initial,1135,1,0); /* _ext */
    q_put(q_initial,1136,4,ATM_TT_RXTX); /* term_type */
    q_put(q_initial,1143,1,0); /* _ext */
    q_put(q_initial,1151,1,1); /* _ext */
    q_put(q_initial,1147,3,ATM_MC_NONE); /* fw_mpx_cap */
    q_put(q_initial,1144,3,ATM_MC_NONE); /* bw_mpx_cap */
    q_put(q_initial,1159,1,1); /* _ext */
    q_put(q_initial,1167,1,1); /* _ext */
    q_put(q_initial,1175,1,0); /* _ext */
    q_put(q_initial,1183,1,0); /* _ext */
    q_put(q_initial,1191,1,1); /* _ext */
    q_put(q_initial,1190,1,0); /* _ipi_low */
    q_put(q_initial,1199,1,1); /* _ext */
    q_put(q_initial,1197,2,0); /* _snap_id */
    q_put(q_initial,1246,1,1); /* _ipi_low */
    q_put(q_initial,1255,1,1); /* _ext */
    q_put(q_initial,1263,1,1); /* _ext */
    q_put(q_initial,1271,1,0); /* _ext */
    q_put(q_initial,1279,1,1); /* _ext */
    q_put(q_initial,1280,8,ATM_IE_BLLI); /* _ie_id */
    q_put(q_initial,1295,1,1); /* _ext */
    q_put(q_initial,1293,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,1292,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,1288,3,0); /* _action_ind */
    q_put(q_initial,1317,2,1); /* _lid */
    q_put(q_initial,1319,1,1); /* _ext */
    q_put(q_initial,1325,2,2); /* _lid */
    q_put(q_initial,1327,1,0); /* _ext */
    q_put(q_initial,1333,2,ATM_IMD_NORMAL); /* l2_mode */
    q_put(q_initial,1328,2,0); /* q933 */
    q_put(q_initial,1335,1,0); /* _ext */
    q_put(q_initial,1343,1,1); /* _ext */
    q_put(q_initial,1351,1,1); /* _ext */
    q_put(q_initial,1359,1,1); /* _ext */
    q_put(q_initial,1367,1,0); /* _ext */
    q_put(q_initial,1375,1,1); /* _ext */
    q_put(q_initial,1383,1,1); /* _ext */
    q_put(q_initial,1389,2,3); /* _lid */
    q_put(q_initial,1391,1,0); /* _ext */
    q_put(q_initial,1397,2,ATM_IMD_NORMAL); /* l3_mode */
    q_put(q_initial,1399,1,0); /* _ext */
    q_put(q_initial,1407,1,0); /* _ext */
    q_put(q_initial,1415,1,1); /* _ext */
    q_put(q_initial,1431,1,1); /* _ext */
    q_put(q_initial,1439,1,1); /* _ext */
    q_put(q_initial,1447,1,1); /* _ext */
    q_put(q_initial,1455,1,0); /* _ext */
    q_put(q_initial,1456,4,ATM_TT_RXTX); /* term_type */
    q_put(q_initial,1463,1,0); /* _ext */
    q_put(q_initial,1471,1,1); /* _ext */
    q_put(q_initial,1467,3,ATM_MC_NONE); /* fw_mpx_cap */
    q_put(q_initial,1464,3,ATM_MC_NONE); /* bw_mpx_cap */
    q_put(q_initial,1479,1,1); /* _ext */
    q_put(q_initial,1487,1,1); /* _ext */
    q_put(q_initial,1495,1,0); /* _ext */
    q_put(q_initial,1503,1,0); /* _ext */
    q_put(q_initial,1511,1,1); /* _ext */
    q_put(q_initial,1510,1,0); /* _ipi_low */
    q_put(q_initial,1519,1,1); /* _ext */
    q_put(q_initial,1517,2,0); /* _snap_id */
    q_put(q_initial,1566,1,1); /* _ipi_low */
    q_put(q_initial,1575,1,1); /* _ext */
    q_put(q_initial,1583,1,1); /* _ext */
    q_put(q_initial,1591,1,0); /* _ext */
    q_put(q_initial,1599,1,1); /* _ext */
    q_put(q_initial,1600,8,ATM_IE_BLLI); /* _ie_id */
    q_put(q_initial,1615,1,1); /* _ext */
    q_put(q_initial,1613,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,1612,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,1608,3,0); /* _action_ind */
    q_put(q_initial,1637,2,1); /* _lid */
    q_put(q_initial,1639,1,1); /* _ext */
    q_put(q_initial,1645,2,2); /* _lid */
    q_put(q_initial,1647,1,0); /* _ext */
    q_put(q_initial,1653,2,ATM_IMD_NORMAL); /* l2_mode */
    q_put(q_initial,1648,2,0); /* q933 */
    q_put(q_initial,1655,1,0); /* _ext */
    q_put(q_initial,1663,1,1); /* _ext */
    q_put(q_initial,1671,1,1); /* _ext */
    q_put(q_initial,1679,1,1); /* _ext */
    q_put(q_initial,1687,1,0); /* _ext */
    q_put(q_initial,1695,1,1); /* _ext */
    q_put(q_initial,1703,1,1); /* _ext */
    q_put(q_initial,1709,2,3); /* _lid */
    q_put(q_initial,1711,1,0); /* _ext */
    q_put(q_initial,1717,2,ATM_IMD_NORMAL); /* l3_mode */
    q_put(q_initial,1719,1,0); /* _ext */
    q_put(q_initial,1727,1,0); /* _ext */
    q_put(q_initial,1735,1,1); /* _ext */
    q_put(q_initial,1751,1,1); /* _ext */
    q_put(q_initial,1759,1,1); /* _ext */
    q_put(q_initial,1767,1,1); /* _ext */
    q_put(q_initial,1775,1,0); /* _ext */
    q_put(q_initial,1776,4,ATM_TT_RXTX); /* term_type */
    q_put(q_initial,1783,1,0); /* _ext */
    q_put(q_initial,1791,1,1); /* _ext */
    q_put(q_initial,1787,3,ATM_MC_NONE); /* fw_mpx_cap */
    q_put(q_initial,1784,3,ATM_MC_NONE); /* bw_mpx_cap */
    q_put(q_initial,1799,1,1); /* _ext */
    q_put(q_initial,1807,1,1); /* _ext */
    q_put(q_initial,1815,1,0); /* _ext */
    q_put(q_initial,1823,1,0); /* _ext */
    q_put(q_initial,1831,1,1); /* _ext */
    q_put(q_initial,1830,1,0); /* _ipi_low */
    q_put(q_initial,1839,1,1); /* _ext */
    q_put(q_initial,1837,2,0); /* _snap_id */
    q_put(q_initial,1886,1,1); /* _ipi_low */
    q_put(q_initial,1895,1,1); /* _ext */
    q_put(q_initial,1903,1,1); /* _ext */
    q_put(q_initial,1911,1,0); /* _ext */
    q_put(q_initial,1919,1,1); /* _ext */
    q_put(q_initial,1920,8,ATM_IE_CALL_STATE); /* _ie_id */
    q_put(q_initial,1935,1,1); /* _ext */
    q_put(q_initial,1933,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,1932,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,1928,3,0); /* _action_ind */
    q_put(q_initial,1960,8,ATM_IE_CDPN); /* _ie_id */
    q_put(q_initial,1975,1,1); /* _ext */
    q_put(q_initial,1973,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,1972,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,1968,3,0); /* _action_ind */
    q_put(q_initial,1999,1,1); /* _ext */
    q_put(q_initial,1992,4,ATM_NP_E164); /* _plan */
    q_put(q_initial,1996,3,ATM_TON_INTRNTNL); /* _type */
    q_put(q_initial,2096,4,ATM_NP_AEA); /* _plan */
    q_put(q_initial,2100,3,ATM_TON_UNKNOWN); /* _type */
    q_put(q_initial,2264,8,ATM_IE_CDPS); /* _ie_id */
    q_put(q_initial,2279,1,1); /* _ext */
    q_put(q_initial,2277,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,2276,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,2272,3,0); /* _action_ind */
    q_put(q_initial,2303,1,1); /* _ext */
    q_put(q_initial,2300,3,ATM_SAT_AEA); /* cdps_type */
    q_put(q_initial,2299,1,0); /* _oddeven */
    q_put(q_initial,2464,8,ATM_IE_CGPN); /* _ie_id */
    q_put(q_initial,2479,1,1); /* _ext */
    q_put(q_initial,2477,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,2476,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,2472,3,0); /* _action_ind */
    q_put(q_initial,2503,1,0); /* _ext */
    q_put(q_initial,2511,1,1); /* _ext */
    q_put(q_initial,2509,2,ATM_PRS_ALLOW); /* pres_ind */
    q_put(q_initial,2504,2,ATM_SCRN_UP_NS); /* scr_ind */
    q_put(q_initial,2519,1,1); /* _ext */
    q_put(q_initial,2680,8,ATM_IE_CGPS); /* _ie_id */
    q_put(q_initial,2695,1,1); /* _ext */
    q_put(q_initial,2693,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,2692,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,2688,3,0); /* _action_ind */
    q_put(q_initial,2719,1,1); /* _ext */
    q_put(q_initial,2716,3,ATM_SAT_AEA); /* cgps_type */
    q_put(q_initial,2715,1,0); /* _oddeven */
    q_put(q_initial,2880,8,ATM_IE_CAUSE); /* _ie_id */
    q_put(q_initial,2895,1,1); /* _ext */
    q_put(q_initial,2893,2,Q2931_CS_ITU); /* cause_cs */
    q_put(q_initial,2892,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,2888,3,0); /* _action_ind */
    q_put(q_initial,2919,1,1); /* _ext */
    q_put(q_initial,2912,4,ATM_LOC_USER); /* location */
    q_put(q_initial,2927,1,1); /* _ext */
    q_put(q_initial,2935,1,1); /* _ext */
    q_put(q_initial,2931,1,ATM_PU_USER); /* pu */
    q_put(q_initial,2930,1,ATM_NA_NORMAL); /* na */
    q_put(q_initial,2928,2,ATM_COND_UNKNOWN); /* cond2 */
    q_put(q_initial,2943,1,1); /* _ext */
    q_put(q_initial,2936,2,ATM_COND_UNKNOWN); /* cond3 */
    q_put(q_initial,3912,8,ATM_IE_CAUSE); /* _ie_id */
    q_put(q_initial,3927,1,1); /* _ext */
    q_put(q_initial,3925,2,Q2931_CS_ITU); /* cause_cs */
    q_put(q_initial,3924,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,3920,3,0); /* _action_ind */
    q_put(q_initial,3951,1,1); /* _ext */
    q_put(q_initial,3944,4,ATM_LOC_USER); /* location */
    q_put(q_initial,3959,1,1); /* _ext */
    q_put(q_initial,3967,1,1); /* _ext */
    q_put(q_initial,3963,1,ATM_PU_USER); /* pu */
    q_put(q_initial,3962,1,ATM_NA_NORMAL); /* na */
    q_put(q_initial,3960,2,ATM_COND_UNKNOWN); /* cond2 */
    q_put(q_initial,3975,1,1); /* _ext */
    q_put(q_initial,3968,2,ATM_COND_UNKNOWN); /* cond3 */
    q_put(q_initial,4944,8,ATM_IE_CONN_ID); /* _ie_id */
    q_put(q_initial,4959,1,1); /* _ext */
    q_put(q_initial,4957,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,4956,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,4952,3,0); /* _action_ind */
    q_put(q_initial,4983,1,1); /* _ext */
    q_put(q_initial,4979,2,ATM_VPA_EXPL); /* _vp_ass */
    q_put(q_initial,4976,3,0); /* _pref_exc */
    q_put(q_initial,5016,8,ATM_IE_E2E_TDL); /* _ie_id */
    q_put(q_initial,5031,1,1); /* _ext */
    q_put(q_initial,5029,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5028,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5024,3,0); /* _action_ind */
    q_put(q_initial,5048,8,ATM_TDL_CUM); /* _id */
    q_put(q_initial,5072,8,ATM_TDL_E2EMAX); /* _id */
    q_put(q_initial,5096,8,ATM_TDL_NGI); /* _id */
    q_put(q_initial,5104,8,ATM_IE_QOS); /* _ie_id */
    q_put(q_initial,5119,1,1); /* _ext */
    q_put(q_initial,5117,2,Q2931_CS_NET); /* qos_cs */
    q_put(q_initial,5116,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5112,3,0); /* _action_ind */
    q_put(q_initial,5136,8,0); /* qos_fw */
    q_put(q_initial,5144,8,0); /* qos_bw */
    q_put(q_initial,5152,8,ATM_IE_BBREP); /* _ie_id */
    q_put(q_initial,5167,1,1); /* _ext */
    q_put(q_initial,5165,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5164,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5160,3,0); /* _action_ind */
    q_put(q_initial,5191,1,1); /* _ext */
    q_put(q_initial,5184,4,2); /* rep_ind */
    q_put(q_initial,5192,8,ATM_IE_RESTART); /* _ie_id */
    q_put(q_initial,5207,1,1); /* _ext */
    q_put(q_initial,5205,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5204,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5200,3,0); /* _action_ind */
    q_put(q_initial,5231,1,1); /* _ext */
    q_put(q_initial,5232,8,ATM_IE_BBS_COMP); /* _ie_id */
    q_put(q_initial,5247,1,1); /* _ext */
    q_put(q_initial,5245,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5244,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5240,3,0); /* _action_ind */
    q_put(q_initial,5271,1,1); /* _ext */
    q_put(q_initial,5264,7,0x21); /* bbsc_ind */
    q_put(q_initial,5272,8,ATM_IE_TNS); /* _ie_id */
    q_put(q_initial,5287,1,1); /* _ext */
    q_put(q_initial,5285,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5284,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5280,3,0); /* _action_ind */
    q_put(q_initial,5311,1,1); /* _ext */
    q_put(q_initial,5308,3,ATM_TNI_NNI); /* _net_type */
    q_put(q_initial,5304,4,ATM_NIP_CARRIER); /* _carrier_id */
    q_put(q_initial,5344,8,ATM_IE_NOTIFY); /* _ie_id */
    q_put(q_initial,5359,1,1); /* _ext */
    q_put(q_initial,5357,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5356,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5352,3,0); /* _action_ind */
    q_put(q_initial,5408,8,ATM_IE_OAM_TD); /* _ie_id */
    q_put(q_initial,5423,1,1); /* _ext */
    q_put(q_initial,5421,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,5420,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5416,3,0); /* _action_ind */
    q_put(q_initial,5447,1,1); /* _ext */
    q_put(q_initial,5445,2,ATM_SHI_NONE); /* shaping */
    q_put(q_initial,5444,1,ATM_OCI_OPT); /* compliance */
    q_put(q_initial,5440,3,ATM_UNFM_NONE); /* fault */
    q_put(q_initial,5455,1,1); /* _ext */
    q_put(q_initial,5452,3,ATM_OFI_0_0); /* fwd_ofi */
    q_put(q_initial,5448,3,ATM_OFI_0_0); /* bwd_ofi */
    q_put(q_initial,5456,8,ATM_IE_GIT); /* _ie_id */
    q_put(q_initial,5471,1,1); /* _ext */
    q_put(q_initial,5469,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,5468,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5464,3,0); /* _action_ind */
    q_put(q_initial,5495,1,0); /* _dummy */
    q_put(q_initial,5504,8,ATM_IT_SESSION); /* _type */
    q_put(q_initial,5680,8,ATM_IT_RESOURCE); /* _type */
    q_put(q_initial,5952,8,ATM_IE_GIT); /* _ie_id */
    q_put(q_initial,5967,1,1); /* _ext */
    q_put(q_initial,5965,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,5964,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,5960,3,0); /* _action_ind */
    q_put(q_initial,5991,1,0); /* _dummy */
    q_put(q_initial,6000,8,ATM_IT_SESSION); /* _type */
    q_put(q_initial,6176,8,ATM_IT_RESOURCE); /* _type */
    q_put(q_initial,6448,8,ATM_IE_GIT); /* _ie_id */
    q_put(q_initial,6463,1,1); /* _ext */
    q_put(q_initial,6461,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,6460,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,6456,3,0); /* _action_ind */
    q_put(q_initial,6487,1,0); /* _dummy */
    q_put(q_initial,6496,8,ATM_IT_SESSION); /* _type */
    q_put(q_initial,6672,8,ATM_IT_RESOURCE); /* _type */
    q_put(q_initial,6944,8,ATM_IE_LIJ_ID); /* _ie_id */
    q_put(q_initial,6959,1,1); /* _ext */
    q_put(q_initial,6957,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,6956,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,6952,3,0); /* _action_ind */
    q_put(q_initial,6983,1,1); /* _ext */
    q_put(q_initial,6976,7,ATM_LIT_ROOT); /* lij_id_type */
    q_put(q_initial,7016,8,ATM_IE_LIJ_PRM); /* _ie_id */
    q_put(q_initial,7031,1,1); /* _ext */
    q_put(q_initial,7029,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,7028,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7024,3,0); /* _action_ind */
    q_put(q_initial,7055,1,1); /* _ext */
    q_put(q_initial,7056,8,ATM_IE_LEAF_SN); /* _ie_id */
    q_put(q_initial,7071,1,1); /* _ext */
    q_put(q_initial,7069,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,7068,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7064,3,0); /* _action_ind */
    q_put(q_initial,7120,8,ATM_IE_SCOPE_SEL); /* _ie_id */
    q_put(q_initial,7135,1,1); /* _ext */
    q_put(q_initial,7133,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,7132,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7128,3,0); /* _action_ind */
    q_put(q_initial,7159,1,1); /* _ext */
    q_put(q_initial,7152,4,ATM_TCS_ORGANIZATIONAL); /* scope_type */
    q_put(q_initial,7168,8,ATM_IE_ALT_TD); /* _ie_id */
    q_put(q_initial,7183,1,1); /* _ext */
    q_put(q_initial,7181,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,7180,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7176,3,0); /* _action_ind */
    q_put(q_initial,7200,8,ATM_TD_FW_PCR_0); /* _id */
    q_put(q_initial,7232,8,ATM_TD_BW_PCR_0); /* _id */
    q_put(q_initial,7264,8,ATM_TD_FW_PCR_01); /* _id */
    q_put(q_initial,7296,8,ATM_TD_BW_PCR_01); /* _id */
    q_put(q_initial,7328,8,ATM_TD_FW_SCR_0); /* _id */
    q_put(q_initial,7360,8,ATM_TD_BW_SCR_0); /* _id */
    q_put(q_initial,7392,8,ATM_TD_FW_SCR_01); /* _id */
    q_put(q_initial,7424,8,ATM_TD_BW_SCR_01); /* _id */
    q_put(q_initial,7456,8,ATM_TD_FW_MBS_0); /* _id */
    q_put(q_initial,7488,8,ATM_TD_BW_MBS_0); /* _id */
    q_put(q_initial,7520,8,ATM_TD_FW_MBS_01); /* _id */
    q_put(q_initial,7552,8,ATM_TD_BW_MBS_01); /* _id */
    q_put(q_initial,7584,8,ATM_TD_BEST_EFFORT); /* _id */
    q_put(q_initial,7592,8,ATM_IE_MIN_TD); /* _ie_id */
    q_put(q_initial,7607,1,1); /* _ext */
    q_put(q_initial,7605,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,7604,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7600,3,0); /* _action_ind */
    q_put(q_initial,7624,8,ATM_TD_FW_PCR_0); /* _id */
    q_put(q_initial,7656,8,ATM_TD_BW_PCR_0); /* _id */
    q_put(q_initial,7688,8,ATM_TD_FW_PCR_01); /* _id */
    q_put(q_initial,7720,8,ATM_TD_BW_PCR_01); /* _id */
    q_put(q_initial,7752,8,ATM_TD_FW_MCR_01); /* _id */
    q_put(q_initial,7784,8,ATM_TD_BW_MCR_01); /* _id */
    q_put(q_initial,7816,8,ATM_IE_EQOS); /* _ie_id */
    q_put(q_initial,7831,1,1); /* _ext */
    q_put(q_initial,7829,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,7828,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,7824,3,0); /* _action_ind */
    q_put(q_initial,7856,8,ATM_EQP_ACC_FW_CDV); /* _id */
    q_put(q_initial,7888,8,ATM_EQP_ACC_BW_CDV); /* _id */
    q_put(q_initial,7920,8,ATM_EQP_CUM_FW_CDV); /* _id */
    q_put(q_initial,7952,8,ATM_EQP_CUM_BW_CDV); /* _id */
    q_put(q_initial,7984,8,ATM_EQP_ACC_FW_CLR); /* _id */
    q_put(q_initial,8000,8,ATM_EQP_ACC_BW_CLR); /* _id */
    q_put(q_initial,8016,8,ATM_IE_ABR_ADD_PRM); /* _ie_id */
    q_put(q_initial,8031,1,1); /* _ext */
    q_put(q_initial,8029,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,8028,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8024,3,0); /* _action_ind */
    q_put(q_initial,8048,8,ATM_AAP_FW_REC); /* _id */
    q_put(q_initial,8088,8,ATM_AAP_BW_REC); /* _id */
    q_put(q_initial,8128,8,ATM_IE_ABR_SET_PRM); /* _ie_id */
    q_put(q_initial,8143,1,1); /* _ext */
    q_put(q_initial,8141,2,Q2931_CS_NET); /* _cs */
    q_put(q_initial,8140,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8136,3,0); /* _action_ind */
    q_put(q_initial,8160,8,ATM_ASP_FW_ICR); /* _id */
    q_put(q_initial,8192,8,ATM_ASP_BW_ICR); /* _id */
    q_put(q_initial,8224,8,ATM_ASP_FW_TBE); /* _id */
    q_put(q_initial,8256,8,ATM_ASP_BW_TBE); /* _id */
    q_put(q_initial,8288,8,ATM_ASP_CRF_RTT); /* _id */
    q_put(q_initial,8320,8,ATM_ASP_FW_RIF); /* _id */
    q_put(q_initial,8336,8,ATM_ASP_BW_RIF); /* _id */
    q_put(q_initial,8352,8,ATM_ASP_FW_RDF); /* _id */
    q_put(q_initial,8368,8,ATM_ASP_BW_RDF); /* _id */
    q_put(q_initial,8384,8,ATM_IE_EPR); /* _ie_id */
    q_put(q_initial,8399,1,1); /* _ext */
    q_put(q_initial,8397,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,8396,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8392,3,0); /* _action_ind */
    q_put(q_initial,8416,8,0); /* _ep_type */
    q_put(q_initial,8440,8,ATM_IE_EP_STATE); /* _ie_id */
    q_put(q_initial,8455,1,1); /* _ext */
    q_put(q_initial,8453,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,8452,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8448,3,0); /* _action_ind */
    q_put(q_initial,8480,8,ATM_IE_BBRT); /* _ie_id */
    q_put(q_initial,8495,1,1); /* _ext */
    q_put(q_initial,8493,2,Q2931_CS_ITU); /* _cs */
    q_put(q_initial,8492,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8488,3,0); /* _action_ind */
    q_put(q_initial,8520,8,0); /* _ie_id */
    q_put(q_initial,8535,1,1); /* _ext */
    q_put(q_initial,8533,2,0); /* __cs */
    q_put(q_initial,8532,1,ATM_FLAG_NO); /* _flag */
    q_put(q_initial,8528,3,0); /* _action_ind */
}


/*
 * If a group contains required fields, these are listed in the
 * following arrays. Each list ends with -1. The variable names
 * end with the group number.
 */

static int required_0[] = {
    0, /* call_ref */
    1, /* msg_type */
    -1
};

static int required_3[] = {
    4, /* fw_max_sdu */
    -1
};

static int required_4[] = {
    5, /* bw_max_sdu */
    -1
};

static int required_5[] = {
    6, /* aal_mode */
    -1
};

static int required_6[] = {
    7, /* sscs_type */
    -1
};

static int required_8[] = {
    8, /* fw_pcr_0 */
    -1
};

static int required_9[] = {
    9, /* bw_pcr_0 */
    -1
};

static int required_10[] = {
    10, /* fw_pcr_01 */
    -1
};

static int required_11[] = {
    11, /* bw_pcr_01 */
    -1
};

static int required_12[] = {
    12, /* fw_scr_0 */
    -1
};

static int required_13[] = {
    13, /* bw_scr_0 */
    -1
};

static int required_14[] = {
    14, /* fw_scr_01 */
    -1
};

static int required_15[] = {
    15, /* bw_scr_01 */
    -1
};

static int required_16[] = {
    16, /* fw_mbs_0 */
    -1
};

static int required_17[] = {
    17, /* bw_mbs_0 */
    -1
};

static int required_18[] = {
    18, /* fw_mbs_01 */
    -1
};

static int required_19[] = {
    19, /* bw_mbs_01 */
    -1
};

static int required_20[] = {
    20, /* best_effort */
    -1
};

static int required_22[] = {
    25, /* bearer_class */
    -1
};

static int required_23[] = {
    26, /* trans_cap */
    -1
};

static int required_26[] = {
    30, /* iso_hli */
    -1
};

static int required_27[] = {
    31, /* user_hli */
    -1
};

static int required_28[] = {
    32, /* hlp */
    -1
};

static int required_29[] = {
    33, /* hli_oui */
    34, /* app_id */
    -1
};

static int required_30[] = {
    35, /* tobedefined */
    -1
};

static int required_32[] = {
    36, /* uil1_proto */
    -1
};

static int required_36[] = {
    40, /* window_size */
    -1
};

static int required_39[] = {
    41, /* user_l2 */
    -1
};

static int required_44[] = {
    44, /* def_pck_size */
    -1
};

static int required_45[] = {
    45, /* pck_win_size */
    -1
};

static int required_57[] = {
    50, /* oui */
    51, /* pid */
    -1
};

static int required_59[] = {
    52, /* ipi_low */
    -1
};

static int required_61[] = {
    53, /* user_l3 */
    -1
};

static int required_63[] = {
    54, /* uil1_proto */
    -1
};

static int required_67[] = {
    58, /* window_size */
    -1
};

static int required_70[] = {
    59, /* user_l2 */
    -1
};

static int required_75[] = {
    62, /* def_pck_size */
    -1
};

static int required_76[] = {
    63, /* pck_win_size */
    -1
};

static int required_88[] = {
    68, /* oui */
    69, /* pid */
    -1
};

static int required_90[] = {
    70, /* ipi_low */
    -1
};

static int required_92[] = {
    71, /* user_l3 */
    -1
};

static int required_94[] = {
    72, /* uil1_proto */
    -1
};

static int required_98[] = {
    76, /* window_size */
    -1
};

static int required_101[] = {
    77, /* user_l2 */
    -1
};

static int required_106[] = {
    80, /* def_pck_size */
    -1
};

static int required_107[] = {
    81, /* pck_win_size */
    -1
};

static int required_119[] = {
    86, /* oui */
    87, /* pid */
    -1
};

static int required_121[] = {
    88, /* ipi_low */
    -1
};

static int required_123[] = {
    89, /* user_l3 */
    -1
};

static int required_124[] = {
    90, /* call_state */
    -1
};

static int required_126[] = {
    91, /* cdpn_e164 */
    -1
};

static int required_127[] = {
    92, /* cdpn_esa */
    -1
};

static int required_128[] = {
    94, /* cdps */
    -1
};

static int required_129[] = {
    95, /* cgpn_plan */
    96, /* cgpn_type */
    99, /* cgpn */
    -1
};

static int required_132[] = {
    101, /* cgps */
    -1
};

static int required_136[] = {
    110, /* user_diag */
    -1
};

static int required_137[] = {
    111, /* ie_id3 */
    -1
};

static int required_149[] = {
    128, /* user_diag */
    -1
};

static int required_150[] = {
    129, /* ie_id3 */
    -1
};

static int required_159[] = {
    138, /* vpi */
    139, /* vci */
    -1
};

static int required_161[] = {
    140, /* cum_delay */
    -1
};

static int required_162[] = {
    141, /* max_delay */
    -1
};

static int required_166[] = {
    146, /* rst_class */
    -1
};

static int required_168[] = {
    148, /* net_id */
    -1
};

static int required_169[] = {
    149, /* notification */
    -1
};

static int required_172[] = {
    156, /* session_id */
    157, /* resource_id */
    -1
};

static int required_173[] = {
    158, /* unrecognized_git_identifiers */
    -1
};

static int required_175[] = {
    160, /* session_id */
    161, /* resource_id */
    -1
};

static int required_176[] = {
    162, /* unrecognized_git_identifiers */
    -1
};

static int required_178[] = {
    164, /* session_id */
    165, /* resource_id */
    -1
};

static int required_179[] = {
    166, /* unrecognized_git_identifiers */
    -1
};

static int required_180[] = {
    168, /* lij_id */
    -1
};

static int required_181[] = {
    169, /* lij_scr_ind */
    -1
};

static int required_182[] = {
    170, /* leaf_sn */
    -1
};

static int required_183[] = {
    172, /* scope_sel */
    -1
};

static int required_185[] = {
    173, /* altfw_pcr_0 */
    -1
};

static int required_186[] = {
    174, /* altbw_pcr_0 */
    -1
};

static int required_187[] = {
    175, /* altfw_pcr_01 */
    -1
};

static int required_188[] = {
    176, /* altbw_pcr_01 */
    -1
};

static int required_189[] = {
    177, /* altfw_scr_0 */
    -1
};

static int required_190[] = {
    178, /* altbw_scr_0 */
    -1
};

static int required_191[] = {
    179, /* altfw_scr_01 */
    -1
};

static int required_192[] = {
    180, /* altbw_scr_01 */
    -1
};

static int required_193[] = {
    181, /* altfw_mbs_0 */
    -1
};

static int required_194[] = {
    182, /* altbw_mbs_0 */
    -1
};

static int required_195[] = {
    183, /* altfw_mbs_01 */
    -1
};

static int required_196[] = {
    184, /* altbw_mbs_01 */
    -1
};

static int required_197[] = {
    185, /* altbest_effort */
    -1
};

static int required_199[] = {
    186, /* minfw_pcr_0 */
    -1
};

static int required_200[] = {
    187, /* minbw_pcr_0 */
    -1
};

static int required_201[] = {
    188, /* minfw_pcr_01 */
    -1
};

static int required_202[] = {
    189, /* minbw_pcr_01 */
    -1
};

static int required_203[] = {
    190, /* minfw_mcr_01 */
    -1
};

static int required_204[] = {
    191, /* minbw_mcr_01 */
    -1
};

static int required_205[] = {
    192, /* eqos_origin */
    -1
};

static int required_206[] = {
    193, /* acc_fw_cdv */
    -1
};

static int required_207[] = {
    194, /* acc_bw_cdv */
    -1
};

static int required_208[] = {
    195, /* cum_fw_cdv */
    -1
};

static int required_209[] = {
    196, /* cum_bw_cdv */
    -1
};

static int required_210[] = {
    197, /* acc_fw_clr */
    -1
};

static int required_211[] = {
    198, /* acc_bw_clr */
    -1
};

static int required_213[] = {
    199, /* abr_fw_add_rec */
    -1
};

static int required_214[] = {
    200, /* abr_bw_add_rec */
    -1
};

static int required_216[] = {
    201, /* abr_fw_icr */
    -1
};

static int required_217[] = {
    202, /* abr_bw_icr */
    -1
};

static int required_218[] = {
    203, /* abr_fw_tbe */
    -1
};

static int required_219[] = {
    204, /* abr_bw_tbe */
    -1
};

static int required_220[] = {
    205, /* atm_crf_rtt */
    -1
};

static int required_221[] = {
    206, /* atm_fw_rif */
    -1
};

static int required_222[] = {
    207, /* atm_bw_rif */
    -1
};

static int required_223[] = {
    208, /* atm_fw_rdf */
    -1
};

static int required_224[] = {
    209, /* atm_bw_rdf */
    -1
};

static int required_225[] = {
    210, /* ep_ref */
    -1
};

static int required_226[] = {
    211, /* ep_state */
    -1
};

static int required_227[] = {
    212, /* type_of_report */
    -1
};


/*
 * Various information about groups.
 */

typedef struct {
    int parent;
    int *required;
    int start;
    int length;
    int offset;
} GROUP;

static GROUP groups[] = {
    { -1, required_0 },
    { 0, NULL, -1, 0, 0 },
    { 1, NULL, -1, 0, 0 },
    { 2, required_3, -1, 0, 0 }, /* fw_max_sdu */
    { 2, required_4, -1, 0, 0 }, /* bw_max_sdu */
    { 2, required_5, -1, 0, 0 }, /* aal_mode */
    { 2, required_6, -1, 0, 0 }, /* sscs_type */
    { 0, NULL, -1, 0, 0 },
    { 7, required_8, -1, 0, 0 }, /* _dummy */
    { 7, required_9, -1, 0, 0 }, /* _dummy */
    { 7, required_10, -1, 0, 0 }, /* _dummy */
    { 7, required_11, -1, 0, 0 }, /* _dummy */
    { 7, required_12, -1, 0, 0 }, /* _dummy */
    { 7, required_13, -1, 0, 0 }, /* _dummy */
    { 7, required_14, -1, 0, 0 }, /* _dummy */
    { 7, required_15, -1, 0, 0 }, /* _dummy */
    { 7, required_16, -1, 0, 0 }, /* _dummy */
    { 7, required_17, -1, 0, 0 }, /* _dummy */
    { 7, required_18, -1, 0, 0 }, /* _dummy */
    { 7, required_19, -1, 0, 0 }, /* _dummy */
    { 7, required_20, -1, 0, 0 }, /* _dummy */
    { 7, NULL, -1, 0, 0 },
    { 0, required_22, -1, 0, 0 },
    { 22, required_23, -1, 0, 0 }, /* _ext */
    { 22, NULL, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 25, required_26, -1, 0, 0 }, /* iso_hli */
    { 25, required_27, -1, 0, 0 }, /* user_hli */
    { 25, required_28, -1, 0, 0 }, /* hlp */
    { 25, required_29, -1, 0, 0 }, /* hli_oui */
    { 25, required_30, -1, 0, 0 }, /* tobedefined */
    { 0, NULL, 36, 18, 0 },
    { 31, required_32, -1, 0, 0 }, /* _ext */
    { 31, NULL, -1, 0, 0 },
    { 33, NULL, -1, 0, 0 },
    { 34, NULL, -1, 0, 0 },
    { 35, required_36, -1, 0, 0 }, /* window_size */
    { 35, NULL, -1, 0, 0 },
    { 34, NULL, -1, 0, 0 },
    { 33, required_39, -1, 0, 0 }, /* _ext */
    { 33, NULL, -1, 0, 0 },
    { 31, NULL, -1, 0, 0 },
    { 41, NULL, -1, 0, 0 },
    { 42, NULL, -1, 0, 0 },
    { 43, required_44, -1, 0, 0 }, /* def_pck_size */
    { 44, required_45, -1, 0, 0 }, /* _ext */
    { 44, NULL, -1, 0, 0 },
    { 43, NULL, -1, 0, 0 },
    { 42, NULL, -1, 0, 0 },
    { 41, NULL, -1, 0, 0 },
    { 49, NULL, -1, 0, 0 },
    { 50, NULL, -1, 0, 0 },
    { 50, NULL, -1, 0, 0 },
    { 49, NULL, -1, 0, 0 },
    { 41, NULL, -1, 0, 0 },
    { 54, NULL, -1, 0, 0 },
    { 55, NULL, -1, 0, 0 },
    { 56, required_57, -1, 0, 0 }, /* _ext */
    { 56, NULL, -1, 0, 0 },
    { 55, required_59, -1, 0, 0 }, /* _ext */
    { 54, NULL, -1, 0, 0 },
    { 41, required_61, -1, 0, 0 }, /* _ext */
    { 0, NULL, 36, 18, 18 },
    { 62, required_63, -1, 0, 0 }, /* _ext */
    { 62, NULL, -1, 0, 0 },
    { 64, NULL, -1, 0, 0 },
    { 65, NULL, -1, 0, 0 },
    { 66, required_67, -1, 0, 0 }, /* window_size */
    { 66, NULL, -1, 0, 0 },
    { 65, NULL, -1, 0, 0 },
    { 64, required_70, -1, 0, 0 }, /* _ext */
    { 64, NULL, -1, 0, 0 },
    { 62, NULL, -1, 0, 0 },
    { 72, NULL, -1, 0, 0 },
    { 73, NULL, -1, 0, 0 },
    { 74, required_75, -1, 0, 0 }, /* def_pck_size */
    { 75, required_76, -1, 0, 0 }, /* _ext */
    { 75, NULL, -1, 0, 0 },
    { 74, NULL, -1, 0, 0 },
    { 73, NULL, -1, 0, 0 },
    { 72, NULL, -1, 0, 0 },
    { 80, NULL, -1, 0, 0 },
    { 81, NULL, -1, 0, 0 },
    { 81, NULL, -1, 0, 0 },
    { 80, NULL, -1, 0, 0 },
    { 72, NULL, -1, 0, 0 },
    { 85, NULL, -1, 0, 0 },
    { 86, NULL, -1, 0, 0 },
    { 87, required_88, -1, 0, 0 }, /* _ext */
    { 87, NULL, -1, 0, 0 },
    { 86, required_90, -1, 0, 0 }, /* _ext */
    { 85, NULL, -1, 0, 0 },
    { 72, required_92, -1, 0, 0 }, /* _ext */
    { 0, NULL, 36, 18, 36 },
    { 93, required_94, -1, 0, 0 }, /* _ext */
    { 93, NULL, -1, 0, 0 },
    { 95, NULL, -1, 0, 0 },
    { 96, NULL, -1, 0, 0 },
    { 97, required_98, -1, 0, 0 }, /* window_size */
    { 97, NULL, -1, 0, 0 },
    { 96, NULL, -1, 0, 0 },
    { 95, required_101, -1, 0, 0 }, /* _ext */
    { 95, NULL, -1, 0, 0 },
    { 93, NULL, -1, 0, 0 },
    { 103, NULL, -1, 0, 0 },
    { 104, NULL, -1, 0, 0 },
    { 105, required_106, -1, 0, 0 }, /* def_pck_size */
    { 106, required_107, -1, 0, 0 }, /* _ext */
    { 106, NULL, -1, 0, 0 },
    { 105, NULL, -1, 0, 0 },
    { 104, NULL, -1, 0, 0 },
    { 103, NULL, -1, 0, 0 },
    { 111, NULL, -1, 0, 0 },
    { 112, NULL, -1, 0, 0 },
    { 112, NULL, -1, 0, 0 },
    { 111, NULL, -1, 0, 0 },
    { 103, NULL, -1, 0, 0 },
    { 116, NULL, -1, 0, 0 },
    { 117, NULL, -1, 0, 0 },
    { 118, required_119, -1, 0, 0 }, /* _ext */
    { 118, NULL, -1, 0, 0 },
    { 117, required_121, -1, 0, 0 }, /* _ext */
    { 116, NULL, -1, 0, 0 },
    { 103, required_123, -1, 0, 0 }, /* _ext */
    { 0, required_124, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 125, required_126, -1, 0, 0 }, /* _type */
    { 125, required_127, -1, 0, 0 }, /* _type */
    { 0, required_128, -1, 0, 0 },
    { 0, required_129, -1, 0, 0 },
    { 129, NULL, -1, 0, 0 },
    { 129, NULL, -1, 0, 0 },
    { 0, required_132, -1, 0, 0 },
    { 0, NULL, 102, 18, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 135, required_136, -1, 0, 0 }, /* user_diag */
    { 135, required_137, -1, 0, 0 }, /* ie_id3 */
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 133, NULL, -1, 0, 0 },
    { 0, NULL, 102, 18, 18 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 148, required_149, -1, 0, 0 }, /* user_diag */
    { 148, required_150, -1, 0, 0 }, /* ie_id3 */
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 146, NULL, -1, 0, 0 },
    { 0, required_159, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 160, required_161, -1, 0, 0 }, /* cum_delay */
    { 160, required_162, -1, 0, 0 }, /* max_delay */
    { 160, NULL, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 0, required_166, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 0, required_168, -1, 0, 0 },
    { 0, required_169, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 0, NULL, 155, 4, 0 },
    { 171, required_172, -1, 0, 0 }, /* _type */
    { 171, required_173, -1, 0, 0 }, /* unrecognized_git_identifiers */
    { 0, NULL, 155, 4, 4 },
    { 174, required_175, -1, 0, 0 }, /* _type */
    { 174, required_176, -1, 0, 0 }, /* unrecognized_git_identifiers */
    { 0, NULL, 155, 4, 8 },
    { 177, required_178, -1, 0, 0 }, /* _type */
    { 177, required_179, -1, 0, 0 }, /* unrecognized_git_identifiers */
    { 0, required_180, -1, 0, 0 },
    { 0, required_181, -1, 0, 0 },
    { 0, required_182, -1, 0, 0 },
    { 0, required_183, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
    { 184, required_185, -1, 0, 0 }, /* altfw_pcr_0 */
    { 184, required_186, -1, 0, 0 }, /* altbw_pcr_0 */
    { 184, required_187, -1, 0, 0 }, /* altfw_pcr_01 */
    { 184, required_188, -1, 0, 0 }, /* altbw_pcr_01 */
    { 184, required_189, -1, 0, 0 }, /* altfw_scr_0 */
    { 184, required_190, -1, 0, 0 }, /* altbw_scr_0 */
    { 184, required_191, -1, 0, 0 }, /* altfw_scr_01 */
    { 184, required_192, -1, 0, 0 }, /* altbw_scr_01 */
    { 184, required_193, -1, 0, 0 }, /* altfw_mbs_0 */
    { 184, required_194, -1, 0, 0 }, /* altbw_mbs_0 */
    { 184, required_195, -1, 0, 0 }, /* altfw_mbs_01 */
    { 184, required_196, -1, 0, 0 }, /* altbw_mbs_01 */
    { 184, required_197, -1, 0, 0 }, /* altbest_effort */
    { 0, NULL, -1, 0, 0 },
    { 198, required_199, -1, 0, 0 }, /* minfw_pcr_0 */
    { 198, required_200, -1, 0, 0 }, /* minbw_pcr_0 */
    { 198, required_201, -1, 0, 0 }, /* minfw_pcr_01 */
    { 198, required_202, -1, 0, 0 }, /* minbw_pcr_01 */
    { 198, required_203, -1, 0, 0 }, /* minfw_mcr_01 */
    { 198, required_204, -1, 0, 0 }, /* minbw_mcr_01 */
    { 0, required_205, -1, 0, 0 },
    { 205, required_206, -1, 0, 0 }, /* acc_fw_cdv */
    { 205, required_207, -1, 0, 0 }, /* acc_bw_cdv */
    { 205, required_208, -1, 0, 0 }, /* cum_fw_cdv */
    { 205, required_209, -1, 0, 0 }, /* cum_bw_cdv */
    { 205, required_210, -1, 0, 0 }, /* acc_fw_clr */
    { 205, required_211, -1, 0, 0 }, /* acc_bw_clr */
    { 0, NULL, -1, 0, 0 },
    { 212, required_213, -1, 0, 0 }, /* abr_fw_add_rec */
    { 212, required_214, -1, 0, 0 }, /* abr_bw_add_rec */
    { 0, NULL, -1, 0, 0 },
    { 215, required_216, -1, 0, 0 }, /* abr_fw_icr */
    { 215, required_217, -1, 0, 0 }, /* abr_bw_icr */
    { 215, required_218, -1, 0, 0 }, /* abr_fw_tbe */
    { 215, required_219, -1, 0, 0 }, /* abr_bw_tbe */
    { 215, required_220, -1, 0, 0 }, /* atm_crf_rtt */
    { 215, required_221, -1, 0, 0 }, /* atm_fw_rif */
    { 215, required_222, -1, 0, 0 }, /* atm_bw_rif */
    { 215, required_223, -1, 0, 0 }, /* atm_fw_rdf */
    { 215, required_224, -1, 0, 0 }, /* atm_bw_rdf */
    { 0, required_225, -1, 0, 0 },
    { 0, required_226, -1, 0, 0 },
    { 0, required_227, -1, 0, 0 },
    { 0, NULL, -1, 0, 0 },
};


/*
 * Named case selectors only have a limited set of valid
 * values. They are listed in the following arrays, each followed
 * by the number of the group it enables.
 */

static int values_3[] = { /* aal_type */
    5, 2,
    -1, -1
};

static int values_29[] = { /* hli_type */
    0, 26,
    1, 27,
    2, 28,
    3, 29,
    4, 30,
    -1, -1
};

static int values_37[] = { /* uil2_proto */
    ATM_L2_X25_LL, 34,
    ATM_L2_X25_ML, 34,
    ATM_L2_HDLC_ARM, 34,
    ATM_L2_HDLC_NRM, 34,
    ATM_L2_HDLC_ABM, 34,
    ATM_L2_Q922, 34,
    ATM_L2_ISO7776, 34,
    ATM_L2_USER, 39,
    ATM_L2_ISO1745, 40,
    ATM_L2_Q291, 40,
    ATM_L2_LAPB, 40,
    ATM_L2_ISO8802, 40,
    ATM_L2_X75, 40,
    -2, 40,
    -1, -1
};

static int values_42[] = { /* uil3_proto */
    ATM_L3_X25, 42,
    ATM_L3_ISO8208, 42,
    ATM_L3_X223, 42,
    ATM_L3_H310, 49,
    ATM_L3_TR9577, 54,
    ATM_L3_USER, 61,
    -1, -1
};

static int values_49[] = { /* ipi_high */
    0x40, 56,
    0x0, 59,
    -2, 59,
    -1, -1
};

static int values_55[] = { /* uil2_proto */
    ATM_L2_X25_LL, 65,
    ATM_L2_X25_ML, 65,
    ATM_L2_HDLC_ARM, 65,
    ATM_L2_HDLC_NRM, 65,
    ATM_L2_HDLC_ABM, 65,
    ATM_L2_Q922, 65,
    ATM_L2_ISO7776, 65,
    ATM_L2_USER, 70,
    ATM_L2_ISO1745, 71,
    ATM_L2_Q291, 71,
    ATM_L2_LAPB, 71,
    ATM_L2_ISO8802, 71,
    ATM_L2_X75, 71,
    -2, 71,
    -1, -1
};

static int values_60[] = { /* uil3_proto */
    ATM_L3_X25, 73,
    ATM_L3_ISO8208, 73,
    ATM_L3_X223, 73,
    ATM_L3_H310, 80,
    ATM_L3_TR9577, 85,
    ATM_L3_USER, 92,
    -1, -1
};

static int values_67[] = { /* ipi_high */
    0x40, 87,
    0x0, 90,
    -2, 90,
    -1, -1
};

static int values_73[] = { /* uil2_proto */
    ATM_L2_X25_LL, 96,
    ATM_L2_X25_ML, 96,
    ATM_L2_HDLC_ARM, 96,
    ATM_L2_HDLC_NRM, 96,
    ATM_L2_HDLC_ABM, 96,
    ATM_L2_Q922, 96,
    ATM_L2_ISO7776, 96,
    ATM_L2_USER, 101,
    ATM_L2_ISO1745, 102,
    ATM_L2_Q291, 102,
    ATM_L2_LAPB, 102,
    ATM_L2_ISO8802, 102,
    ATM_L2_X75, 102,
    -2, 102,
    -1, -1
};

static int values_78[] = { /* uil3_proto */
    ATM_L3_X25, 104,
    ATM_L3_ISO8208, 104,
    ATM_L3_X223, 104,
    ATM_L3_H310, 111,
    ATM_L3_TR9577, 116,
    ATM_L3_USER, 123,
    -1, -1
};

static int values_85[] = { /* ipi_high */
    0x40, 118,
    0x0, 121,
    -2, 121,
    -1, -1
};

static int values_104[] = { /* cause */
    ATM_CV_UNALLOC, 134,
    ATM_CV_NO_ROUTE_DEST, 134,
    ATM_CV_QOS_UNAVAIL, 134,
    ATM_CV_CALL_REJ, 135,
    ATM_CV_NUM_CHANGED, 138,
    ATM_CV_REJ_CLIR, 139,
    ATM_CV_ACC_INF_DISC, 140,
    ATM_CV_INCOMP_DEST, 140,
    ATM_CV_MAND_IE_MISSING, 140,
    ATM_CV_UNKNOWN_IE, 140,
    ATM_CV_INVALID_IE, 140,
    ATM_CV_UCR_UNAVAIL_OLD, 141,
    ATM_CV_UCR_UNAVAIL_NEW, 141,
    ATM_CV_NO_SUCH_CHAN, 142,
    ATM_CV_UNKNOWN_MSG_TYPE, 143,
    ATM_CV_INCOMP_MSG, 143,
    ATM_CV_TIMER_EXP, 144,
    0, 145,
    -2, 145,
    -1, -1
};

static int values_109[] = { /* reason */
    ATM_RSN_USER, 136,
    ATM_RSN_IE_MISS, 137,
    ATM_RSN_IE_INSUFF, 137,
    -1, -1
};

static int values_122[] = { /* cause */
    ATM_CV_UNALLOC, 147,
    ATM_CV_NO_ROUTE_DEST, 147,
    ATM_CV_QOS_UNAVAIL, 147,
    ATM_CV_CALL_REJ, 148,
    ATM_CV_NUM_CHANGED, 151,
    ATM_CV_REJ_CLIR, 152,
    ATM_CV_ACC_INF_DISC, 153,
    ATM_CV_INCOMP_DEST, 153,
    ATM_CV_MAND_IE_MISSING, 153,
    ATM_CV_UNKNOWN_IE, 153,
    ATM_CV_INVALID_IE, 153,
    ATM_CV_UCR_UNAVAIL_OLD, 154,
    ATM_CV_UCR_UNAVAIL_NEW, 154,
    ATM_CV_NO_SUCH_CHAN, 155,
    ATM_CV_UNKNOWN_MSG_TYPE, 156,
    ATM_CV_INCOMP_MSG, 156,
    ATM_CV_TIMER_EXP, 157,
    0, 158,
    -2, 158,
    -1, -1
};

static int values_127[] = { /* reason */
    ATM_RSN_USER, 149,
    ATM_RSN_IE_MISS, 150,
    ATM_RSN_IE_INSUFF, 150,
    -1, -1
};

static int values_155[] = { /* id_std_app */
    ATM_IRS_DSMCC, 172,
    ATM_IRS_H245, 172,
    0, 173,
    -2, 173,
    -1, -1
};

static int values_159[] = { /* id_std_app */
    ATM_IRS_DSMCC, 175,
    ATM_IRS_H245, 175,
    0, 176,
    -2, 176,
    -1, -1
};

static int values_163[] = { /* id_std_app */
    ATM_IRS_DSMCC, 178,
    ATM_IRS_H245, 178,
    0, 179,
    -2, 179,
    -1, -1
};


/*
 * Various information about fields.
 */

typedef struct {
    int parent;
    int pos,size;
    int *values;
    int actual;
} FIELD;

static FIELD fields[] = {
    { 0, 16, 24, NULL, -1 }, /* call_ref */
    { 0, 40, 8, NULL, -1 }, /* msg_type */
    { 0, 56, 16, NULL, -1 }, /* msg_len */
    { 1, 104, 8, values_3, -1 }, /* aal_type */
    { 3, 120, 16, NULL, -1 }, /* fw_max_sdu */
    { 4, 144, 16, NULL, -1 }, /* bw_max_sdu */
    { 5, 168, 8, NULL, -1 }, /* aal_mode */
    { 6, 184, 8, NULL, -1 }, /* sscs_type */
    { 8, 232, 24, NULL, -1 }, /* fw_pcr_0 */
    { 9, 264, 24, NULL, -1 }, /* bw_pcr_0 */
    { 10, 296, 24, NULL, -1 }, /* fw_pcr_01 */
    { 11, 328, 24, NULL, -1 }, /* bw_pcr_01 */
    { 12, 360, 24, NULL, -1 }, /* fw_scr_0 */
    { 13, 392, 24, NULL, -1 }, /* bw_scr_0 */
    { 14, 424, 24, NULL, -1 }, /* fw_scr_01 */
    { 15, 456, 24, NULL, -1 }, /* bw_scr_01 */
    { 16, 488, 24, NULL, -1 }, /* fw_mbs_0 */
    { 17, 520, 24, NULL, -1 }, /* bw_mbs_0 */
    { 18, 552, 24, NULL, -1 }, /* fw_mbs_01 */
    { 19, 584, 24, NULL, -1 }, /* bw_mbs_01 */
    { 20, 616, 0, NULL, -1 }, /* best_effort */
    { 21, 631, 1, NULL, -1 }, /* fw_fdisc */
    { 21, 630, 1, NULL, -1 }, /* bw_fdisc */
    { 21, 625, 1, NULL, -1 }, /* bw_tag */
    { 21, 624, 1, NULL, -1 }, /* fw_tag */
    { 22, 664, 5, NULL, -1 }, /* bearer_class */
    { 23, 672, 7, NULL, -1 }, /* trans_cap */
    { 22, 693, 2, NULL, -1 }, /* susc_clip */
    { 22, 688, 2, NULL, -1 }, /* upcc */
    { 25, 728, 7, values_29, -1 }, /* hli_type */
    { 26, 736, 64, NULL, 0 }, /* iso_hli */
    { 27, 800, 64, NULL, 1 }, /* user_hli */
    { 28, 864, 32, NULL, -1 }, /* hlp */
    { 29, 896, 24, NULL, -1 }, /* hli_oui */
    { 29, 920, 32, NULL, -1 }, /* app_id */
    { 30, 952, 8, NULL, -1 }, /* tobedefined */
    { 32, 992, 5, NULL, -1 }, /* uil1_proto */
    { 33, 1000, 5, values_37, -1 }, /* uil2_proto */
    { 35, 1013, 2, NULL, -1 }, /* l2_mode */
    { 35, 1008, 2, NULL, -1 }, /* q933 */
    { 36, 1016, 7, NULL, -1 }, /* window_size */
    { 39, 1048, 7, NULL, -1 }, /* user_l2 */
    { 41, 1064, 5, values_42, -1 }, /* uil3_proto */
    { 43, 1077, 2, NULL, -1 }, /* l3_mode */
    { 44, 1080, 4, NULL, -1 }, /* def_pck_size */
    { 45, 1096, 7, NULL, -1 }, /* pck_win_size */
    { 50, 1136, 4, NULL, -1 }, /* term_type */
    { 51, 1147, 3, NULL, -1 }, /* fw_mpx_cap */
    { 51, 1144, 3, NULL, -1 }, /* bw_mpx_cap */
    { 55, 1176, 7, values_49, -1 }, /* ipi_high */
    { 57, 1200, 24, NULL, -1 }, /* oui */
    { 57, 1224, 16, NULL, -1 }, /* pid */
    { 59, 1254, 1, NULL, -1 }, /* ipi_low */
    { 61, 1272, 7, NULL, -1 }, /* user_l3 */
    { 63, 1312, 5, NULL, -1 }, /* uil1_proto */
    { 64, 1320, 5, values_55, -1 }, /* uil2_proto */
    { 66, 1333, 2, NULL, -1 }, /* l2_mode */
    { 66, 1328, 2, NULL, -1 }, /* q933 */
    { 67, 1336, 7, NULL, -1 }, /* window_size */
    { 70, 1368, 7, NULL, -1 }, /* user_l2 */
    { 72, 1384, 5, values_60, -1 }, /* uil3_proto */
    { 74, 1397, 2, NULL, -1 }, /* l3_mode */
    { 75, 1400, 4, NULL, -1 }, /* def_pck_size */
    { 76, 1416, 7, NULL, -1 }, /* pck_win_size */
    { 81, 1456, 4, NULL, -1 }, /* term_type */
    { 82, 1467, 3, NULL, -1 }, /* fw_mpx_cap */
    { 82, 1464, 3, NULL, -1 }, /* bw_mpx_cap */
    { 86, 1496, 7, values_67, -1 }, /* ipi_high */
    { 88, 1520, 24, NULL, -1 }, /* oui */
    { 88, 1544, 16, NULL, -1 }, /* pid */
    { 90, 1574, 1, NULL, -1 }, /* ipi_low */
    { 92, 1592, 7, NULL, -1 }, /* user_l3 */
    { 94, 1632, 5, NULL, -1 }, /* uil1_proto */
    { 95, 1640, 5, values_73, -1 }, /* uil2_proto */
    { 97, 1653, 2, NULL, -1 }, /* l2_mode */
    { 97, 1648, 2, NULL, -1 }, /* q933 */
    { 98, 1656, 7, NULL, -1 }, /* window_size */
    { 101, 1688, 7, NULL, -1 }, /* user_l2 */
    { 103, 1704, 5, values_78, -1 }, /* uil3_proto */
    { 105, 1717, 2, NULL, -1 }, /* l3_mode */
    { 106, 1720, 4, NULL, -1 }, /* def_pck_size */
    { 107, 1736, 7, NULL, -1 }, /* pck_win_size */
    { 112, 1776, 4, NULL, -1 }, /* term_type */
    { 113, 1787, 3, NULL, -1 }, /* fw_mpx_cap */
    { 113, 1784, 3, NULL, -1 }, /* bw_mpx_cap */
    { 117, 1816, 7, values_85, -1 }, /* ipi_high */
    { 119, 1840, 24, NULL, -1 }, /* oui */
    { 119, 1864, 16, NULL, -1 }, /* pid */
    { 121, 1894, 1, NULL, -1 }, /* ipi_low */
    { 123, 1912, 7, NULL, -1 }, /* user_l3 */
    { 124, 1952, 6, NULL, -1 }, /* call_state */
    { 126, 2000, 96, NULL, 2 }, /* cdpn_e164 */
    { 127, 2104, 160, NULL, 3 }, /* cdpn_esa */
    { 128, 2300, 3, NULL, -1 }, /* cdps_type */
    { 128, 2304, 160, NULL, 4 }, /* cdps */
    { 129, 2496, 4, NULL, -1 }, /* cgpn_plan */
    { 129, 2500, 3, NULL, -1 }, /* cgpn_type */
    { 130, 2509, 2, NULL, -1 }, /* pres_ind */
    { 130, 2504, 2, NULL, -1 }, /* scr_ind */
    { 129, 2520, 160, NULL, 5 }, /* cgpn */
    { 132, 2716, 3, NULL, -1 }, /* cgps_type */
    { 132, 2720, 160, NULL, 6 }, /* cgps */
    { 133, 2893, 2, NULL, -1 }, /* cause_cs */
    { 133, 2912, 4, NULL, -1 }, /* location */
    { 133, 2920, 7, values_104, -1 }, /* cause */
    { 134, 2931, 1, NULL, -1 }, /* pu */
    { 134, 2930, 1, NULL, -1 }, /* na */
    { 134, 2928, 2, NULL, -1 }, /* cond2 */
    { 135, 2936, 2, NULL, -1 }, /* cond3 */
    { 135, 2938, 5, values_109, -1 }, /* reason */
    { 136, 2944, 216, NULL, 7 }, /* user_diag */
    { 137, 3160, 8, NULL, -1 }, /* ie_id3 */
    { 138, 3168, 224, NULL, 8 }, /* new_dest */
    { 139, 3392, 8, NULL, -1 }, /* invalid */
    { 140, 3400, 224, NULL, 9 }, /* ie_id6 */
    { 141, 3624, 224, NULL, 10 }, /* ucr_id */
    { 142, 3848, 16, NULL, -1 }, /* unav_vpci */
    { 142, 3864, 16, NULL, -1 }, /* unav_vci */
    { 143, 3880, 8, NULL, -1 }, /* bad_msg_type */
    { 144, 3888, 24, NULL, -1 }, /* timer */
    { 146, 3925, 2, NULL, -1 }, /* cause_cs */
    { 146, 3944, 4, NULL, -1 }, /* location */
    { 146, 3952, 7, values_122, -1 }, /* cause */
    { 147, 3963, 1, NULL, -1 }, /* pu */
    { 147, 3962, 1, NULL, -1 }, /* na */
    { 147, 3960, 2, NULL, -1 }, /* cond2 */
    { 148, 3968, 2, NULL, -1 }, /* cond3 */
    { 148, 3970, 5, values_127, -1 }, /* reason */
    { 149, 3976, 216, NULL, 11 }, /* user_diag */
    { 150, 4192, 8, NULL, -1 }, /* ie_id3 */
    { 151, 4200, 224, NULL, 12 }, /* new_dest */
    { 152, 4424, 8, NULL, -1 }, /* invalid */
    { 153, 4432, 224, NULL, 13 }, /* ie_id6 */
    { 154, 4656, 224, NULL, 14 }, /* ucr_id */
    { 155, 4880, 16, NULL, -1 }, /* unav_vpci */
    { 155, 4896, 16, NULL, -1 }, /* unav_vci */
    { 156, 4912, 8, NULL, -1 }, /* bad_msg_type */
    { 157, 4920, 24, NULL, -1 }, /* timer */
    { 159, 4984, 16, NULL, -1 }, /* vpi */
    { 159, 5000, 16, NULL, -1 }, /* vci */
    { 161, 5056, 16, NULL, -1 }, /* cum_delay */
    { 162, 5080, 16, NULL, -1 }, /* max_delay */
    { 164, 5117, 2, NULL, -1 }, /* qos_cs */
    { 164, 5136, 8, NULL, -1 }, /* qos_fw */
    { 164, 5144, 8, NULL, -1 }, /* qos_bw */
    { 165, 5184, 4, NULL, -1 }, /* rep_ind */
    { 166, 5224, 3, NULL, -1 }, /* rst_class */
    { 167, 5264, 7, NULL, -1 }, /* bbsc_ind */
    { 168, 5312, 32, NULL, 15 }, /* net_id */
    { 169, 5376, 32, NULL, 16 }, /* notification */
    { 170, 5445, 2, NULL, -1 }, /* shaping */
    { 170, 5444, 1, NULL, -1 }, /* compliance */
    { 170, 5440, 3, NULL, -1 }, /* fault */
    { 170, 5452, 3, NULL, -1 }, /* fwd_ofi */
    { 170, 5448, 3, NULL, -1 }, /* bwd_ofi */
    { 171, 5496, 7, values_155, -1 }, /* id_std_app */
    { 172, 5520, 160, NULL, 17 }, /* session_id */
    { 172, 5696, 32, NULL, 18 }, /* resource_id */
    { 173, 5728, 224, NULL, 19 }, /* unrecognized_git_identifiers */
    { 174, 5992, 7, values_159, -1 }, /* id_std_app */
    { 175, 6016, 160, NULL, 20 }, /* session_id */
    { 175, 6192, 32, NULL, 21 }, /* resource_id */
    { 176, 6224, 224, NULL, 22 }, /* unrecognized_git_identifiers */
    { 177, 6488, 7, values_163, -1 }, /* id_std_app */
    { 178, 6512, 160, NULL, 23 }, /* session_id */
    { 178, 6688, 32, NULL, 24 }, /* resource_id */
    { 179, 6720, 224, NULL, 25 }, /* unrecognized_git_identifiers */
    { 180, 6976, 7, NULL, -1 }, /* lij_id_type */
    { 180, 6984, 32, NULL, -1 }, /* lij_id */
    { 181, 7048, 2, NULL, -1 }, /* lij_scr_ind */
    { 182, 7088, 32, NULL, -1 }, /* leaf_sn */
    { 183, 7152, 4, NULL, -1 }, /* scope_type */
    { 183, 7160, 8, NULL, -1 }, /* scope_sel */
    { 185, 7208, 24, NULL, -1 }, /* altfw_pcr_0 */
    { 186, 7240, 24, NULL, -1 }, /* altbw_pcr_0 */
    { 187, 7272, 24, NULL, -1 }, /* altfw_pcr_01 */
    { 188, 7304, 24, NULL, -1 }, /* altbw_pcr_01 */
    { 189, 7336, 24, NULL, -1 }, /* altfw_scr_0 */
    { 190, 7368, 24, NULL, -1 }, /* altbw_scr_0 */
    { 191, 7400, 24, NULL, -1 }, /* altfw_scr_01 */
    { 192, 7432, 24, NULL, -1 }, /* altbw_scr_01 */
    { 193, 7464, 24, NULL, -1 }, /* altfw_mbs_0 */
    { 194, 7496, 24, NULL, -1 }, /* altbw_mbs_0 */
    { 195, 7528, 24, NULL, -1 }, /* altfw_mbs_01 */
    { 196, 7560, 24, NULL, -1 }, /* altbw_mbs_01 */
    { 197, 7592, 0, NULL, -1 }, /* altbest_effort */
    { 199, 7632, 24, NULL, -1 }, /* minfw_pcr_0 */
    { 200, 7664, 24, NULL, -1 }, /* minbw_pcr_0 */
    { 201, 7696, 24, NULL, -1 }, /* minfw_pcr_01 */
    { 202, 7728, 24, NULL, -1 }, /* minbw_pcr_01 */
    { 203, 7760, 24, NULL, -1 }, /* minfw_mcr_01 */
    { 204, 7792, 24, NULL, -1 }, /* minbw_mcr_01 */
    { 205, 7848, 8, NULL, -1 }, /* eqos_origin */
    { 206, 7864, 24, NULL, -1 }, /* acc_fw_cdv */
    { 207, 7896, 24, NULL, -1 }, /* acc_bw_cdv */
    { 208, 7928, 24, NULL, -1 }, /* cum_fw_cdv */
    { 209, 7960, 24, NULL, -1 }, /* cum_bw_cdv */
    { 210, 7992, 8, NULL, -1 }, /* acc_fw_clr */
    { 211, 8008, 8, NULL, -1 }, /* acc_bw_clr */
    { 213, 8056, 32, NULL, -1 }, /* abr_fw_add_rec */
    { 214, 8096, 32, NULL, -1 }, /* abr_bw_add_rec */
    { 216, 8168, 24, NULL, -1 }, /* abr_fw_icr */
    { 217, 8200, 24, NULL, -1 }, /* abr_bw_icr */
    { 218, 8232, 24, NULL, -1 }, /* abr_fw_tbe */
    { 219, 8264, 24, NULL, -1 }, /* abr_bw_tbe */
    { 220, 8296, 24, NULL, -1 }, /* atm_crf_rtt */
    { 221, 8328, 8, NULL, -1 }, /* atm_fw_rif */
    { 222, 8344, 8, NULL, -1 }, /* atm_bw_rif */
    { 223, 8360, 8, NULL, -1 }, /* atm_fw_rdf */
    { 224, 8376, 8, NULL, -1 }, /* atm_bw_rdf */
    { 225, 8424, 16, NULL, -1 }, /* ep_ref */
    { 226, 8472, 6, NULL, -1 }, /* ep_state */
    { 227, 8512, 8, NULL, -1 }, /* type_of_report */
};


/*
 * "Microcode" used to parse messages. It detects the
 * presence of fields and copies them from the message to the
 * construction area.
 */

static int parse[] = {
    OP_DUMP, 0,
/*   2*/    OP_COPY, 1, 0, 8, /* _pdsc */
/*   6*/    OP_DUMP, 1,
/*   8*/    OP_COPY, 1, 8, 8, /* _cr_len */
/*  12*/    OP_DUMP, 2,
/*  14*/    OP_COPY, 3, 16, 24, /* call_ref */
/*  18*/    OP_DUMP, 3,
/*  20*/    OP_COPY, 1, 40, 8, /* msg_type */
/*  24*/    OP_DUMP, 4,
/*  26*/    OP_COPY, 0, 55, 1, /* _ext */
/*  30*/    OP_DUMP, 5,
/*  32*/    OP_COPY, 0, 52, 1, /* _flag */
/*  36*/    OP_DUMP, 6,
/*  38*/    OP_COPY, 1, 48, 2, /* _action_ind */
/*  42*/    OP_DUMP, 7,
/*  44*/    OP_BEGIN_LEN, 2, 56, 16, /* msg_len */
/*  48*/    OP_IFEND, 5086, /* _ie_id */
/*  50*/    OP_DUMP, 8,
/*  52*/    OP_COPY, 0, 0, 8, /* _ie_id */
/*  56*/    OP_CASE, 1, 0, 8, 39, /* _ie_id */
/*  61*/    ATM_IE_AAL, 1, 114,
/*  64*/    ATM_IE_TD, 7, 220,
/*  67*/    ATM_IE_BBCAP, 22, 503,
/*  70*/    ATM_IE_BHLI, 25, 593,
/*  73*/    ATM_IE_BLLI, 31, 704,
/*  76*/    ATM_IE_BLLI, 62, 1220,
/*  79*/    ATM_IE_BLLI, 93, 1736,
/*  82*/    ATM_IE_CALL_STATE, 124, 2252,
/*  85*/    ATM_IE_CDPN, 125, 2293,
/*  88*/    ATM_IE_CDPS, 128, 2377,
/*  91*/    ATM_IE_CGPN, 129, 2436,
/*  94*/    ATM_IE_CGPS, 132, 2526,
/*  97*/    ATM_IE_CAUSE, 133, 2585,
/* 100*/    ATM_IE_CAUSE, 146, 2857,
/* 103*/    ATM_IE_CONN_ID, 159, 3129,
/* 106*/    ATM_IE_E2E_TDL, 160, 3194,
/* 109*/    ATM_IE_QOS, 164, 3269,
/* 112*/    ATM_IE_BBREP, 165, 3316,
/* 115*/    ATM_IE_RESTART, 166, 3363,
/* 118*/    ATM_IE_BBS_COMP, 167, 3410,
/* 121*/    ATM_IE_TNS, 168, 3457,
/* 124*/    ATM_IE_NOTIFY, 169, 3516,
/* 127*/    ATM_IE_OAM_TD, 170, 3557,
/* 130*/    ATM_IE_GIT, 171, 3634,
/* 133*/    ATM_IE_GIT, 174, 3741,
/* 136*/    ATM_IE_GIT, 177, 3848,
/* 139*/    ATM_IE_LIJ_ID, 180, 3955,
/* 142*/    ATM_IE_LIJ_PRM, 181, 4008,
/* 145*/    ATM_IE_LEAF_SN, 182, 4055,
/* 148*/    ATM_IE_SCOPE_SEL, 183, 4096,
/* 151*/    ATM_IE_ALT_TD, 184, 4149,
/* 154*/    ATM_IE_MIN_TD, 198, 4338,
/* 157*/    ATM_IE_EQOS, 205, 4452,
/* 160*/    ATM_IE_ABR_ADD_PRM, 212, 4572,
/* 163*/    ATM_IE_ABR_SET_PRM, 215, 4642,
/* 166*/    ATM_IE_EPR, 225, 4789,
/* 169*/    ATM_IE_EP_STATE, 226, 4836,
/* 172*/    ATM_IE_BBRT, 227, 4877,
/* 175*/    -1, 228, 4918,
/* 178*/    OP_DUMP, 9,
/* 180*/    OP_COPY, 0, 87, 1, /* _ext */
/* 184*/    OP_DUMP, 10,
/* 186*/    OP_COPY, 0, 85, 2, /* _cs */
/* 190*/    OP_DUMP, 11,
/* 192*/    OP_COPY, 0, 84, 1, /* _flag */
/* 196*/    OP_DUMP, 12,
/* 198*/    OP_COPY, 1, 80, 3, /* _action_ind */
/* 202*/    OP_DUMP, 13,
/* 204*/    OP_BEGIN_LEN, 2, 88, 16, /* _ie_len */
/* 208*/    OP_BEGIN_REC, RECOV_IND_IE, 1, 284,
/* 212*/    OP_DUMP, 14,
/* 214*/    OP_COPY, 0, 104, 8, /* aal_type */
/* 218*/    OP_CASE, 1, 104, 8, 1, /* aal_type */
/* 223*/    5, 2, 0,
/* 226*/    OP_IFEND, 55, /* _id */
/* 228*/    OP_DUMP, 15,
/* 230*/    OP_COPY, 0, 0, 8, /* _id */
/* 234*/    OP_CASE, 1, 0, 8, 4, /* _id */
/* 239*/    ATM_AALP_FW_MAX_SDU, 3, 9,
/* 242*/    ATM_AALP_BW_MAX_SDU, 4, 14,
/* 245*/    ATM_AALP_AAL_MODE, 5, 19,
/* 248*/    ATM_AALP_SSCS, 6, 24,
/* 251*/    OP_DUMP, 16,
/* 253*/    OP_COPY, 2, 120, 16, /* fw_max_sdu */
/* 257*/    OP_JUMP, 22,
/* 259*/    OP_DUMP, 17,
/* 261*/    OP_COPY, 2, 144, 16, /* bw_max_sdu */
/* 265*/    OP_JUMP, 14,
/* 267*/    OP_DUMP, 18,
/* 269*/    OP_COPY, 1, 168, 8, /* aal_mode */
/* 273*/    OP_JUMP, 6,
/* 275*/    OP_DUMP, 19,
/* 277*/    OP_COPY, 1, 184, 8, /* sscs_type */
/* 281*/    OP_JUMP, -57,
/* 283*/    OP_END_REC,
/* 284*/    OP_END_LEN,  /* _ie_len */
/* 285*/    OP_JUMP, 4847,
/* 287*/    OP_DUMP, 20,
/* 289*/    OP_COPY, 0, 207, 1, /* _ext */
/* 293*/    OP_DUMP, 21,
/* 295*/    OP_COPY, 0, 205, 2, /* _cs */
/* 299*/    OP_DUMP, 22,
/* 301*/    OP_COPY, 0, 204, 1, /* _flag */
/* 305*/    OP_DUMP, 23,
/* 307*/    OP_COPY, 1, 200, 3, /* _action_ind */
/* 311*/    OP_DUMP, 24,
/* 313*/    OP_BEGIN_LEN, 2, 208, 16, /* _ie_len */
/* 317*/    OP_BEGIN_REC, RECOV_IND_IE, 7, 570,
/* 321*/    OP_IFEND, 246, /* _id */
/* 323*/    OP_DUMP, 25,
/* 325*/    OP_COPY, 0, 0, 8, /* _id */
/* 329*/    OP_CASE, 1, 0, 8, 14, /* _id */
/* 334*/    ATM_TD_FW_PCR_0, 8, 39,
/* 337*/    ATM_TD_BW_PCR_0, 9, 49,
/* 340*/    ATM_TD_FW_PCR_01, 10, 59,
/* 343*/    ATM_TD_BW_PCR_01, 11, 69,
/* 346*/    ATM_TD_FW_SCR_0, 12, 79,
/* 349*/    ATM_TD_BW_SCR_0, 13, 89,
/* 352*/    ATM_TD_FW_SCR_01, 14, 99,
/* 355*/    ATM_TD_BW_SCR_01, 15, 109,
/* 358*/    ATM_TD_FW_MBS_0, 16, 119,
/* 361*/    ATM_TD_BW_MBS_0, 17, 129,
/* 364*/    ATM_TD_FW_MBS_01, 18, 139,
/* 367*/    ATM_TD_BW_MBS_01, 19, 149,
/* 370*/    ATM_TD_BEST_EFFORT, 20, 159,
/* 373*/    ATM_TD_TM_OPT, 21, 167,
/* 376*/    OP_BEGIN_LEN, 0, 232, 0, /* _dummy */
/* 380*/    OP_END_LEN,  /* _dummy */
/* 381*/    OP_DUMP, 27,
/* 383*/    OP_COPY, 3, 232, 24, /* fw_pcr_0 */
/* 387*/    OP_JUMP, 178,
/* 389*/    OP_BEGIN_LEN, 0, 264, 0, /* _dummy */
/* 393*/    OP_END_LEN,  /* _dummy */
/* 394*/    OP_DUMP, 29,
/* 396*/    OP_COPY, 3, 264, 24, /* bw_pcr_0 */
/* 400*/    OP_JUMP, 165,
/* 402*/    OP_BEGIN_LEN, 0, 296, 0, /* _dummy */
/* 406*/    OP_END_LEN,  /* _dummy */
/* 407*/    OP_DUMP, 31,
/* 409*/    OP_COPY, 3, 296, 24, /* fw_pcr_01 */
/* 413*/    OP_JUMP, 152,
/* 415*/    OP_BEGIN_LEN, 0, 328, 0, /* _dummy */
/* 419*/    OP_END_LEN,  /* _dummy */
/* 420*/    OP_DUMP, 33,
/* 422*/    OP_COPY, 3, 328, 24, /* bw_pcr_01 */
/* 426*/    OP_JUMP, 139,
/* 428*/    OP_BEGIN_LEN, 0, 360, 0, /* _dummy */
/* 432*/    OP_END_LEN,  /* _dummy */
/* 433*/    OP_DUMP, 35,
/* 435*/    OP_COPY, 3, 360, 24, /* fw_scr_0 */
/* 439*/    OP_JUMP, 126,
/* 441*/    OP_BEGIN_LEN, 0, 392, 0, /* _dummy */
/* 445*/    OP_END_LEN,  /* _dummy */
/* 446*/    OP_DUMP, 37,
/* 448*/    OP_COPY, 3, 392, 24, /* bw_scr_0 */
/* 452*/    OP_JUMP, 113,
/* 454*/    OP_BEGIN_LEN, 0, 424, 0, /* _dummy */
/* 458*/    OP_END_LEN,  /* _dummy */
/* 459*/    OP_DUMP, 39,
/* 461*/    OP_COPY, 3, 424, 24, /* fw_scr_01 */
/* 465*/    OP_JUMP, 100,
/* 467*/    OP_BEGIN_LEN, 0, 456, 0, /* _dummy */
/* 471*/    OP_END_LEN,  /* _dummy */
/* 472*/    OP_DUMP, 41,
/* 474*/    OP_COPY, 3, 456, 24, /* bw_scr_01 */
/* 478*/    OP_JUMP, 87,
/* 480*/    OP_BEGIN_LEN, 0, 488, 0, /* _dummy */
/* 484*/    OP_END_LEN,  /* _dummy */
/* 485*/    OP_DUMP, 43,
/* 487*/    OP_COPY, 3, 488, 24, /* fw_mbs_0 */
/* 491*/    OP_JUMP, 74,
/* 493*/    OP_BEGIN_LEN, 0, 520, 0, /* _dummy */
/* 497*/    OP_END_LEN,  /* _dummy */
/* 498*/    OP_DUMP, 45,
/* 500*/    OP_COPY, 3, 520, 24, /* bw_mbs_0 */
/* 504*/    OP_JUMP, 61,
/* 506*/    OP_BEGIN_LEN, 0, 552, 0, /* _dummy */
/* 510*/    OP_END_LEN,  /* _dummy */
/* 511*/    OP_DUMP, 47,
/* 513*/    OP_COPY, 3, 552, 24, /* fw_mbs_01 */
/* 517*/    OP_JUMP, 48,
/* 519*/    OP_BEGIN_LEN, 0, 584, 0, /* _dummy */
/* 523*/    OP_END_LEN,  /* _dummy */
/* 524*/    OP_DUMP, 49,
/* 526*/    OP_COPY, 3, 584, 24, /* bw_mbs_01 */
/* 530*/    OP_JUMP, 35,
/* 532*/    OP_BEGIN_LEN, 0, 616, 0, /* _dummy */
/* 536*/    OP_END_LEN,  /* _dummy */
/* 537*/    OP_COPY, 0, 616, 0, /* best_effort */
/* 541*/    OP_JUMP, 24,
/* 543*/    OP_DUMP, 52,
/* 545*/    OP_COPY, 0, 631, 1, /* fw_fdisc */
/* 549*/    OP_DUMP, 53,
/* 551*/    OP_COPY, 0, 630, 1, /* bw_fdisc */
/* 555*/    OP_DUMP, 54,
/* 557*/    OP_COPY, 0, 625, 1, /* bw_tag */
/* 561*/    OP_DUMP, 55,
/* 563*/    OP_COPY, 1, 624, 1, /* fw_tag */
/* 567*/    OP_JUMP, -248,
/* 569*/    OP_END_REC,
/* 570*/    OP_END_LEN,  /* _ie_len */
/* 571*/    OP_JUMP, 4561,
/* 573*/    OP_DUMP, 56,
/* 575*/    OP_COPY, 0, 647, 1, /* _ext */
/* 579*/    OP_DUMP, 57,
/* 581*/    OP_COPY, 0, 645, 2, /* _cs */
/* 585*/    OP_DUMP, 58,
/* 587*/    OP_COPY, 0, 644, 1, /* _flag */
/* 591*/    OP_DUMP, 59,
/* 593*/    OP_COPY, 1, 640, 3, /* _action_ind */
/* 597*/    OP_DUMP, 60,
/* 599*/    OP_BEGIN_LEN, 2, 648, 16, /* _ie_len */
/* 603*/    OP_BEGIN_REC, RECOV_IND_IE, 22, 663,
/* 607*/    OP_DUMP, 61,
/* 609*/    OP_COPY, 0, 664, 5, /* bearer_class */
/* 613*/    OP_DUMP, 62,
/* 615*/    OP_COPY, 0, 7, 1, /* _ext */
/* 619*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/* 624*/    0, 23, 3,
/* 627*/    -1, 24, 14,
/* 630*/    OP_DUMP, 63,
/* 632*/    OP_COPY, 0, 679, 1, /* _ext */
/* 636*/    OP_DUMP, 64,
/* 638*/    OP_COPY, 1, 672, 7, /* trans_cap */
/* 642*/    OP_JUMP, 0,
/* 644*/    OP_DUMP, 65,
/* 646*/    OP_COPY, 0, 695, 1, /* _ext */
/* 650*/    OP_DUMP, 66,
/* 652*/    OP_COPY, 0, 693, 2, /* susc_clip */
/* 656*/    OP_DUMP, 67,
/* 658*/    OP_COPY, 1, 688, 2, /* upcc */
/* 662*/    OP_END_REC,
/* 663*/    OP_END_LEN,  /* _ie_len */
/* 664*/    OP_JUMP, 4468,
/* 666*/    OP_DUMP, 68,
/* 668*/    OP_COPY, 0, 711, 1, /* _ext */
/* 672*/    OP_DUMP, 69,
/* 674*/    OP_COPY, 0, 709, 2, /* _cs */
/* 678*/    OP_DUMP, 70,
/* 680*/    OP_COPY, 0, 708, 1, /* _flag */
/* 684*/    OP_DUMP, 71,
/* 686*/    OP_COPY, 1, 704, 3, /* _action_ind */
/* 690*/    OP_DUMP, 72,
/* 692*/    OP_BEGIN_LEN, 2, 712, 16, /* _ie_len */
/* 696*/    OP_BEGIN_REC, RECOV_IND_IE, 25, 777,
/* 700*/    OP_DUMP, 73,
/* 702*/    OP_COPY, 0, 735, 1, /* _ext */
/* 706*/    OP_DUMP, 74,
/* 708*/    OP_COPY, 0, 728, 7, /* hli_type */
/* 712*/    OP_CASE, 1, 728, 7, 5, /* hli_type */
/* 717*/    0, 26, 12,
/* 720*/    1, 27, 17,
/* 723*/    2, 28, 22,
/* 726*/    3, 29, 27,
/* 729*/    4, 30, 38,
/* 732*/    OP_DUMP, 75,
/* 734*/    OP_COPYVAR, 0, 736, 8, /* iso_hli */
/* 738*/    OP_JUMP, 36,
/* 740*/    OP_DUMP, 76,
/* 742*/    OP_COPYVAR, 1, 800, 8, /* user_hli */
/* 746*/    OP_JUMP, 28,
/* 748*/    OP_DUMP, 77,
/* 750*/    OP_COPY, 4, 864, 32, /* hlp */
/* 754*/    OP_JUMP, 20,
/* 756*/    OP_DUMP, 78,
/* 758*/    OP_COPY, 3, 896, 24, /* hli_oui */
/* 762*/    OP_DUMP, 79,
/* 764*/    OP_COPY, 4, 920, 32, /* app_id */
/* 768*/    OP_JUMP, 6,
/* 770*/    OP_DUMP, 80,
/* 772*/    OP_COPY, 1, 952, 8, /* tobedefined */
/* 776*/    OP_END_REC,
/* 777*/    OP_END_LEN,  /* _ie_len */
/* 778*/    OP_JUMP, 4354,
/* 780*/    OP_DUMP, 81,
/* 782*/    OP_COPY, 0, 975, 1, /* _ext */
/* 786*/    OP_DUMP, 82,
/* 788*/    OP_COPY, 0, 973, 2, /* _cs */
/* 792*/    OP_DUMP, 83,
/* 794*/    OP_COPY, 0, 972, 1, /* _flag */
/* 798*/    OP_DUMP, 84,
/* 800*/    OP_COPY, 1, 968, 3, /* _action_ind */
/* 804*/    OP_DUMP, 85,
/* 806*/    OP_BEGIN_LEN, 2, 976, 16, /* _ie_len */
/* 810*/    OP_BEGIN_REC, RECOV_IND_IE, 31, 1296,
/* 814*/    OP_IFEND, 479, /* _lid */
/* 816*/    OP_DUMP, 86,
/* 818*/    OP_COPY, 0, 5, 2, /* _lid */
/* 822*/    OP_CASE, 0, 5, 2, 3, /* _lid */
/* 827*/    1, 32, 6,
/* 830*/    2, 33, 17,
/* 833*/    3, 41, 156,
/* 836*/    OP_DUMP, 87,
/* 838*/    OP_COPY, 0, 999, 1, /* _ext */
/* 842*/    OP_DUMP, 88,
/* 844*/    OP_COPY, 1, 992, 5, /* uil1_proto */
/* 848*/    OP_JUMP, 443,
/* 850*/    OP_DUMP, 89,
/* 852*/    OP_COPY, 0, 1000, 5, /* uil2_proto */
/* 856*/    OP_CASE, 0, 1000, 5, 13, /* uil2_proto */
/* 861*/    ATM_L2_X25_LL, 34, 36,
/* 864*/    ATM_L2_X25_ML, 34, 33,
/* 867*/    ATM_L2_HDLC_ARM, 34, 30,
/* 870*/    ATM_L2_HDLC_NRM, 34, 27,
/* 873*/    ATM_L2_HDLC_ABM, 34, 24,
/* 876*/    ATM_L2_Q922, 34, 21,
/* 879*/    ATM_L2_ISO7776, 34, 18,
/* 882*/    ATM_L2_USER, 39, 79,
/* 885*/    -1, 40, 96,
/* 888*/    ATM_L2_Q291, 40, 93,
/* 891*/    ATM_L2_LAPB, 40, 90,
/* 894*/    ATM_L2_ISO8802, 40, 87,
/* 897*/    ATM_L2_X75, 40, 84,
/* 900*/    OP_DUMP, 90,
/* 902*/    OP_COPY, 0, 7, 1, /* _ext */
/* 906*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/* 911*/    0, 35, 3,
/* 914*/    -1, 38, 45,
/* 917*/    OP_DUMP, 91,
/* 919*/    OP_COPY, 0, 1013, 2, /* l2_mode */
/* 923*/    OP_DUMP, 92,
/* 925*/    OP_COPY, 0, 1008, 2, /* q933 */
/* 929*/    OP_DUMP, 93,
/* 931*/    OP_COPY, 0, 7, 1, /* _ext */
/* 935*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/* 940*/    0, 36, 3,
/* 943*/    -1, 37, 14,
/* 946*/    OP_DUMP, 94,
/* 948*/    OP_COPY, 0, 1016, 7, /* window_size */
/* 952*/    OP_DUMP, 95,
/* 954*/    OP_COPY, 1, 1023, 1, /* _ext */
/* 958*/    OP_JUMP, 0,
/* 960*/    OP_JUMP, 0,
/* 962*/    OP_JUMP, 26,
/* 964*/    OP_DUMP, 96,
/* 966*/    OP_COPY, 1, 1047, 1, /* _ext */
/* 970*/    OP_DUMP, 97,
/* 972*/    OP_COPY, 0, 1048, 7, /* user_l2 */
/* 976*/    OP_DUMP, 98,
/* 978*/    OP_COPY, 1, 1055, 1, /* _ext */
/* 982*/    OP_JUMP, 6,
/* 984*/    OP_DUMP, 99,
/* 986*/    OP_COPY, 1, 1063, 1, /* _ext */
/* 990*/    OP_JUMP, 301,
/* 992*/    OP_DUMP, 100,
/* 994*/    OP_COPY, 0, 1064, 5, /* uil3_proto */
/* 998*/    OP_CASE, 0, 1064, 5, 6, /* uil3_proto */
/*1003*/    ATM_L3_X25, 42, 15,
/*1006*/    ATM_L3_ISO8208, 42, 12,
/*1009*/    ATM_L3_X223, 42, 9,
/*1012*/    ATM_L3_H310, 49, 89,
/*1015*/    ATM_L3_TR9577, 54, 150,
/*1018*/    ATM_L3_USER, 61, 254,
/*1021*/    OP_DUMP, 101,
/*1023*/    OP_COPY, 0, 7, 1, /* _ext */
/*1027*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1032*/    0, 43, 3,
/*1035*/    -1, 48, 64,
/*1038*/    OP_DUMP, 102,
/*1040*/    OP_COPY, 0, 1077, 2, /* l3_mode */
/*1044*/    OP_DUMP, 103,
/*1046*/    OP_COPY, 0, 7, 1, /* _ext */
/*1050*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1055*/    0, 44, 3,
/*1058*/    -1, 47, 39,
/*1061*/    OP_DUMP, 104,
/*1063*/    OP_COPY, 0, 1080, 4, /* def_pck_size */
/*1067*/    OP_DUMP, 105,
/*1069*/    OP_COPY, 0, 7, 1, /* _ext */
/*1073*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1078*/    0, 45, 3,
/*1081*/    -1, 46, 14,
/*1084*/    OP_DUMP, 106,
/*1086*/    OP_COPY, 1, 1095, 1, /* _ext */
/*1090*/    OP_DUMP, 107,
/*1092*/    OP_COPY, 1, 1096, 7, /* pck_win_size */
/*1096*/    OP_JUMP, 0,
/*1098*/    OP_JUMP, 0,
/*1100*/    OP_JUMP, 0,
/*1102*/    OP_JUMP, 189,
/*1104*/    OP_DUMP, 108,
/*1106*/    OP_COPY, 0, 7, 1, /* _ext */
/*1110*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1115*/    0, 50, 3,
/*1118*/    -1, 53, 45,
/*1121*/    OP_DUMP, 109,
/*1123*/    OP_COPY, 0, 1136, 4, /* term_type */
/*1127*/    OP_DUMP, 110,
/*1129*/    OP_COPY, 0, 7, 1, /* _ext */
/*1133*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1138*/    0, 51, 3,
/*1141*/    -1, 52, 20,
/*1144*/    OP_DUMP, 111,
/*1146*/    OP_COPY, 0, 1151, 1, /* _ext */
/*1150*/    OP_DUMP, 112,
/*1152*/    OP_COPY, 0, 1147, 3, /* fw_mpx_cap */
/*1156*/    OP_DUMP, 113,
/*1158*/    OP_COPY, 1, 1144, 3, /* bw_mpx_cap */
/*1162*/    OP_JUMP, 0,
/*1164*/    OP_JUMP, 0,
/*1166*/    OP_JUMP, 125,
/*1168*/    OP_DUMP, 114,
/*1170*/    OP_COPY, 0, 7, 1, /* _ext */
/*1174*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1179*/    0, 55, 3,
/*1182*/    -1, 60, 88,
/*1185*/    OP_DUMP, 115,
/*1187*/    OP_COPY, 0, 1183, 1, /* _ext */
/*1191*/    OP_DUMP, 116,
/*1193*/    OP_COPY, 0, 1176, 7, /* ipi_high */
/*1197*/    OP_CASE, 1, 1176, 7, 2, /* ipi_high */
/*1202*/    0x40, 56, 3,
/*1205*/    -1, 59, 51,
/*1208*/    OP_DUMP, 117,
/*1210*/    OP_COPY, 0, 1191, 1, /* _ext */
/*1214*/    OP_DUMP, 118,
/*1216*/    OP_COPY, 0, 6, 1, /* _ipi_low */
/*1220*/    OP_CASE, 1, 6, 1, 2, /* _ipi_low */
/*1225*/    0, 57, 3,
/*1228*/    -1, 58, 26,
/*1231*/    OP_DUMP, 119,
/*1233*/    OP_COPY, 0, 1199, 1, /* _ext */
/*1237*/    OP_DUMP, 120,
/*1239*/    OP_COPY, 1, 1197, 2, /* _snap_id */
/*1243*/    OP_DUMP, 121,
/*1245*/    OP_COPY, 3, 1200, 24, /* oui */
/*1249*/    OP_DUMP, 122,
/*1251*/    OP_COPY, 2, 1224, 16, /* pid */
/*1255*/    OP_JUMP, 0,
/*1257*/    OP_JUMP, 12,
/*1259*/    OP_DUMP, 123,
/*1261*/    OP_COPY, 0, 1255, 1, /* _ext */
/*1265*/    OP_DUMP, 124,
/*1267*/    OP_COPY, 1, 1254, 1, /* ipi_low */
/*1271*/    OP_JUMP, 0,
/*1273*/    OP_JUMP, 18,
/*1275*/    OP_DUMP, 125,
/*1277*/    OP_COPY, 1, 1271, 1, /* _ext */
/*1281*/    OP_DUMP, 126,
/*1283*/    OP_COPY, 0, 1272, 7, /* user_l3 */
/*1287*/    OP_DUMP, 127,
/*1289*/    OP_COPY, 1, 1279, 1, /* _ext */
/*1293*/    OP_JUMP, -481,
/*1295*/    OP_END_REC,
/*1296*/    OP_END_LEN,  /* _ie_len */
/*1297*/    OP_JUMP, 3835,
/*1299*/    OP_DUMP, 128,
/*1301*/    OP_COPY, 0, 1295, 1, /* _ext */
/*1305*/    OP_DUMP, 129,
/*1307*/    OP_COPY, 0, 1293, 2, /* _cs */
/*1311*/    OP_DUMP, 130,
/*1313*/    OP_COPY, 0, 1292, 1, /* _flag */
/*1317*/    OP_DUMP, 131,
/*1319*/    OP_COPY, 1, 1288, 3, /* _action_ind */
/*1323*/    OP_DUMP, 132,
/*1325*/    OP_BEGIN_LEN, 2, 1296, 16, /* _ie_len */
/*1329*/    OP_BEGIN_REC, RECOV_IND_IE, 62, 1815,
/*1333*/    OP_IFEND, 479, /* _lid */
/*1335*/    OP_DUMP, 133,
/*1337*/    OP_COPY, 0, 5, 2, /* _lid */
/*1341*/    OP_CASE, 0, 5, 2, 3, /* _lid */
/*1346*/    1, 63, 6,
/*1349*/    2, 64, 17,
/*1352*/    3, 72, 156,
/*1355*/    OP_DUMP, 134,
/*1357*/    OP_COPY, 0, 1319, 1, /* _ext */
/*1361*/    OP_DUMP, 135,
/*1363*/    OP_COPY, 1, 1312, 5, /* uil1_proto */
/*1367*/    OP_JUMP, 443,
/*1369*/    OP_DUMP, 136,
/*1371*/    OP_COPY, 0, 1320, 5, /* uil2_proto */
/*1375*/    OP_CASE, 0, 1320, 5, 13, /* uil2_proto */
/*1380*/    ATM_L2_X25_LL, 65, 36,
/*1383*/    ATM_L2_X25_ML, 65, 33,
/*1386*/    ATM_L2_HDLC_ARM, 65, 30,
/*1389*/    ATM_L2_HDLC_NRM, 65, 27,
/*1392*/    ATM_L2_HDLC_ABM, 65, 24,
/*1395*/    ATM_L2_Q922, 65, 21,
/*1398*/    ATM_L2_ISO7776, 65, 18,
/*1401*/    ATM_L2_USER, 70, 79,
/*1404*/    -1, 71, 96,
/*1407*/    ATM_L2_Q291, 71, 93,
/*1410*/    ATM_L2_LAPB, 71, 90,
/*1413*/    ATM_L2_ISO8802, 71, 87,
/*1416*/    ATM_L2_X75, 71, 84,
/*1419*/    OP_DUMP, 137,
/*1421*/    OP_COPY, 0, 7, 1, /* _ext */
/*1425*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1430*/    0, 66, 3,
/*1433*/    -1, 69, 45,
/*1436*/    OP_DUMP, 138,
/*1438*/    OP_COPY, 0, 1333, 2, /* l2_mode */
/*1442*/    OP_DUMP, 139,
/*1444*/    OP_COPY, 0, 1328, 2, /* q933 */
/*1448*/    OP_DUMP, 140,
/*1450*/    OP_COPY, 0, 7, 1, /* _ext */
/*1454*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1459*/    0, 67, 3,
/*1462*/    -1, 68, 14,
/*1465*/    OP_DUMP, 141,
/*1467*/    OP_COPY, 0, 1336, 7, /* window_size */
/*1471*/    OP_DUMP, 142,
/*1473*/    OP_COPY, 1, 1343, 1, /* _ext */
/*1477*/    OP_JUMP, 0,
/*1479*/    OP_JUMP, 0,
/*1481*/    OP_JUMP, 26,
/*1483*/    OP_DUMP, 143,
/*1485*/    OP_COPY, 1, 1367, 1, /* _ext */
/*1489*/    OP_DUMP, 144,
/*1491*/    OP_COPY, 0, 1368, 7, /* user_l2 */
/*1495*/    OP_DUMP, 145,
/*1497*/    OP_COPY, 1, 1375, 1, /* _ext */
/*1501*/    OP_JUMP, 6,
/*1503*/    OP_DUMP, 146,
/*1505*/    OP_COPY, 1, 1383, 1, /* _ext */
/*1509*/    OP_JUMP, 301,
/*1511*/    OP_DUMP, 147,
/*1513*/    OP_COPY, 0, 1384, 5, /* uil3_proto */
/*1517*/    OP_CASE, 0, 1384, 5, 6, /* uil3_proto */
/*1522*/    ATM_L3_X25, 73, 15,
/*1525*/    ATM_L3_ISO8208, 73, 12,
/*1528*/    ATM_L3_X223, 73, 9,
/*1531*/    ATM_L3_H310, 80, 89,
/*1534*/    ATM_L3_TR9577, 85, 150,
/*1537*/    ATM_L3_USER, 92, 254,
/*1540*/    OP_DUMP, 148,
/*1542*/    OP_COPY, 0, 7, 1, /* _ext */
/*1546*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1551*/    0, 74, 3,
/*1554*/    -1, 79, 64,
/*1557*/    OP_DUMP, 149,
/*1559*/    OP_COPY, 0, 1397, 2, /* l3_mode */
/*1563*/    OP_DUMP, 150,
/*1565*/    OP_COPY, 0, 7, 1, /* _ext */
/*1569*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1574*/    0, 75, 3,
/*1577*/    -1, 78, 39,
/*1580*/    OP_DUMP, 151,
/*1582*/    OP_COPY, 0, 1400, 4, /* def_pck_size */
/*1586*/    OP_DUMP, 152,
/*1588*/    OP_COPY, 0, 7, 1, /* _ext */
/*1592*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1597*/    0, 76, 3,
/*1600*/    -1, 77, 14,
/*1603*/    OP_DUMP, 153,
/*1605*/    OP_COPY, 1, 1415, 1, /* _ext */
/*1609*/    OP_DUMP, 154,
/*1611*/    OP_COPY, 1, 1416, 7, /* pck_win_size */
/*1615*/    OP_JUMP, 0,
/*1617*/    OP_JUMP, 0,
/*1619*/    OP_JUMP, 0,
/*1621*/    OP_JUMP, 189,
/*1623*/    OP_DUMP, 155,
/*1625*/    OP_COPY, 0, 7, 1, /* _ext */
/*1629*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1634*/    0, 81, 3,
/*1637*/    -1, 84, 45,
/*1640*/    OP_DUMP, 156,
/*1642*/    OP_COPY, 0, 1456, 4, /* term_type */
/*1646*/    OP_DUMP, 157,
/*1648*/    OP_COPY, 0, 7, 1, /* _ext */
/*1652*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1657*/    0, 82, 3,
/*1660*/    -1, 83, 20,
/*1663*/    OP_DUMP, 158,
/*1665*/    OP_COPY, 0, 1471, 1, /* _ext */
/*1669*/    OP_DUMP, 159,
/*1671*/    OP_COPY, 0, 1467, 3, /* fw_mpx_cap */
/*1675*/    OP_DUMP, 160,
/*1677*/    OP_COPY, 1, 1464, 3, /* bw_mpx_cap */
/*1681*/    OP_JUMP, 0,
/*1683*/    OP_JUMP, 0,
/*1685*/    OP_JUMP, 125,
/*1687*/    OP_DUMP, 161,
/*1689*/    OP_COPY, 0, 7, 1, /* _ext */
/*1693*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1698*/    0, 86, 3,
/*1701*/    -1, 91, 88,
/*1704*/    OP_DUMP, 162,
/*1706*/    OP_COPY, 0, 1503, 1, /* _ext */
/*1710*/    OP_DUMP, 163,
/*1712*/    OP_COPY, 0, 1496, 7, /* ipi_high */
/*1716*/    OP_CASE, 1, 1496, 7, 2, /* ipi_high */
/*1721*/    0x40, 87, 3,
/*1724*/    -1, 90, 51,
/*1727*/    OP_DUMP, 164,
/*1729*/    OP_COPY, 0, 1511, 1, /* _ext */
/*1733*/    OP_DUMP, 165,
/*1735*/    OP_COPY, 0, 6, 1, /* _ipi_low */
/*1739*/    OP_CASE, 1, 6, 1, 2, /* _ipi_low */
/*1744*/    0, 88, 3,
/*1747*/    -1, 89, 26,
/*1750*/    OP_DUMP, 166,
/*1752*/    OP_COPY, 0, 1519, 1, /* _ext */
/*1756*/    OP_DUMP, 167,
/*1758*/    OP_COPY, 1, 1517, 2, /* _snap_id */
/*1762*/    OP_DUMP, 168,
/*1764*/    OP_COPY, 3, 1520, 24, /* oui */
/*1768*/    OP_DUMP, 169,
/*1770*/    OP_COPY, 2, 1544, 16, /* pid */
/*1774*/    OP_JUMP, 0,
/*1776*/    OP_JUMP, 12,
/*1778*/    OP_DUMP, 170,
/*1780*/    OP_COPY, 0, 1575, 1, /* _ext */
/*1784*/    OP_DUMP, 171,
/*1786*/    OP_COPY, 1, 1574, 1, /* ipi_low */
/*1790*/    OP_JUMP, 0,
/*1792*/    OP_JUMP, 18,
/*1794*/    OP_DUMP, 172,
/*1796*/    OP_COPY, 1, 1591, 1, /* _ext */
/*1800*/    OP_DUMP, 173,
/*1802*/    OP_COPY, 0, 1592, 7, /* user_l3 */
/*1806*/    OP_DUMP, 174,
/*1808*/    OP_COPY, 1, 1599, 1, /* _ext */
/*1812*/    OP_JUMP, -481,
/*1814*/    OP_END_REC,
/*1815*/    OP_END_LEN,  /* _ie_len */
/*1816*/    OP_JUMP, 3316,
/*1818*/    OP_DUMP, 175,
/*1820*/    OP_COPY, 0, 1615, 1, /* _ext */
/*1824*/    OP_DUMP, 176,
/*1826*/    OP_COPY, 0, 1613, 2, /* _cs */
/*1830*/    OP_DUMP, 177,
/*1832*/    OP_COPY, 0, 1612, 1, /* _flag */
/*1836*/    OP_DUMP, 178,
/*1838*/    OP_COPY, 1, 1608, 3, /* _action_ind */
/*1842*/    OP_DUMP, 179,
/*1844*/    OP_BEGIN_LEN, 2, 1616, 16, /* _ie_len */
/*1848*/    OP_BEGIN_REC, RECOV_IND_IE, 93, 2334,
/*1852*/    OP_IFEND, 479, /* _lid */
/*1854*/    OP_DUMP, 180,
/*1856*/    OP_COPY, 0, 5, 2, /* _lid */
/*1860*/    OP_CASE, 0, 5, 2, 3, /* _lid */
/*1865*/    1, 94, 6,
/*1868*/    2, 95, 17,
/*1871*/    3, 103, 156,
/*1874*/    OP_DUMP, 181,
/*1876*/    OP_COPY, 0, 1639, 1, /* _ext */
/*1880*/    OP_DUMP, 182,
/*1882*/    OP_COPY, 1, 1632, 5, /* uil1_proto */
/*1886*/    OP_JUMP, 443,
/*1888*/    OP_DUMP, 183,
/*1890*/    OP_COPY, 0, 1640, 5, /* uil2_proto */
/*1894*/    OP_CASE, 0, 1640, 5, 13, /* uil2_proto */
/*1899*/    ATM_L2_X25_LL, 96, 36,
/*1902*/    ATM_L2_X25_ML, 96, 33,
/*1905*/    ATM_L2_HDLC_ARM, 96, 30,
/*1908*/    ATM_L2_HDLC_NRM, 96, 27,
/*1911*/    ATM_L2_HDLC_ABM, 96, 24,
/*1914*/    ATM_L2_Q922, 96, 21,
/*1917*/    ATM_L2_ISO7776, 96, 18,
/*1920*/    ATM_L2_USER, 101, 79,
/*1923*/    -1, 102, 96,
/*1926*/    ATM_L2_Q291, 102, 93,
/*1929*/    ATM_L2_LAPB, 102, 90,
/*1932*/    ATM_L2_ISO8802, 102, 87,
/*1935*/    ATM_L2_X75, 102, 84,
/*1938*/    OP_DUMP, 184,
/*1940*/    OP_COPY, 0, 7, 1, /* _ext */
/*1944*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1949*/    0, 97, 3,
/*1952*/    -1, 100, 45,
/*1955*/    OP_DUMP, 185,
/*1957*/    OP_COPY, 0, 1653, 2, /* l2_mode */
/*1961*/    OP_DUMP, 186,
/*1963*/    OP_COPY, 0, 1648, 2, /* q933 */
/*1967*/    OP_DUMP, 187,
/*1969*/    OP_COPY, 0, 7, 1, /* _ext */
/*1973*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*1978*/    0, 98, 3,
/*1981*/    -1, 99, 14,
/*1984*/    OP_DUMP, 188,
/*1986*/    OP_COPY, 0, 1656, 7, /* window_size */
/*1990*/    OP_DUMP, 189,
/*1992*/    OP_COPY, 1, 1663, 1, /* _ext */
/*1996*/    OP_JUMP, 0,
/*1998*/    OP_JUMP, 0,
/*2000*/    OP_JUMP, 26,
/*2002*/    OP_DUMP, 190,
/*2004*/    OP_COPY, 1, 1687, 1, /* _ext */
/*2008*/    OP_DUMP, 191,
/*2010*/    OP_COPY, 0, 1688, 7, /* user_l2 */
/*2014*/    OP_DUMP, 192,
/*2016*/    OP_COPY, 1, 1695, 1, /* _ext */
/*2020*/    OP_JUMP, 6,
/*2022*/    OP_DUMP, 193,
/*2024*/    OP_COPY, 1, 1703, 1, /* _ext */
/*2028*/    OP_JUMP, 301,
/*2030*/    OP_DUMP, 194,
/*2032*/    OP_COPY, 0, 1704, 5, /* uil3_proto */
/*2036*/    OP_CASE, 0, 1704, 5, 6, /* uil3_proto */
/*2041*/    ATM_L3_X25, 104, 15,
/*2044*/    ATM_L3_ISO8208, 104, 12,
/*2047*/    ATM_L3_X223, 104, 9,
/*2050*/    ATM_L3_H310, 111, 89,
/*2053*/    ATM_L3_TR9577, 116, 150,
/*2056*/    ATM_L3_USER, 123, 254,
/*2059*/    OP_DUMP, 195,
/*2061*/    OP_COPY, 0, 7, 1, /* _ext */
/*2065*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2070*/    0, 105, 3,
/*2073*/    -1, 110, 64,
/*2076*/    OP_DUMP, 196,
/*2078*/    OP_COPY, 0, 1717, 2, /* l3_mode */
/*2082*/    OP_DUMP, 197,
/*2084*/    OP_COPY, 0, 7, 1, /* _ext */
/*2088*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2093*/    0, 106, 3,
/*2096*/    -1, 109, 39,
/*2099*/    OP_DUMP, 198,
/*2101*/    OP_COPY, 0, 1720, 4, /* def_pck_size */
/*2105*/    OP_DUMP, 199,
/*2107*/    OP_COPY, 0, 7, 1, /* _ext */
/*2111*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2116*/    0, 107, 3,
/*2119*/    -1, 108, 14,
/*2122*/    OP_DUMP, 200,
/*2124*/    OP_COPY, 1, 1735, 1, /* _ext */
/*2128*/    OP_DUMP, 201,
/*2130*/    OP_COPY, 1, 1736, 7, /* pck_win_size */
/*2134*/    OP_JUMP, 0,
/*2136*/    OP_JUMP, 0,
/*2138*/    OP_JUMP, 0,
/*2140*/    OP_JUMP, 189,
/*2142*/    OP_DUMP, 202,
/*2144*/    OP_COPY, 0, 7, 1, /* _ext */
/*2148*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2153*/    0, 112, 3,
/*2156*/    -1, 115, 45,
/*2159*/    OP_DUMP, 203,
/*2161*/    OP_COPY, 0, 1776, 4, /* term_type */
/*2165*/    OP_DUMP, 204,
/*2167*/    OP_COPY, 0, 7, 1, /* _ext */
/*2171*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2176*/    0, 113, 3,
/*2179*/    -1, 114, 20,
/*2182*/    OP_DUMP, 205,
/*2184*/    OP_COPY, 0, 1791, 1, /* _ext */
/*2188*/    OP_DUMP, 206,
/*2190*/    OP_COPY, 0, 1787, 3, /* fw_mpx_cap */
/*2194*/    OP_DUMP, 207,
/*2196*/    OP_COPY, 1, 1784, 3, /* bw_mpx_cap */
/*2200*/    OP_JUMP, 0,
/*2202*/    OP_JUMP, 0,
/*2204*/    OP_JUMP, 125,
/*2206*/    OP_DUMP, 208,
/*2208*/    OP_COPY, 0, 7, 1, /* _ext */
/*2212*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2217*/    0, 117, 3,
/*2220*/    -1, 122, 88,
/*2223*/    OP_DUMP, 209,
/*2225*/    OP_COPY, 0, 1823, 1, /* _ext */
/*2229*/    OP_DUMP, 210,
/*2231*/    OP_COPY, 0, 1816, 7, /* ipi_high */
/*2235*/    OP_CASE, 1, 1816, 7, 2, /* ipi_high */
/*2240*/    0x40, 118, 3,
/*2243*/    -1, 121, 51,
/*2246*/    OP_DUMP, 211,
/*2248*/    OP_COPY, 0, 1831, 1, /* _ext */
/*2252*/    OP_DUMP, 212,
/*2254*/    OP_COPY, 0, 6, 1, /* _ipi_low */
/*2258*/    OP_CASE, 1, 6, 1, 2, /* _ipi_low */
/*2263*/    0, 119, 3,
/*2266*/    -1, 120, 26,
/*2269*/    OP_DUMP, 213,
/*2271*/    OP_COPY, 0, 1839, 1, /* _ext */
/*2275*/    OP_DUMP, 214,
/*2277*/    OP_COPY, 1, 1837, 2, /* _snap_id */
/*2281*/    OP_DUMP, 215,
/*2283*/    OP_COPY, 3, 1840, 24, /* oui */
/*2287*/    OP_DUMP, 216,
/*2289*/    OP_COPY, 2, 1864, 16, /* pid */
/*2293*/    OP_JUMP, 0,
/*2295*/    OP_JUMP, 12,
/*2297*/    OP_DUMP, 217,
/*2299*/    OP_COPY, 0, 1895, 1, /* _ext */
/*2303*/    OP_DUMP, 218,
/*2305*/    OP_COPY, 1, 1894, 1, /* ipi_low */
/*2309*/    OP_JUMP, 0,
/*2311*/    OP_JUMP, 18,
/*2313*/    OP_DUMP, 219,
/*2315*/    OP_COPY, 1, 1911, 1, /* _ext */
/*2319*/    OP_DUMP, 220,
/*2321*/    OP_COPY, 0, 1912, 7, /* user_l3 */
/*2325*/    OP_DUMP, 221,
/*2327*/    OP_COPY, 1, 1919, 1, /* _ext */
/*2331*/    OP_JUMP, -481,
/*2333*/    OP_END_REC,
/*2334*/    OP_END_LEN,  /* _ie_len */
/*2335*/    OP_JUMP, 2797,
/*2337*/    OP_DUMP, 222,
/*2339*/    OP_COPY, 0, 1935, 1, /* _ext */
/*2343*/    OP_DUMP, 223,
/*2345*/    OP_COPY, 0, 1933, 2, /* _cs */
/*2349*/    OP_DUMP, 224,
/*2351*/    OP_COPY, 0, 1932, 1, /* _flag */
/*2355*/    OP_DUMP, 225,
/*2357*/    OP_COPY, 1, 1928, 3, /* _action_ind */
/*2361*/    OP_DUMP, 226,
/*2363*/    OP_BEGIN_LEN, 2, 1936, 16, /* _ie_len */
/*2367*/    OP_BEGIN_REC, RECOV_IND_IE, 124, 2378,
/*2371*/    OP_DUMP, 227,
/*2373*/    OP_COPY, 1, 1952, 6, /* call_state */
/*2377*/    OP_END_REC,
/*2378*/    OP_END_LEN,  /* _ie_len */
/*2379*/    OP_JUMP, 2753,
/*2381*/    OP_DUMP, 228,
/*2383*/    OP_COPY, 0, 1975, 1, /* _ext */
/*2387*/    OP_DUMP, 229,
/*2389*/    OP_COPY, 0, 1973, 2, /* _cs */
/*2393*/    OP_DUMP, 230,
/*2395*/    OP_COPY, 0, 1972, 1, /* _flag */
/*2399*/    OP_DUMP, 231,
/*2401*/    OP_COPY, 1, 1968, 3, /* _action_ind */
/*2405*/    OP_DUMP, 232,
/*2407*/    OP_BEGIN_LEN, 2, 1976, 16, /* _ie_len */
/*2411*/    OP_BEGIN_REC, RECOV_IND_IE, 125, 2465,
/*2415*/    OP_DUMP, 233,
/*2417*/    OP_COPY, 0, 1999, 1, /* _ext */
/*2421*/    OP_DUMP, 234,
/*2423*/    OP_COPY, 0, 0, 4, /* _plan */
/*2427*/    OP_CASE, 0, 0, 4, 2, /* _plan */
/*2432*/    ATM_NP_E164, 126, 3,
/*2435*/    ATM_NP_AEA, 127, 14,
/*2438*/    OP_DUMP, 235,
/*2440*/    OP_COPY, 1, 1996, 3, /* _type */
/*2444*/    OP_DUMP, 236,
/*2446*/    OP_COPYVAR, 2, 2000, 12, /* cdpn_e164 */
/*2450*/    OP_JUMP, 12,
/*2452*/    OP_DUMP, 237,
/*2454*/    OP_COPY, 1, 2100, 3, /* _type */
/*2458*/    OP_DUMP, 238,
/*2460*/    OP_COPYVAR, 3, 2104, 20, /* cdpn_esa */
/*2464*/    OP_END_REC,
/*2465*/    OP_END_LEN,  /* _ie_len */
/*2466*/    OP_JUMP, 2666,
/*2468*/    OP_DUMP, 239,
/*2470*/    OP_COPY, 0, 2279, 1, /* _ext */
/*2474*/    OP_DUMP, 240,
/*2476*/    OP_COPY, 0, 2277, 2, /* _cs */
/*2480*/    OP_DUMP, 241,
/*2482*/    OP_COPY, 0, 2276, 1, /* _flag */
/*2486*/    OP_DUMP, 242,
/*2488*/    OP_COPY, 1, 2272, 3, /* _action_ind */
/*2492*/    OP_DUMP, 243,
/*2494*/    OP_BEGIN_LEN, 2, 2280, 16, /* _ie_len */
/*2498*/    OP_BEGIN_REC, RECOV_IND_IE, 128, 2527,
/*2502*/    OP_DUMP, 244,
/*2504*/    OP_COPY, 0, 2303, 1, /* _ext */
/*2508*/    OP_DUMP, 245,
/*2510*/    OP_COPY, 0, 2300, 3, /* cdps_type */
/*2514*/    OP_DUMP, 246,
/*2516*/    OP_COPY, 1, 2299, 1, /* _oddeven */
/*2520*/    OP_DUMP, 247,
/*2522*/    OP_COPYVAR, 4, 2304, 20, /* cdps */
/*2526*/    OP_END_REC,
/*2527*/    OP_END_LEN,  /* _ie_len */
/*2528*/    OP_JUMP, 2604,
/*2530*/    OP_DUMP, 248,
/*2532*/    OP_COPY, 0, 2479, 1, /* _ext */
/*2536*/    OP_DUMP, 249,
/*2538*/    OP_COPY, 0, 2477, 2, /* _cs */
/*2542*/    OP_DUMP, 250,
/*2544*/    OP_COPY, 0, 2476, 1, /* _flag */
/*2548*/    OP_DUMP, 251,
/*2550*/    OP_COPY, 1, 2472, 3, /* _action_ind */
/*2554*/    OP_DUMP, 252,
/*2556*/    OP_BEGIN_LEN, 2, 2480, 16, /* _ie_len */
/*2560*/    OP_BEGIN_REC, RECOV_IND_IE, 129, 2620,
/*2564*/    OP_DUMP, 253,
/*2566*/    OP_COPY, 0, 2496, 4, /* cgpn_plan */
/*2570*/    OP_DUMP, 254,
/*2572*/    OP_COPY, 0, 2500, 3, /* cgpn_type */
/*2576*/    OP_DUMP, 255,
/*2578*/    OP_COPY, 0, 7, 1, /* _ext */
/*2582*/    OP_CASE, 1, 7, 1, 2, /* _ext */
/*2587*/    0, 130, 3,
/*2590*/    -1, 131, 20,
/*2593*/    OP_DUMP, 256,
/*2595*/    OP_COPY, 0, 2511, 1, /* _ext */
/*2599*/    OP_DUMP, 257,
/*2601*/    OP_COPY, 0, 2509, 2, /* pres_ind */
/*2605*/    OP_DUMP, 258,
/*2607*/    OP_COPY, 1, 2504, 2, /* scr_ind */
/*2611*/    OP_JUMP, 0,
/*2613*/    OP_DUMP, 259,
/*2615*/    OP_COPYVAR, 5, 2520, 20, /* cgpn */
/*2619*/    OP_END_REC,
/*2620*/    OP_END_LEN,  /* _ie_len */
/*2621*/    OP_JUMP, 2511,
/*2623*/    OP_DUMP, 260,
/*2625*/    OP_COPY, 0, 2695, 1, /* _ext */
/*2629*/    OP_DUMP, 261,
/*2631*/    OP_COPY, 0, 2693, 2, /* _cs */
/*2635*/    OP_DUMP, 262,
/*2637*/    OP_COPY, 0, 2692, 1, /* _flag */
/*2641*/    OP_DUMP, 263,
/*2643*/    OP_COPY, 1, 2688, 3, /* _action_ind */
/*2647*/    OP_DUMP, 264,
/*2649*/    OP_BEGIN_LEN, 2, 2696, 16, /* _ie_len */
/*2653*/    OP_BEGIN_REC, RECOV_IND_IE, 132, 2682,
/*2657*/    OP_DUMP, 265,
/*2659*/    OP_COPY, 0, 2719, 1, /* _ext */
/*2663*/    OP_DUMP, 266,
/*2665*/    OP_COPY, 0, 2716, 3, /* cgps_type */
/*2669*/    OP_DUMP, 267,
/*2671*/    OP_COPY, 1, 2715, 1, /* _oddeven */
/*2675*/    OP_DUMP, 268,
/*2677*/    OP_COPYVAR, 6, 2720, 20, /* cgps */
/*2681*/    OP_END_REC,
/*2682*/    OP_END_LEN,  /* _ie_len */
/*2683*/    OP_JUMP, 2449,
/*2685*/    OP_DUMP, 269,
/*2687*/    OP_COPY, 0, 2895, 1, /* _ext */
/*2691*/    OP_DUMP, 270,
/*2693*/    OP_COPY, 0, 2893, 2, /* cause_cs */
/*2697*/    OP_DUMP, 271,
/*2699*/    OP_COPY, 0, 2892, 1, /* _flag */
/*2703*/    OP_DUMP, 272,
/*2705*/    OP_COPY, 1, 2888, 3, /* _action_ind */
/*2709*/    OP_DUMP, 273,
/*2711*/    OP_BEGIN_LEN, 2, 2896, 16, /* _ie_len */
/*2715*/    OP_BEGIN_REC, RECOV_IND_IE, 133, 2957,
/*2719*/    OP_DUMP, 274,
/*2721*/    OP_COPY, 0, 2919, 1, /* _ext */
/*2725*/    OP_DUMP, 275,
/*2727*/    OP_COPY, 1, 2912, 4, /* location */
/*2731*/    OP_DUMP, 276,
/*2733*/    OP_COPY, 0, 2927, 1, /* _ext */
/*2737*/    OP_DUMP, 277,
/*2739*/    OP_COPY, 0, 2920, 7, /* cause */
/*2743*/    OP_CASE, 1, 2920, 7, 18, /* cause */
/*2748*/    ATM_CV_UNALLOC, 134, 51,
/*2751*/    ATM_CV_NO_ROUTE_DEST, 134, 48,
/*2754*/    ATM_CV_QOS_UNAVAIL, 134, 45,
/*2757*/    ATM_CV_CALL_REJ, 135, 70,
/*2760*/    ATM_CV_NUM_CHANGED, 138, 117,
/*2763*/    ATM_CV_REJ_CLIR, 139, 124,
/*2766*/    ATM_CV_ACC_INF_DISC, 140, 131,
/*2769*/    ATM_CV_INCOMP_DEST, 140, 128,
/*2772*/    ATM_CV_MAND_IE_MISSING, 140, 125,
/*2775*/    ATM_CV_UNKNOWN_IE, 140, 122,
/*2778*/    ATM_CV_INVALID_IE, 140, 119,
/*2781*/    ATM_CV_UCR_UNAVAIL_OLD, 141, 126,
/*2784*/    ATM_CV_UCR_UNAVAIL_NEW, 141, 123,
/*2787*/    ATM_CV_NO_SUCH_CHAN, 142, 130,
/*2790*/    ATM_CV_UNKNOWN_MSG_TYPE, 143, 143,
/*2793*/    ATM_CV_INCOMP_MSG, 143, 140,
/*2796*/    ATM_CV_TIMER_EXP, 144, 147,
/*2799*/    -1, 145, 154,
/*2802*/    OP_IFEND, 153,
/*2804*/    OP_DUMP, 278,
/*2806*/    OP_COPY, 0, 2935, 1, /* _ext */
/*2810*/    OP_DUMP, 279,
/*2812*/    OP_COPY, 0, 2931, 1, /* pu */
/*2816*/    OP_DUMP, 280,
/*2818*/    OP_COPY, 0, 2930, 1, /* na */
/*2822*/    OP_DUMP, 281,
/*2824*/    OP_COPY, 1, 2928, 2, /* cond2 */
/*2828*/    OP_JUMP, 126,
/*2830*/    OP_IFEND, 125,
/*2832*/    OP_DUMP, 282,
/*2834*/    OP_COPY, 0, 2943, 1, /* _ext */
/*2838*/    OP_DUMP, 283,
/*2840*/    OP_COPY, 0, 2936, 2, /* cond3 */
/*2844*/    OP_DUMP, 284,
/*2846*/    OP_COPY, 0, 2938, 5, /* reason */
/*2850*/    OP_CASE, 1, 2938, 5, 3, /* reason */
/*2855*/    ATM_RSN_USER, 136, 6,
/*2858*/    ATM_RSN_IE_MISS, 137, 11,
/*2861*/    ATM_RSN_IE_INSUFF, 137, 8,
/*2864*/    OP_DUMP, 285,
/*2866*/    OP_COPYVAR, 7, 2944, 27, /* user_diag */
/*2870*/    OP_JUMP, 6,
/*2872*/    OP_DUMP, 286,
/*2874*/    OP_COPY, 1, 3160, 8, /* ie_id3 */
/*2878*/    OP_JUMP, 76,
/*2880*/    OP_IFEND, 75,
/*2882*/    OP_DUMP, 287,
/*2884*/    OP_COPYVAR, 8, 3168, 28, /* new_dest */
/*2888*/    OP_JUMP, 66,
/*2890*/    OP_IFEND, 65,
/*2892*/    OP_DUMP, 288,
/*2894*/    OP_COPY, 1, 3392, 8, /* invalid */
/*2898*/    OP_JUMP, 56,
/*2900*/    OP_IFEND, 55,
/*2902*/    OP_DUMP, 289,
/*2904*/    OP_COPYVAR, 9, 3400, 28, /* ie_id6 */
/*2908*/    OP_JUMP, 46,
/*2910*/    OP_IFEND, 45,
/*2912*/    OP_DUMP, 290,
/*2914*/    OP_COPYVAR, 10, 3624, 28, /* ucr_id */
/*2918*/    OP_JUMP, 36,
/*2920*/    OP_IFEND, 35,
/*2922*/    OP_DUMP, 291,
/*2924*/    OP_COPY, 2, 3848, 16, /* unav_vpci */
/*2928*/    OP_DUMP, 292,
/*2930*/    OP_COPY, 2, 3864, 16, /* unav_vci */
/*2934*/    OP_JUMP, 20,
/*2936*/    OP_IFEND, 19,
/*2938*/    OP_DUMP, 293,
/*2940*/    OP_COPY, 1, 3880, 8, /* bad_msg_type */
/*2944*/    OP_JUMP, 10,
/*2946*/    OP_IFEND, 9,
/*2948*/    OP_DUMP, 294,
/*2950*/    OP_COPY, 3, 3888, 24, /* timer */
/*2954*/    OP_JUMP, 0,
/*2956*/    OP_END_REC,
/*2957*/    OP_END_LEN,  /* _ie_len */
/*2958*/    OP_JUMP, 2174,
/*2960*/    OP_DUMP, 295,
/*2962*/    OP_COPY, 0, 3927, 1, /* _ext */
/*2966*/    OP_DUMP, 296,
/*2968*/    OP_COPY, 0, 3925, 2, /* cause_cs */
/*2972*/    OP_DUMP, 297,
/*2974*/    OP_COPY, 0, 3924, 1, /* _flag */
/*2978*/    OP_DUMP, 298,
/*2980*/    OP_COPY, 1, 3920, 3, /* _action_ind */
/*2984*/    OP_DUMP, 299,
/*2986*/    OP_BEGIN_LEN, 2, 3928, 16, /* _ie_len */
/*2990*/    OP_BEGIN_REC, RECOV_IND_IE, 146, 3232,
/*2994*/    OP_DUMP, 300,
/*2996*/    OP_COPY, 0, 3951, 1, /* _ext */
/*3000*/    OP_DUMP, 301,
/*3002*/    OP_COPY, 1, 3944, 4, /* location */
/*3006*/    OP_DUMP, 302,
/*3008*/    OP_COPY, 0, 3959, 1, /* _ext */
/*3012*/    OP_DUMP, 303,
/*3014*/    OP_COPY, 0, 3952, 7, /* cause */
/*3018*/    OP_CASE, 1, 3952, 7, 18, /* cause */
/*3023*/    ATM_CV_UNALLOC, 147, 51,
/*3026*/    ATM_CV_NO_ROUTE_DEST, 147, 48,
/*3029*/    ATM_CV_QOS_UNAVAIL, 147, 45,
/*3032*/    ATM_CV_CALL_REJ, 148, 70,
/*3035*/    ATM_CV_NUM_CHANGED, 151, 117,
/*3038*/    ATM_CV_REJ_CLIR, 152, 124,
/*3041*/    ATM_CV_ACC_INF_DISC, 153, 131,
/*3044*/    ATM_CV_INCOMP_DEST, 153, 128,
/*3047*/    ATM_CV_MAND_IE_MISSING, 153, 125,
/*3050*/    ATM_CV_UNKNOWN_IE, 153, 122,
/*3053*/    ATM_CV_INVALID_IE, 153, 119,
/*3056*/    ATM_CV_UCR_UNAVAIL_OLD, 154, 126,
/*3059*/    ATM_CV_UCR_UNAVAIL_NEW, 154, 123,
/*3062*/    ATM_CV_NO_SUCH_CHAN, 155, 130,
/*3065*/    ATM_CV_UNKNOWN_MSG_TYPE, 156, 143,
/*3068*/    ATM_CV_INCOMP_MSG, 156, 140,
/*3071*/    ATM_CV_TIMER_EXP, 157, 147,
/*3074*/    -1, 158, 154,
/*3077*/    OP_IFEND, 153,
/*3079*/    OP_DUMP, 304,
/*3081*/    OP_COPY, 0, 3967, 1, /* _ext */
/*3085*/    OP_DUMP, 305,
/*3087*/    OP_COPY, 0, 3963, 1, /* pu */
/*3091*/    OP_DUMP, 306,
/*3093*/    OP_COPY, 0, 3962, 1, /* na */
/*3097*/    OP_DUMP, 307,
/*3099*/    OP_COPY, 1, 3960, 2, /* cond2 */
/*3103*/    OP_JUMP, 126,
/*3105*/    OP_IFEND, 125,
/*3107*/    OP_DUMP, 308,
/*3109*/    OP_COPY, 0, 3975, 1, /* _ext */
/*3113*/    OP_DUMP, 309,
/*3115*/    OP_COPY, 0, 3968, 2, /* cond3 */
/*3119*/    OP_DUMP, 310,
/*3121*/    OP_COPY, 0, 3970, 5, /* reason */
/*3125*/    OP_CASE, 1, 3970, 5, 3, /* reason */
/*3130*/    ATM_RSN_USER, 149, 6,
/*3133*/    ATM_RSN_IE_MISS, 150, 11,
/*3136*/    ATM_RSN_IE_INSUFF, 150, 8,
/*3139*/    OP_DUMP, 311,
/*3141*/    OP_COPYVAR, 11, 3976, 27, /* user_diag */
/*3145*/    OP_JUMP, 6,
/*3147*/    OP_DUMP, 312,
/*3149*/    OP_COPY, 1, 4192, 8, /* ie_id3 */
/*3153*/    OP_JUMP, 76,
/*3155*/    OP_IFEND, 75,
/*3157*/    OP_DUMP, 313,
/*3159*/    OP_COPYVAR, 12, 4200, 28, /* new_dest */
/*3163*/    OP_JUMP, 66,
/*3165*/    OP_IFEND, 65,
/*3167*/    OP_DUMP, 314,
/*3169*/    OP_COPY, 1, 4424, 8, /* invalid */
/*3173*/    OP_JUMP, 56,
/*3175*/    OP_IFEND, 55,
/*3177*/    OP_DUMP, 315,
/*3179*/    OP_COPYVAR, 13, 4432, 28, /* ie_id6 */
/*3183*/    OP_JUMP, 46,
/*3185*/    OP_IFEND, 45,
/*3187*/    OP_DUMP, 316,
/*3189*/    OP_COPYVAR, 14, 4656, 28, /* ucr_id */
/*3193*/    OP_JUMP, 36,
/*3195*/    OP_IFEND, 35,
/*3197*/    OP_DUMP, 317,
/*3199*/    OP_COPY, 2, 4880, 16, /* unav_vpci */
/*3203*/    OP_DUMP, 318,
/*3205*/    OP_COPY, 2, 4896, 16, /* unav_vci */
/*3209*/    OP_JUMP, 20,
/*3211*/    OP_IFEND, 19,
/*3213*/    OP_DUMP, 319,
/*3215*/    OP_COPY, 1, 4912, 8, /* bad_msg_type */
/*3219*/    OP_JUMP, 10,
/*3221*/    OP_IFEND, 9,
/*3223*/    OP_DUMP, 320,
/*3225*/    OP_COPY, 3, 4920, 24, /* timer */
/*3229*/    OP_JUMP, 0,
/*3231*/    OP_END_REC,
/*3232*/    OP_END_LEN,  /* _ie_len */
/*3233*/    OP_JUMP, 1899,
/*3235*/    OP_DUMP, 321,
/*3237*/    OP_COPY, 0, 4959, 1, /* _ext */
/*3241*/    OP_DUMP, 322,
/*3243*/    OP_COPY, 0, 4957, 2, /* _cs */
/*3247*/    OP_DUMP, 323,
/*3249*/    OP_COPY, 0, 4956, 1, /* _flag */
/*3253*/    OP_DUMP, 324,
/*3255*/    OP_COPY, 1, 4952, 3, /* _action_ind */
/*3259*/    OP_DUMP, 325,
/*3261*/    OP_BEGIN_LEN, 2, 4960, 16, /* _ie_len */
/*3265*/    OP_BEGIN_REC, RECOV_IND_IE, 159, 3300,
/*3269*/    OP_DUMP, 326,
/*3271*/    OP_COPY, 0, 4983, 1, /* _ext */
/*3275*/    OP_DUMP, 327,
/*3277*/    OP_COPY, 0, 4979, 2, /* _vp_ass */
/*3281*/    OP_DUMP, 328,
/*3283*/    OP_COPY, 1, 4976, 3, /* _pref_exc */
/*3287*/    OP_DUMP, 329,
/*3289*/    OP_COPY, 2, 4984, 16, /* vpi */
/*3293*/    OP_DUMP, 330,
/*3295*/    OP_COPY, 2, 5000, 16, /* vci */
/*3299*/    OP_END_REC,
/*3300*/    OP_END_LEN,  /* _ie_len */
/*3301*/    OP_JUMP, 1831,
/*3303*/    OP_DUMP, 331,
/*3305*/    OP_COPY, 0, 5031, 1, /* _ext */
/*3309*/    OP_DUMP, 332,
/*3311*/    OP_COPY, 0, 5029, 2, /* _cs */
/*3315*/    OP_DUMP, 333,
/*3317*/    OP_COPY, 0, 5028, 1, /* _flag */
/*3321*/    OP_DUMP, 334,
/*3323*/    OP_COPY, 1, 5024, 3, /* _action_ind */
/*3327*/    OP_DUMP, 335,
/*3329*/    OP_BEGIN_LEN, 2, 5032, 16, /* _ie_len */
/*3333*/    OP_BEGIN_REC, RECOV_IND_IE, 160, 3378,
/*3337*/    OP_IFEND, 38, /* _id */
/*3339*/    OP_DUMP, 336,
/*3341*/    OP_COPY, 0, 0, 8, /* _id */
/*3345*/    OP_CASE, 1, 0, 8, 3, /* _id */
/*3350*/    ATM_TDL_CUM, 161, 6,
/*3353*/    ATM_TDL_E2EMAX, 162, 11,
/*3356*/    ATM_TDL_NGI, 163, 16,
/*3359*/    OP_DUMP, 337,
/*3361*/    OP_COPY, 2, 5056, 16, /* cum_delay */
/*3365*/    OP_JUMP, 8,
/*3367*/    OP_DUMP, 338,
/*3369*/    OP_COPY, 2, 5080, 16, /* max_delay */
/*3373*/    OP_JUMP, 0,
/*3375*/    OP_JUMP, -40,
/*3377*/    OP_END_REC,
/*3378*/    OP_END_LEN,  /* _ie_len */
/*3379*/    OP_JUMP, 1753,
/*3381*/    OP_DUMP, 339,
/*3383*/    OP_COPY, 0, 5119, 1, /* _ext */
/*3387*/    OP_DUMP, 340,
/*3389*/    OP_COPY, 0, 5117, 2, /* qos_cs */
/*3393*/    OP_DUMP, 341,
/*3395*/    OP_COPY, 0, 5116, 1, /* _flag */
/*3399*/    OP_DUMP, 342,
/*3401*/    OP_COPY, 1, 5112, 3, /* _action_ind */
/*3405*/    OP_DUMP, 343,
/*3407*/    OP_BEGIN_LEN, 2, 5120, 16, /* _ie_len */
/*3411*/    OP_BEGIN_REC, RECOV_IND_IE, 164, 3428,
/*3415*/    OP_DUMP, 344,
/*3417*/    OP_COPY, 1, 5136, 8, /* qos_fw */
/*3421*/    OP_DUMP, 345,
/*3423*/    OP_COPY, 1, 5144, 8, /* qos_bw */
/*3427*/    OP_END_REC,
/*3428*/    OP_END_LEN,  /* _ie_len */
/*3429*/    OP_JUMP, 1703,
/*3431*/    OP_DUMP, 346,
/*3433*/    OP_COPY, 0, 5167, 1, /* _ext */
/*3437*/    OP_DUMP, 347,
/*3439*/    OP_COPY, 0, 5165, 2, /* _cs */
/*3443*/    OP_DUMP, 348,
/*3445*/    OP_COPY, 0, 5164, 1, /* _flag */
/*3449*/    OP_DUMP, 349,
/*3451*/    OP_COPY, 1, 5160, 3, /* _action_ind */
/*3455*/    OP_DUMP, 350,
/*3457*/    OP_BEGIN_LEN, 2, 5168, 16, /* _ie_len */
/*3461*/    OP_BEGIN_REC, RECOV_IND_IE, 165, 3478,
/*3465*/    OP_DUMP, 351,
/*3467*/    OP_COPY, 0, 5191, 1, /* _ext */
/*3471*/    OP_DUMP, 352,
/*3473*/    OP_COPY, 1, 5184, 4, /* rep_ind */
/*3477*/    OP_END_REC,
/*3478*/    OP_END_LEN,  /* _ie_len */
/*3479*/    OP_JUMP, 1653,
/*3481*/    OP_DUMP, 353,
/*3483*/    OP_COPY, 0, 5207, 1, /* _ext */
/*3487*/    OP_DUMP, 354,
/*3489*/    OP_COPY, 0, 5205, 2, /* _cs */
/*3493*/    OP_DUMP, 355,
/*3495*/    OP_COPY, 0, 5204, 1, /* _flag */
/*3499*/    OP_DUMP, 356,
/*3501*/    OP_COPY, 1, 5200, 3, /* _action_ind */
/*3505*/    OP_DUMP, 357,
/*3507*/    OP_BEGIN_LEN, 2, 5208, 16, /* _ie_len */
/*3511*/    OP_BEGIN_REC, RECOV_IND_IE, 166, 3528,
/*3515*/    OP_DUMP, 358,
/*3517*/    OP_COPY, 0, 5231, 1, /* _ext */
/*3521*/    OP_DUMP, 359,
/*3523*/    OP_COPY, 1, 5224, 3, /* rst_class */
/*3527*/    OP_END_REC,
/*3528*/    OP_END_LEN,  /* _ie_len */
/*3529*/    OP_JUMP, 1603,
/*3531*/    OP_DUMP, 360,
/*3533*/    OP_COPY, 0, 5247, 1, /* _ext */
/*3537*/    OP_DUMP, 361,
/*3539*/    OP_COPY, 0, 5245, 2, /* _cs */
/*3543*/    OP_DUMP, 362,
/*3545*/    OP_COPY, 0, 5244, 1, /* _flag */
/*3549*/    OP_DUMP, 363,
/*3551*/    OP_COPY, 1, 5240, 3, /* _action_ind */
/*3555*/    OP_DUMP, 364,
/*3557*/    OP_BEGIN_LEN, 2, 5248, 16, /* _ie_len */
/*3561*/    OP_BEGIN_REC, RECOV_IND_IE, 167, 3578,
/*3565*/    OP_DUMP, 365,
/*3567*/    OP_COPY, 0, 5271, 1, /* _ext */
/*3571*/    OP_DUMP, 366,
/*3573*/    OP_COPY, 1, 5264, 7, /* bbsc_ind */
/*3577*/    OP_END_REC,
/*3578*/    OP_END_LEN,  /* _ie_len */
/*3579*/    OP_JUMP, 1553,
/*3581*/    OP_DUMP, 367,
/*3583*/    OP_COPY, 0, 5287, 1, /* _ext */
/*3587*/    OP_DUMP, 368,
/*3589*/    OP_COPY, 0, 5285, 2, /* _cs */
/*3593*/    OP_DUMP, 369,
/*3595*/    OP_COPY, 0, 5284, 1, /* _flag */
/*3599*/    OP_DUMP, 370,
/*3601*/    OP_COPY, 1, 5280, 3, /* _action_ind */
/*3605*/    OP_DUMP, 371,
/*3607*/    OP_BEGIN_LEN, 2, 5288, 16, /* _ie_len */
/*3611*/    OP_BEGIN_REC, RECOV_IND_IE, 168, 3640,
/*3615*/    OP_DUMP, 372,
/*3617*/    OP_COPY, 0, 5311, 1, /* _ext */
/*3621*/    OP_DUMP, 373,
/*3623*/    OP_COPY, 0, 5308, 3, /* _net_type */
/*3627*/    OP_DUMP, 374,
/*3629*/    OP_COPY, 1, 5304, 4, /* _carrier_id */
/*3633*/    OP_DUMP, 375,
/*3635*/    OP_COPYVAR, 15, 5312, 4, /* net_id */
/*3639*/    OP_END_REC,
/*3640*/    OP_END_LEN,  /* _ie_len */
/*3641*/    OP_JUMP, 1491,
/*3643*/    OP_DUMP, 376,
/*3645*/    OP_COPY, 0, 5359, 1, /* _ext */
/*3649*/    OP_DUMP, 377,
/*3651*/    OP_COPY, 0, 5357, 2, /* _cs */
/*3655*/    OP_DUMP, 378,
/*3657*/    OP_COPY, 0, 5356, 1, /* _flag */
/*3661*/    OP_DUMP, 379,
/*3663*/    OP_COPY, 1, 5352, 3, /* _action_ind */
/*3667*/    OP_DUMP, 380,
/*3669*/    OP_BEGIN_LEN, 2, 5360, 16, /* _ie_len */
/*3673*/    OP_BEGIN_REC, RECOV_IND_IE, 169, 3684,
/*3677*/    OP_DUMP, 381,
/*3679*/    OP_COPYVAR, 16, 5376, 4, /* notification */
/*3683*/    OP_END_REC,
/*3684*/    OP_END_LEN,  /* _ie_len */
/*3685*/    OP_JUMP, 1447,
/*3687*/    OP_DUMP, 382,
/*3689*/    OP_COPY, 0, 5423, 1, /* _ext */
/*3693*/    OP_DUMP, 383,
/*3695*/    OP_COPY, 0, 5421, 2, /* _cs */
/*3699*/    OP_DUMP, 384,
/*3701*/    OP_COPY, 0, 5420, 1, /* _flag */
/*3705*/    OP_DUMP, 385,
/*3707*/    OP_COPY, 1, 5416, 3, /* _action_ind */
/*3711*/    OP_DUMP, 386,
/*3713*/    OP_BEGIN_LEN, 2, 5424, 16, /* _ie_len */
/*3717*/    OP_BEGIN_REC, RECOV_IND_IE, 170, 3764,
/*3721*/    OP_DUMP, 387,
/*3723*/    OP_COPY, 0, 5447, 1, /* _ext */
/*3727*/    OP_DUMP, 388,
/*3729*/    OP_COPY, 0, 5445, 2, /* shaping */
/*3733*/    OP_DUMP, 389,
/*3735*/    OP_COPY, 0, 5444, 1, /* compliance */
/*3739*/    OP_DUMP, 390,
/*3741*/    OP_COPY, 1, 5440, 3, /* fault */
/*3745*/    OP_DUMP, 391,
/*3747*/    OP_COPY, 0, 5455, 1, /* _ext */
/*3751*/    OP_DUMP, 392,
/*3753*/    OP_COPY, 0, 5452, 3, /* fwd_ofi */
/*3757*/    OP_DUMP, 393,
/*3759*/    OP_COPY, 1, 5448, 3, /* bwd_ofi */
/*3763*/    OP_END_REC,
/*3764*/    OP_END_LEN,  /* _ie_len */
/*3765*/    OP_JUMP, 1367,
/*3767*/    OP_DUMP, 394,
/*3769*/    OP_COPY, 0, 5471, 1, /* _ext */
/*3773*/    OP_DUMP, 395,
/*3775*/    OP_COPY, 0, 5469, 2, /* _cs */
/*3779*/    OP_DUMP, 396,
/*3781*/    OP_COPY, 0, 5468, 1, /* _flag */
/*3785*/    OP_DUMP, 397,
/*3787*/    OP_COPY, 1, 5464, 3, /* _action_ind */
/*3791*/    OP_DUMP, 398,
/*3793*/    OP_BEGIN_LEN, 2, 5472, 16, /* _ie_len */
/*3797*/    OP_BEGIN_REC, RECOV_IND_IE, 171, 3874,
/*3801*/    OP_DUMP, 399,
/*3803*/    OP_COPY, 1, 5495, 1, /* _dummy */
/*3807*/    OP_DUMP, 400,
/*3809*/    OP_COPY, 0, 5496, 7, /* id_std_app */
/*3813*/    OP_CASE, 1, 5496, 7, 3, /* id_std_app */
/*3818*/    ATM_IRS_DSMCC, 172, 6,
/*3821*/    ATM_IRS_H245, 172, 3,
/*3824*/    -1, 173, 40,
/*3827*/    OP_DUMP, 401,
/*3829*/    OP_COPY, 1, 5504, 8, /* _type */
/*3833*/    OP_DUMP, 402,
/*3835*/    OP_BEGIN_LEN, 1, 5512, 8, /* _length */
/*3839*/    OP_DUMP, 403,
/*3841*/    OP_COPYVAR, 17, 5520, 20, /* session_id */
/*3845*/    OP_END_LEN,  /* _length */
/*3846*/    OP_DUMP, 404,
/*3848*/    OP_COPY, 1, 5680, 8, /* _type */
/*3852*/    OP_DUMP, 405,
/*3854*/    OP_BEGIN_LEN, 1, 5688, 8, /* _length */
/*3858*/    OP_DUMP, 406,
/*3860*/    OP_COPYVAR, 18, 5696, 4, /* resource_id */
/*3864*/    OP_END_LEN,  /* _length */
/*3865*/    OP_JUMP, 6,
/*3867*/    OP_DUMP, 407,
/*3869*/    OP_COPYVAR, 19, 5728, 28, /* unrecognized_git_identifiers */
/*3873*/    OP_END_REC,
/*3874*/    OP_END_LEN,  /* _ie_len */
/*3875*/    OP_JUMP, 1257,
/*3877*/    OP_DUMP, 408,
/*3879*/    OP_COPY, 0, 5967, 1, /* _ext */
/*3883*/    OP_DUMP, 409,
/*3885*/    OP_COPY, 0, 5965, 2, /* _cs */
/*3889*/    OP_DUMP, 410,
/*3891*/    OP_COPY, 0, 5964, 1, /* _flag */
/*3895*/    OP_DUMP, 411,
/*3897*/    OP_COPY, 1, 5960, 3, /* _action_ind */
/*3901*/    OP_DUMP, 412,
/*3903*/    OP_BEGIN_LEN, 2, 5968, 16, /* _ie_len */
/*3907*/    OP_BEGIN_REC, RECOV_IND_IE, 174, 3984,
/*3911*/    OP_DUMP, 413,
/*3913*/    OP_COPY, 1, 5991, 1, /* _dummy */
/*3917*/    OP_DUMP, 414,
/*3919*/    OP_COPY, 0, 5992, 7, /* id_std_app */
/*3923*/    OP_CASE, 1, 5992, 7, 3, /* id_std_app */
/*3928*/    ATM_IRS_DSMCC, 175, 6,
/*3931*/    ATM_IRS_H245, 175, 3,
/*3934*/    -1, 176, 40,
/*3937*/    OP_DUMP, 415,
/*3939*/    OP_COPY, 1, 6000, 8, /* _type */
/*3943*/    OP_DUMP, 416,
/*3945*/    OP_BEGIN_LEN, 1, 6008, 8, /* _length */
/*3949*/    OP_DUMP, 417,
/*3951*/    OP_COPYVAR, 20, 6016, 20, /* session_id */
/*3955*/    OP_END_LEN,  /* _length */
/*3956*/    OP_DUMP, 418,
/*3958*/    OP_COPY, 1, 6176, 8, /* _type */
/*3962*/    OP_DUMP, 419,
/*3964*/    OP_BEGIN_LEN, 1, 6184, 8, /* _length */
/*3968*/    OP_DUMP, 420,
/*3970*/    OP_COPYVAR, 21, 6192, 4, /* resource_id */
/*3974*/    OP_END_LEN,  /* _length */
/*3975*/    OP_JUMP, 6,
/*3977*/    OP_DUMP, 421,
/*3979*/    OP_COPYVAR, 22, 6224, 28, /* unrecognized_git_identifiers */
/*3983*/    OP_END_REC,
/*3984*/    OP_END_LEN,  /* _ie_len */
/*3985*/    OP_JUMP, 1147,
/*3987*/    OP_DUMP, 422,
/*3989*/    OP_COPY, 0, 6463, 1, /* _ext */
/*3993*/    OP_DUMP, 423,
/*3995*/    OP_COPY, 0, 6461, 2, /* _cs */
/*3999*/    OP_DUMP, 424,
/*4001*/    OP_COPY, 0, 6460, 1, /* _flag */
/*4005*/    OP_DUMP, 425,
/*4007*/    OP_COPY, 1, 6456, 3, /* _action_ind */
/*4011*/    OP_DUMP, 426,
/*4013*/    OP_BEGIN_LEN, 2, 6464, 16, /* _ie_len */
/*4017*/    OP_BEGIN_REC, RECOV_IND_IE, 177, 4094,
/*4021*/    OP_DUMP, 427,
/*4023*/    OP_COPY, 1, 6487, 1, /* _dummy */
/*4027*/    OP_DUMP, 428,
/*4029*/    OP_COPY, 0, 6488, 7, /* id_std_app */
/*4033*/    OP_CASE, 1, 6488, 7, 3, /* id_std_app */
/*4038*/    ATM_IRS_DSMCC, 178, 6,
/*4041*/    ATM_IRS_H245, 178, 3,
/*4044*/    -1, 179, 40,
/*4047*/    OP_DUMP, 429,
/*4049*/    OP_COPY, 1, 6496, 8, /* _type */
/*4053*/    OP_DUMP, 430,
/*4055*/    OP_BEGIN_LEN, 1, 6504, 8, /* _length */
/*4059*/    OP_DUMP, 431,
/*4061*/    OP_COPYVAR, 23, 6512, 20, /* session_id */
/*4065*/    OP_END_LEN,  /* _length */
/*4066*/    OP_DUMP, 432,
/*4068*/    OP_COPY, 1, 6672, 8, /* _type */
/*4072*/    OP_DUMP, 433,
/*4074*/    OP_BEGIN_LEN, 1, 6680, 8, /* _length */
/*4078*/    OP_DUMP, 434,
/*4080*/    OP_COPYVAR, 24, 6688, 4, /* resource_id */
/*4084*/    OP_END_LEN,  /* _length */
/*4085*/    OP_JUMP, 6,
/*4087*/    OP_DUMP, 435,
/*4089*/    OP_COPYVAR, 25, 6720, 28, /* unrecognized_git_identifiers */
/*4093*/    OP_END_REC,
/*4094*/    OP_END_LEN,  /* _ie_len */
/*4095*/    OP_JUMP, 1037,
/*4097*/    OP_DUMP, 436,
/*4099*/    OP_COPY, 0, 6959, 1, /* _ext */
/*4103*/    OP_DUMP, 437,
/*4105*/    OP_COPY, 0, 6957, 2, /* _cs */
/*4109*/    OP_DUMP, 438,
/*4111*/    OP_COPY, 0, 6956, 1, /* _flag */
/*4115*/    OP_DUMP, 439,
/*4117*/    OP_COPY, 1, 6952, 3, /* _action_ind */
/*4121*/    OP_DUMP, 440,
/*4123*/    OP_BEGIN_LEN, 2, 6960, 16, /* _ie_len */
/*4127*/    OP_BEGIN_REC, RECOV_IND_IE, 180, 4150,
/*4131*/    OP_DUMP, 441,
/*4133*/    OP_COPY, 0, 6983, 1, /* _ext */
/*4137*/    OP_DUMP, 442,
/*4139*/    OP_COPY, 1, 6976, 7, /* lij_id_type */
/*4143*/    OP_DUMP, 443,
/*4145*/    OP_COPY, 4, 6984, 32, /* lij_id */
/*4149*/    OP_END_REC,
/*4150*/    OP_END_LEN,  /* _ie_len */
/*4151*/    OP_JUMP, 981,
/*4153*/    OP_DUMP, 444,
/*4155*/    OP_COPY, 0, 7031, 1, /* _ext */
/*4159*/    OP_DUMP, 445,
/*4161*/    OP_COPY, 0, 7029, 2, /* _cs */
/*4165*/    OP_DUMP, 446,
/*4167*/    OP_COPY, 0, 7028, 1, /* _flag */
/*4171*/    OP_DUMP, 447,
/*4173*/    OP_COPY, 1, 7024, 3, /* _action_ind */
/*4177*/    OP_DUMP, 448,
/*4179*/    OP_BEGIN_LEN, 2, 7032, 16, /* _ie_len */
/*4183*/    OP_BEGIN_REC, RECOV_IND_IE, 181, 4200,
/*4187*/    OP_DUMP, 449,
/*4189*/    OP_COPY, 0, 7055, 1, /* _ext */
/*4193*/    OP_DUMP, 450,
/*4195*/    OP_COPY, 1, 7048, 2, /* lij_scr_ind */
/*4199*/    OP_END_REC,
/*4200*/    OP_END_LEN,  /* _ie_len */
/*4201*/    OP_JUMP, 931,
/*4203*/    OP_DUMP, 451,
/*4205*/    OP_COPY, 0, 7071, 1, /* _ext */
/*4209*/    OP_DUMP, 452,
/*4211*/    OP_COPY, 0, 7069, 2, /* _cs */
/*4215*/    OP_DUMP, 453,
/*4217*/    OP_COPY, 0, 7068, 1, /* _flag */
/*4221*/    OP_DUMP, 454,
/*4223*/    OP_COPY, 1, 7064, 3, /* _action_ind */
/*4227*/    OP_DUMP, 455,
/*4229*/    OP_BEGIN_LEN, 2, 7072, 16, /* _ie_len */
/*4233*/    OP_BEGIN_REC, RECOV_IND_IE, 182, 4244,
/*4237*/    OP_DUMP, 456,
/*4239*/    OP_COPY, 4, 7088, 32, /* leaf_sn */
/*4243*/    OP_END_REC,
/*4244*/    OP_END_LEN,  /* _ie_len */
/*4245*/    OP_JUMP, 887,
/*4247*/    OP_DUMP, 457,
/*4249*/    OP_COPY, 0, 7135, 1, /* _ext */
/*4253*/    OP_DUMP, 458,
/*4255*/    OP_COPY, 0, 7133, 2, /* _cs */
/*4259*/    OP_DUMP, 459,
/*4261*/    OP_COPY, 0, 7132, 1, /* _flag */
/*4265*/    OP_DUMP, 460,
/*4267*/    OP_COPY, 1, 7128, 3, /* _action_ind */
/*4271*/    OP_DUMP, 461,
/*4273*/    OP_BEGIN_LEN, 2, 7136, 16, /* _ie_len */
/*4277*/    OP_BEGIN_REC, RECOV_IND_IE, 183, 4300,
/*4281*/    OP_DUMP, 462,
/*4283*/    OP_COPY, 0, 7159, 1, /* _ext */
/*4287*/    OP_DUMP, 463,
/*4289*/    OP_COPY, 1, 7152, 4, /* scope_type */
/*4293*/    OP_DUMP, 464,
/*4295*/    OP_COPY, 1, 7160, 8, /* scope_sel */
/*4299*/    OP_END_REC,
/*4300*/    OP_END_LEN,  /* _ie_len */
/*4301*/    OP_JUMP, 831,
/*4303*/    OP_DUMP, 465,
/*4305*/    OP_COPY, 0, 7183, 1, /* _ext */
/*4309*/    OP_DUMP, 466,
/*4311*/    OP_COPY, 0, 7181, 2, /* _cs */
/*4315*/    OP_DUMP, 467,
/*4317*/    OP_COPY, 0, 7180, 1, /* _flag */
/*4321*/    OP_DUMP, 468,
/*4323*/    OP_COPY, 1, 7176, 3, /* _action_ind */
/*4327*/    OP_DUMP, 469,
/*4329*/    OP_BEGIN_LEN, 2, 7184, 16, /* _ie_len */
/*4333*/    OP_BEGIN_REC, RECOV_IND_IE, 184, 4492,
/*4337*/    OP_IFEND, 152, /* _id */
/*4339*/    OP_DUMP, 470,
/*4341*/    OP_COPY, 0, 0, 8, /* _id */
/*4345*/    OP_CASE, 1, 0, 8, 13, /* _id */
/*4350*/    ATM_TD_FW_PCR_0, 185, 36,
/*4353*/    ATM_TD_BW_PCR_0, 186, 41,
/*4356*/    ATM_TD_FW_PCR_01, 187, 46,
/*4359*/    ATM_TD_BW_PCR_01, 188, 51,
/*4362*/    ATM_TD_FW_SCR_0, 189, 56,
/*4365*/    ATM_TD_BW_SCR_0, 190, 61,
/*4368*/    ATM_TD_FW_SCR_01, 191, 66,
/*4371*/    ATM_TD_BW_SCR_01, 192, 71,
/*4374*/    ATM_TD_FW_MBS_0, 193, 76,
/*4377*/    ATM_TD_BW_MBS_0, 194, 81,
/*4380*/    ATM_TD_FW_MBS_01, 195, 86,
/*4383*/    ATM_TD_BW_MBS_01, 196, 91,
/*4386*/    ATM_TD_BEST_EFFORT, 197, 96,
/*4389*/    OP_DUMP, 471,
/*4391*/    OP_COPY, 3, 7208, 24, /* altfw_pcr_0 */
/*4395*/    OP_JUMP, 92,
/*4397*/    OP_DUMP, 472,
/*4399*/    OP_COPY, 3, 7240, 24, /* altbw_pcr_0 */
/*4403*/    OP_JUMP, 84,
/*4405*/    OP_DUMP, 473,
/*4407*/    OP_COPY, 3, 7272, 24, /* altfw_pcr_01 */
/*4411*/    OP_JUMP, 76,
/*4413*/    OP_DUMP, 474,
/*4415*/    OP_COPY, 3, 7304, 24, /* altbw_pcr_01 */
/*4419*/    OP_JUMP, 68,
/*4421*/    OP_DUMP, 475,
/*4423*/    OP_COPY, 3, 7336, 24, /* altfw_scr_0 */
/*4427*/    OP_JUMP, 60,
/*4429*/    OP_DUMP, 476,
/*4431*/    OP_COPY, 3, 7368, 24, /* altbw_scr_0 */
/*4435*/    OP_JUMP, 52,
/*4437*/    OP_DUMP, 477,
/*4439*/    OP_COPY, 3, 7400, 24, /* altfw_scr_01 */
/*4443*/    OP_JUMP, 44,
/*4445*/    OP_DUMP, 478,
/*4447*/    OP_COPY, 3, 7432, 24, /* altbw_scr_01 */
/*4451*/    OP_JUMP, 36,
/*4453*/    OP_DUMP, 479,
/*4455*/    OP_COPY, 3, 7464, 24, /* altfw_mbs_0 */
/*4459*/    OP_JUMP, 28,
/*4461*/    OP_DUMP, 480,
/*4463*/    OP_COPY, 3, 7496, 24, /* altbw_mbs_0 */
/*4467*/    OP_JUMP, 20,
/*4469*/    OP_DUMP, 481,
/*4471*/    OP_COPY, 3, 7528, 24, /* altfw_mbs_01 */
/*4475*/    OP_JUMP, 12,
/*4477*/    OP_DUMP, 482,
/*4479*/    OP_COPY, 3, 7560, 24, /* altbw_mbs_01 */
/*4483*/    OP_JUMP, 4,
/*4485*/    OP_COPY, 0, 7592, 0, /* altbest_effort */
/*4489*/    OP_JUMP, -154,
/*4491*/    OP_END_REC,
/*4492*/    OP_END_LEN,  /* _ie_len */
/*4493*/    OP_JUMP, 639,
/*4495*/    OP_DUMP, 484,
/*4497*/    OP_COPY, 0, 7607, 1, /* _ext */
/*4501*/    OP_DUMP, 485,
/*4503*/    OP_COPY, 0, 7605, 2, /* _cs */
/*4507*/    OP_DUMP, 486,
/*4509*/    OP_COPY, 0, 7604, 1, /* _flag */
/*4513*/    OP_DUMP, 487,
/*4515*/    OP_COPY, 1, 7600, 3, /* _action_ind */
/*4519*/    OP_DUMP, 488,
/*4521*/    OP_BEGIN_LEN, 2, 7608, 16, /* _ie_len */
/*4525*/    OP_BEGIN_REC, RECOV_IND_IE, 198, 4609,
/*4529*/    OP_IFEND, 77, /* _id */
/*4531*/    OP_DUMP, 489,
/*4533*/    OP_COPY, 0, 0, 8, /* _id */
/*4537*/    OP_CASE, 1, 0, 8, 6, /* _id */
/*4542*/    ATM_TD_FW_PCR_0, 199, 15,
/*4545*/    ATM_TD_BW_PCR_0, 200, 20,
/*4548*/    ATM_TD_FW_PCR_01, 201, 25,
/*4551*/    ATM_TD_BW_PCR_01, 202, 30,
/*4554*/    ATM_TD_FW_MCR_01, 203, 35,
/*4557*/    ATM_TD_BW_MCR_01, 204, 40,
/*4560*/    OP_DUMP, 490,
/*4562*/    OP_COPY, 3, 7632, 24, /* minfw_pcr_0 */
/*4566*/    OP_JUMP, 38,
/*4568*/    OP_DUMP, 491,
/*4570*/    OP_COPY, 3, 7664, 24, /* minbw_pcr_0 */
/*4574*/    OP_JUMP, 30,
/*4576*/    OP_DUMP, 492,
/*4578*/    OP_COPY, 3, 7696, 24, /* minfw_pcr_01 */
/*4582*/    OP_JUMP, 22,
/*4584*/    OP_DUMP, 493,
/*4586*/    OP_COPY, 3, 7728, 24, /* minbw_pcr_01 */
/*4590*/    OP_JUMP, 14,
/*4592*/    OP_DUMP, 494,
/*4594*/    OP_COPY, 3, 7760, 24, /* minfw_mcr_01 */
/*4598*/    OP_JUMP, 6,
/*4600*/    OP_DUMP, 495,
/*4602*/    OP_COPY, 3, 7792, 24, /* minbw_mcr_01 */
/*4606*/    OP_JUMP, -79,
/*4608*/    OP_END_REC,
/*4609*/    OP_END_LEN,  /* _ie_len */
/*4610*/    OP_JUMP, 522,
/*4612*/    OP_DUMP, 496,
/*4614*/    OP_COPY, 0, 7831, 1, /* _ext */
/*4618*/    OP_DUMP, 497,
/*4620*/    OP_COPY, 0, 7829, 2, /* _cs */
/*4624*/    OP_DUMP, 498,
/*4626*/    OP_COPY, 0, 7828, 1, /* _flag */
/*4630*/    OP_DUMP, 499,
/*4632*/    OP_COPY, 1, 7824, 3, /* _action_ind */
/*4636*/    OP_DUMP, 500,
/*4638*/    OP_BEGIN_LEN, 2, 7832, 16, /* _ie_len */
/*4642*/    OP_BEGIN_REC, RECOV_IND_IE, 205, 4732,
/*4646*/    OP_DUMP, 501,
/*4648*/    OP_COPY, 1, 7848, 8, /* eqos_origin */
/*4652*/    OP_IFEND, 77, /* _id */
/*4654*/    OP_DUMP, 502,
/*4656*/    OP_COPY, 0, 0, 8, /* _id */
/*4660*/    OP_CASE, 1, 0, 8, 6, /* _id */
/*4665*/    ATM_EQP_ACC_FW_CDV, 206, 15,
/*4668*/    ATM_EQP_ACC_BW_CDV, 207, 20,
/*4671*/    ATM_EQP_CUM_FW_CDV, 208, 25,
/*4674*/    ATM_EQP_CUM_BW_CDV, 209, 30,
/*4677*/    ATM_EQP_ACC_FW_CLR, 210, 35,
/*4680*/    ATM_EQP_ACC_BW_CLR, 211, 40,
/*4683*/    OP_DUMP, 503,
/*4685*/    OP_COPY, 3, 7864, 24, /* acc_fw_cdv */
/*4689*/    OP_JUMP, 38,
/*4691*/    OP_DUMP, 504,
/*4693*/    OP_COPY, 3, 7896, 24, /* acc_bw_cdv */
/*4697*/    OP_JUMP, 30,
/*4699*/    OP_DUMP, 505,
/*4701*/    OP_COPY, 3, 7928, 24, /* cum_fw_cdv */
/*4705*/    OP_JUMP, 22,
/*4707*/    OP_DUMP, 506,
/*4709*/    OP_COPY, 3, 7960, 24, /* cum_bw_cdv */
/*4713*/    OP_JUMP, 14,
/*4715*/    OP_DUMP, 507,
/*4717*/    OP_COPY, 1, 7992, 8, /* acc_fw_clr */
/*4721*/    OP_JUMP, 6,
/*4723*/    OP_DUMP, 508,
/*4725*/    OP_COPY, 1, 8008, 8, /* acc_bw_clr */
/*4729*/    OP_JUMP, -79,
/*4731*/    OP_END_REC,
/*4732*/    OP_END_LEN,  /* _ie_len */
/*4733*/    OP_JUMP, 399,
/*4735*/    OP_DUMP, 509,
/*4737*/    OP_COPY, 0, 8031, 1, /* _ext */
/*4741*/    OP_DUMP, 510,
/*4743*/    OP_COPY, 0, 8029, 2, /* _cs */
/*4747*/    OP_DUMP, 511,
/*4749*/    OP_COPY, 0, 8028, 1, /* _flag */
/*4753*/    OP_DUMP, 512,
/*4755*/    OP_COPY, 1, 8024, 3, /* _action_ind */
/*4759*/    OP_DUMP, 513,
/*4761*/    OP_BEGIN_LEN, 2, 8032, 16, /* _ie_len */
/*4765*/    OP_BEGIN_REC, RECOV_IND_IE, 212, 4805,
/*4769*/    OP_IFEND, 33, /* _id */
/*4771*/    OP_DUMP, 514,
/*4773*/    OP_COPY, 0, 0, 8, /* _id */
/*4777*/    OP_CASE, 1, 0, 8, 2, /* _id */
/*4782*/    ATM_AAP_FW_REC, 213, 3,
/*4785*/    ATM_AAP_BW_REC, 214, 8,
/*4788*/    OP_DUMP, 515,
/*4790*/    OP_COPY, 4, 8056, 32, /* abr_fw_add_rec */
/*4794*/    OP_JUMP, 6,
/*4796*/    OP_DUMP, 516,
/*4798*/    OP_COPY, 4, 8096, 32, /* abr_bw_add_rec */
/*4802*/    OP_JUMP, -35,
/*4804*/    OP_END_REC,
/*4805*/    OP_END_LEN,  /* _ie_len */
/*4806*/    OP_JUMP, 326,
/*4808*/    OP_DUMP, 517,
/*4810*/    OP_COPY, 0, 8143, 1, /* _ext */
/*4814*/    OP_DUMP, 518,
/*4816*/    OP_COPY, 0, 8141, 2, /* _cs */
/*4820*/    OP_DUMP, 519,
/*4822*/    OP_COPY, 0, 8140, 1, /* _flag */
/*4826*/    OP_DUMP, 520,
/*4828*/    OP_COPY, 1, 8136, 3, /* _action_ind */
/*4832*/    OP_DUMP, 521,
/*4834*/    OP_BEGIN_LEN, 2, 8144, 16, /* _ie_len */
/*4838*/    OP_BEGIN_REC, RECOV_IND_IE, 215, 4955,
/*4842*/    OP_IFEND, 110, /* _id */
/*4844*/    OP_DUMP, 522,
/*4846*/    OP_COPY, 0, 0, 8, /* _id */
/*4850*/    OP_CASE, 1, 0, 8, 9, /* _id */
/*4855*/    ATM_ASP_FW_ICR, 216, 24,
/*4858*/    ATM_ASP_BW_ICR, 217, 29,
/*4861*/    ATM_ASP_FW_TBE, 218, 34,
/*4864*/    ATM_ASP_BW_TBE, 219, 39,
/*4867*/    ATM_ASP_CRF_RTT, 220, 44,
/*4870*/    ATM_ASP_FW_RIF, 221, 49,
/*4873*/    ATM_ASP_BW_RIF, 222, 54,
/*4876*/    ATM_ASP_FW_RDF, 223, 59,
/*4879*/    ATM_ASP_BW_RDF, 224, 64,
/*4882*/    OP_DUMP, 523,
/*4884*/    OP_COPY, 3, 8168, 24, /* abr_fw_icr */
/*4888*/    OP_JUMP, 62,
/*4890*/    OP_DUMP, 524,
/*4892*/    OP_COPY, 3, 8200, 24, /* abr_bw_icr */
/*4896*/    OP_JUMP, 54,
/*4898*/    OP_DUMP, 525,
/*4900*/    OP_COPY, 3, 8232, 24, /* abr_fw_tbe */
/*4904*/    OP_JUMP, 46,
/*4906*/    OP_DUMP, 526,
/*4908*/    OP_COPY, 3, 8264, 24, /* abr_bw_tbe */
/*4912*/    OP_JUMP, 38,
/*4914*/    OP_DUMP, 527,
/*4916*/    OP_COPY, 3, 8296, 24, /* atm_crf_rtt */
/*4920*/    OP_JUMP, 30,
/*4922*/    OP_DUMP, 528,
/*4924*/    OP_COPY, 1, 8328, 8, /* atm_fw_rif */
/*4928*/    OP_JUMP, 22,
/*4930*/    OP_DUMP, 529,
/*4932*/    OP_COPY, 1, 8344, 8, /* atm_bw_rif */
/*4936*/    OP_JUMP, 14,
/*4938*/    OP_DUMP, 530,
/*4940*/    OP_COPY, 1, 8360, 8, /* atm_fw_rdf */
/*4944*/    OP_JUMP, 6,
/*4946*/    OP_DUMP, 531,
/*4948*/    OP_COPY, 1, 8376, 8, /* atm_bw_rdf */
/*4952*/    OP_JUMP, -112,
/*4954*/    OP_END_REC,
/*4955*/    OP_END_LEN,  /* _ie_len */
/*4956*/    OP_JUMP, 176,
/*4958*/    OP_DUMP, 532,
/*4960*/    OP_COPY, 0, 8399, 1, /* _ext */
/*4964*/    OP_DUMP, 533,
/*4966*/    OP_COPY, 0, 8397, 2, /* _cs */
/*4970*/    OP_DUMP, 534,
/*4972*/    OP_COPY, 0, 8396, 1, /* _flag */
/*4976*/    OP_DUMP, 535,
/*4978*/    OP_COPY, 1, 8392, 3, /* _action_ind */
/*4982*/    OP_DUMP, 536,
/*4984*/    OP_BEGIN_LEN, 2, 8400, 16, /* _ie_len */
/*4988*/    OP_BEGIN_REC, RECOV_IND_IE, 225, 5005,
/*4992*/    OP_DUMP, 537,
/*4994*/    OP_COPY, 1, 8416, 8, /* _ep_type */
/*4998*/    OP_DUMP, 538,
/*5000*/    OP_COPY, 2, 8424, 16, /* ep_ref */
/*5004*/    OP_END_REC,
/*5005*/    OP_END_LEN,  /* _ie_len */
/*5006*/    OP_JUMP, 126,
/*5008*/    OP_DUMP, 539,
/*5010*/    OP_COPY, 0, 8455, 1, /* _ext */
/*5014*/    OP_DUMP, 540,
/*5016*/    OP_COPY, 0, 8453, 2, /* _cs */
/*5020*/    OP_DUMP, 541,
/*5022*/    OP_COPY, 0, 8452, 1, /* _flag */
/*5026*/    OP_DUMP, 542,
/*5028*/    OP_COPY, 1, 8448, 3, /* _action_ind */
/*5032*/    OP_DUMP, 543,
/*5034*/    OP_BEGIN_LEN, 2, 8456, 16, /* _ie_len */
/*5038*/    OP_BEGIN_REC, RECOV_IND_IE, 226, 5049,
/*5042*/    OP_DUMP, 544,
/*5044*/    OP_COPY, 1, 8472, 6, /* ep_state */
/*5048*/    OP_END_REC,
/*5049*/    OP_END_LEN,  /* _ie_len */
/*5050*/    OP_JUMP, 82,
/*5052*/    OP_DUMP, 545,
/*5054*/    OP_COPY, 0, 8495, 1, /* _ext */
/*5058*/    OP_DUMP, 546,
/*5060*/    OP_COPY, 0, 8493, 2, /* _cs */
/*5064*/    OP_DUMP, 547,
/*5066*/    OP_COPY, 0, 8492, 1, /* _flag */
/*5070*/    OP_DUMP, 548,
/*5072*/    OP_COPY, 1, 8488, 3, /* _action_ind */
/*5076*/    OP_DUMP, 549,
/*5078*/    OP_BEGIN_LEN, 2, 8496, 16, /* _ie_len */
/*5082*/    OP_BEGIN_REC, RECOV_IND_IE, 227, 5093,
/*5086*/    OP_DUMP, 550,
/*5088*/    OP_COPY, 1, 8512, 8, /* type_of_report */
/*5092*/    OP_END_REC,
/*5093*/    OP_END_LEN,  /* _ie_len */
/*5094*/    OP_JUMP, 38,
/*5096*/    OP_DUMP, 551,
/*5098*/    OP_COPY, 0, 8535, 1, /* _ext */
/*5102*/    OP_DUMP, 552,
/*5104*/    OP_COPY, 0, 8533, 2, /* __cs */
/*5108*/    OP_DUMP, 553,
/*5110*/    OP_COPY, 0, 8532, 1, /* _flag */
/*5114*/    OP_DUMP, 554,
/*5116*/    OP_COPY, 1, 8528, 3, /* _action_ind */
/*5120*/    OP_DUMP, 555,
/*5122*/    OP_BEGIN_LEN, 2, 8536, 16, /* _ie_len */
/*5126*/    OP_BEGIN_REC, RECOV_IND_IE, 228, 5133,
/*5130*/    OP_ABORT, RECOV_ASE_UNKNOWN_IE,
/*5132*/    OP_END_REC,
/*5133*/    OP_END_LEN,  /* _ie_len */
/*5134*/    OP_JUMP, -5088,
/*5136*/    OP_END_LEN,  /* msg_len */
/*5137*/    OP_END
};


/*
 * Sorry, this is necessary ...
 */

#include "qlib.c"
