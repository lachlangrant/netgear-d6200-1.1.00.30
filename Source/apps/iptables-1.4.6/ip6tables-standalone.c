/*
 * Author: Paul.Russell@rustcorp.com.au and mneuling@radlogic.com.au
 *
 * (C) 2000-2002 by the netfilter coreteam <coreteam@netfilter.org>:
 * 		    Paul 'Rusty' Russell <rusty@rustcorp.com.au>
 * 		    Marc Boucher <marc+nf@mbsi.ca>
 * 		    James Morris <jmorris@intercode.com.au>
 * 		    Harald Welte <laforge@gnumonks.org>
 * 		    Jozsef Kadlecsik <kadlec@blackhole.kfki.hu>
 *
 * Based on the ipchains code by Paul Russell and Michael Neuling
 *
 *	iptables -- IP firewall administration for kernels with
 *	firewall table (aimed for the 2.3 kernels)
 *
 *	See the accompanying manual page iptables(8) for information
 *	about proper usage of this program.
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ip6tables.h>
#include "ip6tables-multi.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>


#ifdef IPTABLES_MULTI
int
ip6tables_main(int argc, char *argv[])
#else
int
main(int argc, char *argv[])
#endif
{
	int ret;
	char *table = "filter";
	struct ip6tc_handle *handle = NULL;

	ip6tables_globals.program_name = "ip6tables";
	ret = xtables_init_all(&ip6tables_globals, NFPROTO_IPV6);
	if (ret < 0) {
		fprintf(stderr, "%s/%s Failed to initialize xtables\n",
				ip6tables_globals.program_name,
				ip6tables_globals.program_version);
		exit(1);
	}

#if defined(ALL_INCLUSIVE) || defined(NO_SHARED_LIBS)
	init_extensions();
#endif

	if (argv[1] && strcmp(argv[1],"-C") == 0) {
		FILE *fp=NULL;
		char buf[512]="";
		char *margv[128];
		int margc=0;
		pid_t pid;

		if (argv[2] == NULL) {
			printf("%s -C rules\n",argv[0]);
			return -1;
		}

		fp = fopen(argv[2],"r");

		while (fgets(buf,512,fp)) {
			char *sp1, *sp2;
			if (strlen(buf) < 10) {
				continue;
			}
			if (((sp1=strstr(buf, "\"")) != NULL) &&
				((sp2=strstr(sp1+1, "\"")) != NULL)) {
				*(sp1++)=' ';
				*(sp2--)=' ';
				while(sp1 && (sp1++<sp2)) {
					if ((sp1=strstr(sp1, " ")) != NULL) {
						*sp1 = '\1';
					}
				}
			}
			margc = 0;
			margv[margc++] = strtok(buf," ");
			while ((margv[margc]=strtok(NULL," "))){
				int i = strlen(margv[margc]);
				if (strcmp(margv[margc], "\n") == 0) {
					continue;
				}
				sp1 = margv[margc];
				while ((sp1=strstr(sp1, "\1")) != NULL) {
					*(sp1++)=' ';
				}

				if (margv[margc][i-1] == '\n') {
					margv[margc][i-1] = '\0';
				}
				margc++;
			};	
			if (margv[0] && margv[0][0] == '#') {
                continue;
			}

            margv[0] = argv[0];
            /* fork to process this command */
            if ((pid = fork()) < 0) {
                perror("fork");
                exit(1);
            } else if (pid == 0) {
                ret = do_command6(margc, margv, &table, &handle);
                if (ret) {
                    ret = ip6tc_commit(handle);
				}
                exit(!ret);
            } else {
                int status;
                waitpid(pid,&status,WUNTRACED);
            }
        }
        fclose(fp);

    }else{
		ret = do_command6(argc, argv, &table, &handle);
		if (ret) {
			ret = ip6tc_commit(handle);
			//ip6tc_free(handle);
		}
	}

	if (!ret) {
		if (errno == EINVAL) {
			fprintf(stderr, "ip6tables: %s. "
					"Run `dmesg' for more information.\n",
				ip6tc_strerror(errno));
		} else {
			fprintf(stderr, "ip6tables: %s.\n",
				ip6tc_strerror(errno));
		}
	}

	exit(!ret);
}
