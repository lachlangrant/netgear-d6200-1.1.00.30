/* PPPoE support library "libpppoe"
 *
 * Copyright 2000 Michal Ostrowski <mostrows@styx.uwaterloo.ca>,
 *		  Jamal Hadi Salim <hadi@cyberus.ca>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version
 *  2 of the License, or (at your option) any later version.
 */

#include "pppoe.h"


extern char	*pppoe_srv_name;

#ifdef MULTI_OFFER
#define MAX_BLACKLIST_NUM  (5)
static unsigned char blacklist[MAX_BLACKLIST_NUM][ETH_ALEN] = {{0}};
static struct sockaddr_ll init_remote;

static int isblackhost(unsigned char *addr)
{
	int i;

	for(i = 0; i < MAX_BLACKLIST_NUM; i++)
	{
/*		error("isblackhost %02X:%02X:%02X:%02X:%02X:%02X",
				blacklist[i][0],
				blacklist[i][1],
				blacklist[i][2],
				blacklist[i][3],
				blacklist[i][4],
				blacklist[i][5]);*/
		if (memcmp(blacklist[i], addr, sizeof(blacklist[0])) == 0)
		{
			return 1;
		}
	}

	return 0;
}
void InitBlacklist()
{
	memset(blacklist, 0, sizeof(blacklist));
}
void removefromBlacklist(unsigned char *addr)
{
	int i;
	unsigned char zero[ETH_ALEN];
	int found = 0;

	memset(zero, 0, sizeof(zero));

	for (i = 0; i < MAX_BLACKLIST_NUM; i++)
	{
		if (!found && memcmp(blacklist[i], addr, sizeof(zero)) == 0)
		{
			found = 1;
		}

		if (found)
		{
			if (i < MAX_BLACKLIST_NUM - 1)
			{
				memcpy(blacklist[i], blacklist[i + 1], sizeof(zero));

				if (memcmp(blacklist[i], zero, sizeof(zero)) == 0)
				{
					break;
				}
			}
			else
			{
				memset(blacklist[i], 0, sizeof(zero));
			}
		}
	}
}

int addToBlacklist(unsigned char *addr)
{
	int i;
	unsigned char zero[ETH_ALEN];

	memset(zero, 0, sizeof(zero));
/* error("addToBlacklist %02X:%02X:%02X:%02X:%02X:%02X",
            addr[0],
            addr[1],
            addr[2],
            addr[3],
            addr[4],
            addr[5]); */
	for(i = 0; i < MAX_BLACKLIST_NUM; i++)
	{
		if (memcmp(blacklist[i], zero, sizeof(zero)) == 0)
		{
			memcpy(blacklist[i], addr, sizeof(zero));
			return 0;
		}
		else if (memcmp(blacklist[i], addr, sizeof(zero)) == 0)
		{
			return 0;
		}
	}
	return 1;
}
static int std_rcv_timeout(struct session* ses,
		struct pppoe_packet *p_in,
		struct pppoe_packet **p_out)
{
		if(ses->state == PADO_CODE)
		{
			poe_info(ses,"Clear blacklist." );
			memset(blacklist, 0, sizeof(blacklist));
		}

	    if(ses->state == PADS_CODE && ses->retransmits >= 3 /* retry 4 times*/)
	    {
			if (addToBlacklist((*p_out)->addr.sll_addr))
			{
				poe_info(ses, "Blacklist is full");
			}else
			{
				poe_info(ses, "Add %E to blacklist.", (*p_out)->addr.sll_addr);
			}

			memset(&ses->curr_pkt, 0, sizeof(struct pppoe_packet));

			ses->curr_pkt.hdr = (struct pppoe_hdr*) ses->curr_pkt.buf;
			ses->curr_pkt.hdr->ver = 1;
			ses->curr_pkt.hdr->type = 1;
			ses->curr_pkt.hdr->code = PADI_CODE;

			ses->state = PADO_CODE;

			memcpy(&ses->curr_pkt.addr, &init_remote, sizeof(struct sockaddr_ll));

			poe_info(ses, "Resending PADI");
			if (DEB_DISC)
				poe_dbglog(ses, "Resending PADI");

			ses->retransmits = 0;

			if (ses->filt->ntag)
			{
				ses->curr_pkt.tags[TAG_AC_NAME] = ses->filt->ntag;
				poe_info(ses, "overriding AC name\n");
			}

			/* Ron fixed add pppoe srv support */
			if (pppoe_srv_name != NULL)
			{
				if (strlen(pppoe_srv_name) > 255)
				{
					poe_error(ses,
							" Service name too long (maximum allowed 256 chars)");
					poe_die(-1);
				}
				ses->filt->stag = make_filter_tag(PTT_SRV_NAME, strlen(
						pppoe_srv_name), pppoe_srv_name);
				if (ses->filt->stag == NULL)
				{
					poe_error(ses, "failed to malloc for service name");
					poe_die(-1);
				}
				printf("pppoe_srv_name=%s\n", pppoe_srv_name);
			}

			if (ses->filt->stag)
				ses->curr_pkt.tags[TAG_SRV_NAME] = ses->filt->stag;
			else
				printf("ses->filt->stag==NULL\n");

			if (ses->filt->htag)
				ses->curr_pkt.tags[TAG_HOST_UNIQ] = ses->filt->htag;

			send_disc(ses, &ses->curr_pkt);
			(*p_out) = &ses->curr_pkt;
		}else if(p_out && *p_out)
	    {
	    	++ses->retransmits;
	    	send_disc(ses,*p_out);
	    }

		return 0;
}
#endif /* MULTI_OFFER */
static int std_rcv_pado(struct session* ses,
			struct pppoe_packet *p_in,
			struct pppoe_packet **p_out){

    if( verify_packet(ses, p_in) < 0)
	return -1;

    if(ses->state != PADO_CODE ){
	poe_error(ses,"Unexpected packet: %P",p_in);
	return 0;
    }


    if (DEB_DISC2) {
	poe_dbglog (ses,"PADO received: %P", p_in);
    }
#ifdef MULTI_OFFER
/*  follow Netgear spec 1.9
	In multiple offer case from server, because GUI doesn't support to identify concentrator
	name.
	If router has experienced that the offer from this server is failed, router should ignore this
	offer and wait for other offers from different servers.
*/
    if(isblackhost(p_in->addr.sll_addr))
    {
    	poe_info (ses,"Host in blacklist. %P", p_in);
    	return 0;
    }else
    {
    	poe_info (ses,"Host not in blacklist. %P", p_in);
    }
#endif
    memcpy(&ses->remote, &p_in->addr, sizeof(struct sockaddr_ll));
    memcpy( &ses->curr_pkt.addr, &ses->remote , sizeof(struct sockaddr_ll));

    ses->curr_pkt.hdr->code = PADR_CODE;

    /* The HOST_UNIQ has been verified already... there's no "if" about this */
    /* if(ses->filt->htag) */
    copy_tag(&ses->curr_pkt,get_tag(p_in->hdr,PTT_HOST_UNIQ));

    if (ses->filt->ntag) {
    	ses->curr_pkt.tags[TAG_AC_NAME]=NULL;
    }
//    copy_tag(&ses->curr_pkt,get_tag(p_in->hdr,PTT_AC_NAME));

    if(ses->filt->stag) {
    	ses->curr_pkt.tags[TAG_SRV_NAME]=NULL;
    }
    copy_tag(&ses->curr_pkt,get_tag(p_in->hdr,PTT_SRV_NAME));

    copy_tag(&ses->curr_pkt,get_tag(p_in->hdr,PTT_AC_COOKIE));
    copy_tag(&ses->curr_pkt,get_tag(p_in->hdr,PTT_RELAY_SID));

    ses->state = PADS_CODE;

    ses->retransmits = 0;

    send_disc(ses, &ses->curr_pkt);
    (*p_out) = &ses->curr_pkt;
#ifdef MULTI_OFFER
	if (addToBlacklist((*p_out)->addr.sll_addr))
	{
		poe_info(ses, "LINE[%d] Blacklist is full", __LINE__);
	}else
	{
		poe_info(ses, "Add %E to blacklist after send PADR.", (*p_out)->addr.sll_addr);
	}
#endif /* MULTI_OFFER */
    if (ses->np)
	return 1;

    return 0;
}

static int std_init_disc(struct session* ses,
			 struct pppoe_packet *p_in,
			 struct pppoe_packet **p_out){

    memset(&ses->curr_pkt,0, sizeof(struct pppoe_packet));


    /* Check if already connected */
    if( ses->state != PADO_CODE ){
	return 1;
    }

#ifdef _RECORD_SID_
/* Send PADT before PADI to terminate old Session */
/*
 * Netgear Spec V1.9
 *
 * To avoid the case that server will limit the PPPoE sessions accounts, client should
 * terminate previous session before establish a new session. Client should record the
 * session id into flash whenever the connection is established.
 * (Client should record the session id into flash, and send PADT to terminate these
 * sessions before sending PADI).
 */
    poe_info (ses, "Sending PADT");
    send_padt(ses, &ses->curr_pkt);
#endif

    ses->curr_pkt.hdr = (struct pppoe_hdr*) ses->curr_pkt.buf;
    ses->curr_pkt.hdr->ver  = 1;
    ses->curr_pkt.hdr->type = 1;
    ses->curr_pkt.hdr->code = PADI_CODE;

    memcpy( &ses->curr_pkt.addr, &ses->remote , sizeof(struct sockaddr_ll));

    poe_info (ses,"Sending PADI");
    if (DEB_DISC)
	poe_dbglog (ses,"Sending PADI");

    ses->retransmits = 0 ;

    if(ses->filt->ntag) {
	ses->curr_pkt.tags[TAG_AC_NAME]=ses->filt->ntag;
	poe_info(ses,"overriding AC name\n");
    }

    /* Ron fixed add pppoe srv support */
    if (pppoe_srv_name !=NULL) {
	if (strlen (pppoe_srv_name) > 255) {
	    poe_error (ses," Service name too long (maximum allowed 256 chars)");
	    poe_die(-1);
	}
	ses->filt->stag = make_filter_tag(PTT_SRV_NAME,
					  strlen(pppoe_srv_name),
					  pppoe_srv_name);
	if ( ses->filt->stag == NULL) {
	    poe_error (ses,"failed to malloc for service name");
	    poe_die(-1);
	}
	printf("pppoe_srv_name=%s\n",pppoe_srv_name);
    }

    if(ses->filt->stag)
	ses->curr_pkt.tags[TAG_SRV_NAME]=ses->filt->stag;
    else
    	printf("ses->filt->stag==NULL\n");

    if(ses->filt->htag)
	ses->curr_pkt.tags[TAG_HOST_UNIQ]=ses->filt->htag;

//#ifndef _RECORD_SID_
//    ses->curr_pkt.hdr->code = PADT_CODE;
//    send_disc(ses, &ses->curr_pkt);
//#endif

    ses->curr_pkt.hdr->code = PADI_CODE;
    send_disc(ses, &ses->curr_pkt);
    (*p_out)= &ses->curr_pkt;
    return 0;
}


static int std_rcv_pads(struct session* ses,
			struct pppoe_packet *p_in,
			struct pppoe_packet **p_out){
    if( verify_packet(ses, p_in) < 0)
	return -1;

    if (DEB_DISC)
	poe_dbglog (ses,"Got connection: %x",
		    ntohs(p_in->hdr->sid));
    poe_info (ses,"Got connection: %x", ntohs(p_in->hdr->sid));

    ses->sp.sa_family = AF_PPPOX;
    ses->sp.sa_protocol = PX_PROTO_OE;
    ses->sp.sa_addr.pppoe.sid = p_in->hdr->sid;

    memcpy(ses->sp.sa_addr.pppoe.dev,ses->name, IFNAMSIZ);
    memcpy(ses->sp.sa_addr.pppoe.remote, p_in->addr.sll_addr, ETH_ALEN);


    return 1;
}

static int std_rcv_padt(struct session* ses,
			struct pppoe_packet *p_in,
			struct pppoe_packet **p_out){
    ses->state = PADO_CODE;
    return 0;
}



extern int disc_sock;
int client_init_ses (struct session *ses, char* devnam)
{
    int i=0;
    int retval;
    char dev[IFNAMSIZ+1];
    int addr[ETH_ALEN];
    int sid;

    /* do error checks here; session name etc are valid */
//    poe_info (ses,"init_ses: creating socket");

    /* Make socket if necessary */
    if( disc_sock < 0 ){

	disc_sock = socket(PF_PACKET, SOCK_DGRAM, 0);
	if( disc_sock < 0 ){
	    poe_fatal(ses,
		      "Cannot create PF_PACKET socket for PPPoE discovery\n");
	}

    }

    /* Check for long format */
    retval =sscanf(devnam, FMTSTRING(IFNAMSIZ),addr, addr+1, addr+2,
		   addr+3, addr+4, addr+5,&sid,dev);
    if( retval != 8 ){
	/* Verify the device name , construct ses->local */
	retval = get_sockaddr_ll(devnam,&ses->local);
	if (retval < 0)
	    poe_fatal(ses, "client_init_ses: "
		      "Cannot create PF_PACKET socket for PPPoE discovery\n");


	ses->state = PADO_CODE;
	memcpy(&ses->remote, &ses->local, sizeof(struct sockaddr_ll) );

	memset( ses->remote.sll_addr, 0xff, ETH_ALEN);
    }else{
	/* long form parsed */

	/* Verify the device name , construct ses->local */
	retval = get_sockaddr_ll(dev,&ses->local);
	if (retval < 0)
	    poe_fatal(ses,"client_init_ses(2): "
		      "Cannot create PF_PACKET socket for PPPoE discovery\n");
	ses->state = PADS_CODE;
	ses->sp.sa_family = AF_PPPOX;
	ses->sp.sa_protocol = PX_PROTO_OE;
	ses->sp.sa_addr.pppoe.sid = sid;

	memcpy(&ses->remote, &ses->local, sizeof(struct sockaddr_ll) );

	for(; i < ETH_ALEN ; ++i ){
	    ses->sp.sa_addr.pppoe.remote[i] = addr[i];
	    ses->remote.sll_addr[i]=addr[i];
	}
	memcpy(ses->sp.sa_addr.pppoe.dev, dev, IFNAMSIZ);



    }
    if( retval < 0 )
	error("bad device name: %s",devnam);


    retval = bind( disc_sock ,
		   (struct sockaddr*)&ses->local,
		   sizeof(struct sockaddr_ll));


    if( retval < 0 ){
	error("bind to PF_PACKET socket failed: %m");
    }

    ses->fd = socket(AF_PPPOX,SOCK_STREAM,PX_PROTO_OE);
    if(ses->fd < 0)
    {
	poe_fatal(ses,"Failed to create PPPoE socket: %m");
    }


    ses->init_disc = std_init_disc;
    ses->rcv_pado  = std_rcv_pado;
    ses->rcv_pads  = std_rcv_pads;
    ses->rcv_padt  = std_rcv_padt;
#ifdef MULTI_OFFER
    /* Jacky add */
    memcpy(&init_remote, &ses->remote, sizeof(struct sockaddr_ll) );
    ses->timeout = std_rcv_timeout;
#endif
    /* this should be filter overridable */
    ses->retries = 10;

    return ses->fd;
}

