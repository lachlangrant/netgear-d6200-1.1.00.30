#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <mtd/mtd-user.h>
#include <mtd/jffs2-user.h>
#include "upgrade_flash.h"
#include "filenames.h"
#include "flash.h"
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

//#define _SC_DEBUG_ENABLE_
#include "sc_debug.h"

#ifdef BOOT_NAND_FLASH
#define TMP_LOADER      "/tmp/loader"
#else
#define TMP_LOADER      LOADER
#endif

/*
 * flash_get_str() -- Get item from FLASH protect area to @buf.
 *  length of buf is @buf_len
 *  Offset of item is @offset, length of this item is @len
 *  A '\0' is stored after the last character in the buffer.
 *
 * Return buf if success. NULL if error
 */
static char* flash_get_str( char* buf, int buf_len, int offset, int len)
{
    FILE *fp;

    if(buf_len < len+1) {
        return NULL;
    }

    fp = fopen(LOADER, "r");
    fseek(fp, offset, 0);
    fread(buf, len, 1, fp);
    fclose(fp);
    buf[len] = '\0';
    return buf;
}

static FILE* loader_fp;

#ifdef BCM_BSP
#define CRC32_INIT_VALUE 0xffffffff /* Initial CRC32 checksum value */
#define CRC_LEN 4

static unsigned long Crc32_table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

static unsigned long getCrc32(char *pdata, unsigned long size, unsigned long crc)
{
    while (size-- > 0)
        crc = (crc >> 8) ^ Crc32_table[(crc ^ *pdata++) & 0xff];

    return crc;
}
int sc_adjust_nvram_checksum()
{
    unsigned long crc = CRC32_INIT_VALUE,pre_crc;
    PNVRAM_DATA pNvramData = malloc(sizeof(NVRAM_DATA));
    
    if(pNvramData == NULL)
        return -1;
        
    memset(pNvramData, 0,sizeof(NVRAM_DATA));
    fseek(loader_fp, NVRAM_DATA_OFFSET, 0);
    fread(pNvramData,sizeof(NVRAM_DATA),1,loader_fp);
    pre_crc = pNvramData->ulCheckSum ;
    
    pNvramData->ulCheckSum = 0;    
    crc = getCrc32((char *)pNvramData, sizeof(NVRAM_DATA), crc);
    SC_XCFPRINTF("%s: ulCheckSum: %lu -> %lu\n",__FUNCTION__,pre_crc,crc);
    pNvramData->ulCheckSum = crc;
    
    fseek(loader_fp, NVRAM_DATA_OFFSET, 0);
    fwrite(pNvramData,sizeof(NVRAM_DATA),1,loader_fp);
    
    free(pNvramData);
    return 0;
}
#endif
/* close mtd block */
int sc_set_close()
{
    int ret = 0;
#ifdef BCM_BSP
    sc_adjust_nvram_checksum();
#endif
    ret = fclose(loader_fp);
#ifdef BOOT_NAND_FLASH
    if(ret == 0)
        system("/bin/flash_eraseall -j -q "LOADER"; /bin/cp "TMP_LOADER" "LOADER);
#endif
    return ret;
}


/* open mtd block */
int sc_set_open()
{
#ifdef BOOT_NAND_FLASH
    system("/bin/cp "LOADER" "TMP_LOADER);
#endif
    if ((loader_fp=fopen(TMP_LOADER, "r+")) != NULL) {
        return 1;
    } else {
        return 0;
    }
}
/*
 * flash_set_str() -- Set value of @buf to FLASH protect area.
 *  length of buf is @buf_len
 *  Offset of item is @offset, length of this item is @len
 *
 * Return 0 if success. -1 if error
 */
static int flash_set_str(char* buf, int offset, int len)
{
    fseek(loader_fp, offset, 0);
    if (fwrite(buf, len, 1, loader_fp)) {
        return 0;
    }
    return -1;
}

/*
 * sc_get_pin() -- get WPS PIN from FLASH to @buf.
 * A '\0' is stored after the last character in the buffer.
 * Return buf if success. NULL if error
 */
char* sc_get_pin( char* buf, int len)
{
    flash_get_str(buf, len, WPSPIN_OFFSET, WPSPIN_LEN);
    if(strspn(buf,"0123456789")!=8)
    {
        strcpy(buf,"94229882"); //Default
    }
    
    return buf;
}

/*
 * sc_get_sn() -- get SN from FLASH to @buf.
 * A '\0' is stored after the last character in the buffer.
 * Return buf if success. NULL if error
 */
char* sc_get_sn( char* buf, int len)
{
    return sc_get_csn(buf,len);
}

/*
 * sc_get_csn() -- get CSN from FLASH to @buf.
 * A '\0' is stored after the last character in the buffer.
 * Return buf if success. NULL if error
 */
char* sc_get_csn( char* buf, int len)
{
    flash_get_str(buf, len, CSN_OFFSET, CSN_LEN);
    {
        char *ff = strchr(buf,0xff);
        if(ff)
            *ff = '\0';
    }
    if(!isascii(buf[0]) || strlen(buf) == 0) {
        /*
         * PCBA without assign SN will return 0xFFFFFFFFF,
         * but this is invalid. Copy default value for safe
         */
        strcpy(buf, "1234567890");
    }
    return buf;
}
/*
 * sc_get_mac() -- get mac from FLASH to @buf.
 * A '\0' is stored after the last character in the buffer.
 * Return buf if success. NULL if error
 */
char* sc_get_mac( char* buf, int len)
{
    unsigned char mac[7];

    flash_get_str((char *)mac, sizeof(mac), MAC_OFFSET, MAC_LEN);
    sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    /* if not assign, the value should be FF:FF:FF:FF:FF:FF, so use the default MAC */
    if (strcmp(buf, "FF:FF:FF:FF:FF:FF") == 0) {
        strcpy(buf, "00:c0:02:12:35:88");
    }
    return buf;
}

/*
 * sc_get_mac_if() -- get MAC address of @ifname
 */
int sc_get_mac_if( char *ifname, char mac[6])
{
    int sockd;
    struct ifreq ifr;

    sockd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockd == -1)
        return -1;
    strcpy(ifr.ifr_name, ifname);
    if(ioctl(sockd, SIOCGIFHWADDR, &ifr) < 0)
        return -1;
    memcpy(mac,ifr.ifr_hwaddr.sa_data,6);
    return 0;
}

/*
 * sc_get_pid() -- get PID information from FLASH to @buf.
 *                 length of @buf is @len
 */
int sc_get_pid(char* buf, int len)
{
    FILE *fp;
    char ver[32];
    int major, minor, release;
    sercomm_pid_t *pid;

    if(len < sizeof(sercomm_pid_t)) {
        return -1;
    }

    memset(buf , 0 , len);
    /* 1. Get PID from bootloader block */
    if((fp=fopen(LOADER, "r"))==NULL) {
        return -1;
    }

    fseek(fp, PID_OFFSET, 0);
    fread(buf, PID_LEN, 1, fp);
    fclose(fp);

    /* 2. Update FW Version */
    if((fp=fopen(ANNEX_VERSION, "r")) == NULL) {
        /* Can not open version, just use PID from bootloader */
        return 0;
    }
    fgets(ver, sizeof(ver), fp);
    fclose(fp);
    sscanf(ver, "A%*[^.].%x.%x.%x%*s", &major, &minor, &release);

    pid = (sercomm_pid_t *)buf;
    pid->fw_ver[0]=((major & 0x0F) << 4) + (minor &0x0F);
    pid->fw_ver[1]=release;
    return 0;
}

/*
 * The value in this table should be the nvram value
 */
struct region_related_s region_relateds[] = {
  /* region     wiz_language    wifi_region       fw_time_zone */
    {0x0001,    "English",      "United States",    "-8",    "0","8","35","pppoe","1492", "NA"},      /* NA */
    {0x0002,    "English",      "Europe",           "GMTb",  "1","0","38","pppoa","1458", "WW"},    /* WW */
    {0x0003,    "German",       "Europe",           "+1a",   "0","1","32","pppoe","1492", "GR"},     /* GR */
    {0x0004,    "Chinese",      "Asia",             "+8a",   "1","0","38","pppoa","1458", "PR"},     /* PR */
    {0x0005,    "Russian",      "Europe",           "+3a",   "1","0","38","pppoa","1458", "RU"},     /* RU */
    {0x0006,    "English",      "South America",    "-3b",   "1","0","38","pppoa","1458", "BZ"},     /* BZ */
    {0x0007,    "English",      "Europe",           "+5:30b","1","0","38","pppoa","1458", "IN"},  /* IN */
    {0x0008,    "Korean",       "Korea",            "+9a",   "1","0","38","pppoa","1458", "KO"},     /* KO */
    {0x0009,    "Japanese",     "Japan",            "+9a",   "1","0","38","pppoa","1458", "JP"},     /* JP */
    {0x000a,    "English",     "Australia",         "+10a",   "1","0","38","pppoa","1458","AU"},     /* AU */
    {0x000c,    "English",      "United States",    "-8",    "0","8","35","pppoe","1492", "US"},      /* US */
    {0,         NULL,           NULL,               NULL,   NULL,   NULL,   NULL,   NULL,   NULL,NULL}
};
/*
 * Get DUT Region related struct point
 * Return NULL if error
 */
struct region_related_s *sc_get_region(void) {
    char buf[3];    /* Region is 2 bytes, but flash_get_str() needs another byte at the tail of buf */
    unsigned short region;
    int i;

    flash_get_str(buf, sizeof(buf), REGION_OFFSET, REGION_LEN);
    memcpy(&region, buf, sizeof(region));
    region = ntohs(region);

    /* Check if region is valid or not */
    if(region == 0) {
        /* Region invalid */
        return NULL;
    }

    for(i=0; region_relateds[i].region; i++) {
        if(region == region_relateds[i].region) {
            /* find it */
            return &region_relateds[i];
        }
    }
    return NULL;
}

/* get domain from flash to buf
 * return 0 if success; return -1 if fail
 */
char* sc_get_domain(char* buf, int len)
{
    return flash_get_str(buf, len, DOMAIN_OFFSET, DOMAIN_LEN);
}

/* get Language ID from flash to buf
 * return 0 if success; return -1 if fail
 */
char* sc_get_country(char* buf, int len)
{
    return flash_get_str(buf, len, LANG_ID_OFFSET, LANG_ID_LEN);
}

/* get PCBA SN from flash to buf
 * return 0 if success; return -1 if fail
 */
char* sc_get_pcba_sn(char* buf, int len)
{
#ifndef BOOT_NAND_FLASH
    return flash_get_str(buf, len, PCBA_SN_OFFSET, PCBA_SN_LEN);
#else
    /*PCBA SN has seperate MTD as MTCode asked.*/
#define BLOCKSIZE (16*1024)	// block size
    int fd;
    loff_t offset = 0;

    if(len < PCBA_SN_LEN + 1) {
        return NULL;
    }

    fd = open(PCBASN_PATH, O_RDONLY);
    while(ioctl(fd, MEMGETBADBLOCK, &offset) > 0)
    {
        offset += BLOCKSIZE;
        if(offset >= PCBASN_MTD_SIZE)
        {
            SC_XCFPRINTF("%s: get pcba sn failed: all blocks bad\n",__FUNCTION__);
            close(fd);
            return "";
        }
        lseek(fd, offset, 0);
    }
    
    read(fd, buf, PCBA_SN_LEN);
    close(fd);
    buf[PCBA_SN_LEN] = '\0';
    return buf;
#endif
}

char* sc_get_def_ssid( char* buf, int len)
{
    flash_get_str(buf, len, SSID_OFFSET, SSID_LEN);
    {
        char *ff = strchr(buf,0xff);
        if(ff)
            *ff = '\0';
    }
    if(!isascii(buf[0]) || strlen(buf) == 0) {
        /*
         * without assign will return 0xFFFFFFFFF,
         * but this is invalid. Copy default value for safe
         */
        strcpy(buf, "NETGEAR");
    }
    return buf;
}

char* sc_get_def_wpakey( char* buf, int len)
{
    flash_get_str(buf, len, WPAKEY_OFFSET, WPAKEY_LEN);
    {
        char *ff = strchr(buf,0xff);
        if(ff)
            *ff = '\0';
    }
    if(!isascii(buf[0]) || strlen(buf) == 0) {
        /*
         * without assign will return 0xFFFFFFFFF,
         * but this is invalid. Copy default value for safe
         */
        strcpy(buf, "12345678");
    }
    return buf;
}
/* assign MAC to device
 * return 1 if success; return -1 if fail
 */
int sc_set_mac(char *buf)
{
    return flash_set_str(buf, MAC_OFFSET, MAC_LEN);
}

/* assign CSN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_csn(char *buf)
{
    return flash_set_str(buf, CSN_OFFSET, CSN_LEN);
}

/* assign PIN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_pin(char *buf)
{
    return flash_set_str(buf, WPSPIN_OFFSET, WPSPIN_LEN);
}

/* assign domain(region) to device
 * return 1 if success; return -1 if fail
 */
int sc_set_domain(char *buf)
{
    return flash_set_str(buf, DOMAIN_OFFSET, DOMAIN_LEN);
}

/* assign language ID to device
 * return 1 if success; return -1 if fail
 */
int sc_set_country(char *buf)
{
    return flash_set_str(buf, LANG_ID_OFFSET, LANG_ID_LEN);
}

/* assign PCBA SN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_pcba_sn(char *buf)
{
#ifndef BOOT_NAND_FLASH
    return flash_set_str(buf, PCBA_SN_OFFSET, PCBA_SN_LEN);
#else

    SC_XCFPRINTF("%s: Not support pcba sn assign\n",__FUNCTION__);
    return 0;

#endif
}


/* assign SSID to device
 * return 1 if success; return -1 if fail
 */
int sc_set_ssid(char *buf)
{
    return flash_set_str(buf, SSID_OFFSET, SSID_LEN);
}


/* assign WPAKEY to device
 * return 1 if success; return -1 if fail
 */
int sc_set_wpakey(char *buf)
{
    return flash_set_str(buf, WPAKEY_OFFSET, WPAKEY_LEN);
}

/* assign REGION to device
 * return 1 if success; return -1 if fail
 */
int sc_set_region(char *buf)
{
    return flash_set_str(buf, REGION_OFFSET, REGION_LEN);
}	

static int flash_set_str_force(char* buf, int offset, int len)
{
    fseek(loader_fp, offset, 0);
    if (fwrite(buf, len, 1, loader_fp)) {
        return 0;
    }
    return -1;	
}


int region_is_exist()
{
    struct region_related_s *region_p = sc_get_region();

    if(region_p)
        return 1;

    return 0;
}


int region_is_US()
{
        struct region_related_s *region_p = sc_get_region();
        if (region_p && region_p->region == 0x000c)
                return 1;
        else
                return 0;
}



int region_is_NA()
{
	struct region_related_s *region_p = sc_get_region();
	if (region_p && region_p->region == 1)
		return 1;
	else
		return 0;
}
int sc_set_remotescfgmgrmagic(unsigned char *buf)
{
	SC_CFPRINTF("buf:%02x, %02x, %02x, %02x\n", buf[0], buf[1], buf[2], buf[3]);
	return flash_set_str((char *)buf, REMOTESCFGMGRMAGIC_OFFSET, REMOTESCFGMGRMAGIC_LEN);
}	

int sc_clear_remotescfgmgmagic()	
{
	unsigned char remotescfgmgrmagic_off[REMOTESCFGMGRMAGIC_LEN] = {0xff, 0xff, 0xff, 0xff};
	return flash_set_str_force((char *)remotescfgmgrmagic_off, REMOTESCFGMGRMAGIC_OFFSET, REMOTESCFGMGRMAGIC_LEN);
}

int sc_check_remotescfgmgrmagic()
{
	int flag = 0;
	unsigned char buf[5];
	unsigned char remotescfgmgrmagic_on[REMOTESCFGMGRMAGIC_LEN] = {0x31, 0x3e, 0xc5, 0x8d};
	if (flash_get_str((char *)buf, sizeof(buf), REMOTESCFGMGRMAGIC_OFFSET, REMOTESCFGMGRMAGIC_LEN))
	{
		SC_CFPRINTF("buf:%02x, %02x, %02x, %02x\n", buf[0], buf[1], buf[2], buf[3]);
		SC_CFPRINTF("magic:%02x, %02x, %02x, %02x\n", remotescfgmgrmagic_on[0], remotescfgmgrmagic_on[1], remotescfgmgrmagic_on[2], remotescfgmgrmagic_on[3]);
		if (memcmp(buf, remotescfgmgrmagic_on, REMOTESCFGMGRMAGIC_LEN)==0)	
		{
			flag = 1;
		}
	}
	return flag;
}

