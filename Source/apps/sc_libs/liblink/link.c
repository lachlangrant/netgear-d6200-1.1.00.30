#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>
#include <ctype.h>
#include "sc_if_stats.h"
#include "sc_namespace.h"
typedef u_int64_t u64;
typedef u_int32_t uint32;
typedef u_int16_t uint16;
typedef u_int8_t uchar;
#include "link.h"
#include "if_name.h"
#include "adsl.h"
#include "nvram.h"
#include "nvram_var.h"
#ifdef _3G_FEATURE_
#include "3g_utils.h"
#endif

//#define _SC_DEBUG_ENABLE_
#include "sc_debug.h"

#define RTL819X_IOCTL_READ_PORT_STATUS          (SIOCDEVPRIVATE + 0x01)
#define SIOCGLINKSTATE          (SIOCDEVPRIVATE + 1)
#define SIOCMIBINFO          (SIOCDEVPRIVATE + 3)


#ifdef RTL_BSP
struct lan_port_status {
    unsigned char link;
    unsigned char speed;
    unsigned char duplex;
    unsigned char nway;
};
#else

#define SPEED_10MBIT        10000000
#define SPEED_100MBIT       100000000
#define SPEED_1000MBIT      1000000000

typedef struct lan_port_status
{
    unsigned long link;//mib ulIfLastChange actually;
    unsigned long speed;
    unsigned long duplex;
} IOCTL_MIB_INFO, *PIOCTL_MIB_INFO;

#endif
int get_lanport_status(int portnum,struct lan_port_status *port_status)
{
    struct ifreq ifr;
     int sockfd;
#ifdef RTL_BSP
     char *name = LAN_PHY_IFNAME;
#else
    char name[10] = "eth0";
#endif
     struct lan_port_status status;
#ifdef RTL_BSP
     unsigned int *args;
#endif

     if(portnum > 5)
        return -1;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
            printf("fatal error socket\n");
            return -3;
       }
#ifdef RTL_BSP
    args = (unsigned int *)&status;
    ((unsigned int *)(&ifr.ifr_data))[0] =(unsigned int)&status;
    *args = portnum;
#else
    sprintf(name, "eth%d",portnum);
    ((unsigned int *)(&ifr.ifr_data))[0] =(unsigned int)&status;
#endif

    strcpy((char*)&ifr.ifr_name, name);

#ifdef RTL_BSP
    if (ioctl(sockfd, RTL819X_IOCTL_READ_PORT_STATUS, &ifr)<0)
#else
    if (ioctl(sockfd, SIOCMIBINFO, &ifr)<0)
#endif
    {
            printf("device ioctl:");
            close(sockfd);
             return -1;
     }
     close(sockfd);
     memcpy((char *)port_status,(char *)&status,sizeof(struct lan_port_status));
    return 0;

}
/*******************************************************************************
 * NAME: get_ports_link_status
 *
 * DESCRIPTION:
 *       get ALL ports status
 *
 * INPUTS:
 *
 * OUTPUTS:
 *       status --- All interfaces status including link/up_rate/down_rate...
 *
 * COMMENTS:
 *
 *******************************************************************************/
struct port_link_status_s *get_ports_link_status(void) {
    static struct port_link_status_s status[MAX_PHY_PORTS];
    int i;
    int ret;
    /* Set it to init value */
    for(i=0; i<MAX_PHY_PORTS; i++) {
        status[i].link = LINK_UP;
        status[i].duplex = DUPLEX_HALF;
        status[i].up_rate = PHY_RATE_10M;
        status[i].down_rate = PHY_RATE_10M;
    }


    /* Get Link speed & status*/
    for(i=0; i<MAX_PHY_PORTS; i++) {
        struct lan_port_status port_status;
        int rate = PHY_RATE_100M;
        if ((ret = get_lanport_status(i, &port_status)) < 0)
        {
            SC_CFPRINTF("Get Link speed fail code: %d\n", ret);
            SC_CFPRINTF("Get Link duplex fail code: %d\n", ret);
            goto back;
        }
        status[ETH_PORTMAP(i)].link = port_status.link ? LINK_UP : LINK_DOWN;
 #ifdef RTL_BSP
        switch(port_status.speed)
        {
            case 1:
                rate = PHY_RATE_100M;
                break;
            case 2:
                rate = PHY_RATE_1000M;
                break;
            case 0:
                rate = PHY_RATE_10M;
                break;
        }
#else
        rate = port_status.speed;
#endif
        status[ETH_PORTMAP(i)].up_rate = status[ETH_PORTMAP(i)].down_rate = rate;
        /* Get Link duplex */
        status[ETH_PORTMAP(i)].duplex = port_status.duplex ? DUPLEX_FULL : DUPLEX_HALF;
    }

back:
#ifdef _SC_DEBUG_ENABLE_
    SC_XCFPRINTF("\n");
    SC_XCFPRINTF("PORT#(L/WAN): %-6s%-6s%-8s%-10s%-10s%-10s%-10s%-10s\n",
        "LINK", "SPEED", "DUPLEX", "TXPKTS", "TXBYTES", "RXPKTS", "RXBYTES", "Collisions");
    for(i=0; i<MAX_PHY_PORTS; i++) {
        SC_XCFPRINTF("PORT%d (%s%c): %-6s%-6d%-8s%-10u%-10u%-10u%-10u%-10u\n",
            i,
            i==WAN_PORT_INDEX?"WAN":"LAN",
            i==WAN_PORT_INDEX?' ':'1'+i,
            status[i].link == LINK_UP ? "UP": "DOWN",
            status[i].up_rate/(1000*1000) ,
            status[i].duplex == DUPLEX_FULL ? "FULL": "HALF",
            status[i].txpkts, status[i].txbytes, status[i].rxpkts, status[i].rxbytes, status[i].collisions);
    }
#endif /* _SC_DEBUG_ENABLE_ */
    return status;
}

/*port from cgi*/
int modem_status_cgi(cgi_adsl_info_t *adsl)
{
	char tmp[2048]="";
	char *pt=NULL;
	FILE *fp;

	/* ----- get firmware ------ */
	system("/usr/sbin/adslctl --version 2> /tmp/adsl-ver ");
	fp=fopen("/tmp/adsl-ver","r");
	if(fp==NULL)
		return -1;
	fread(tmp,2048,1,fp);
	fclose(fp);

	pt=strstr(tmp,"ADSL PHY:");

	if(pt==NULL){
		return -1;
	}
	sscanf(pt,"%*[^-]- %s",adsl->firmware);

	/* ------ get status -------*/
	system("/usr/sbin/adslctl info > /tmp/adsl-stats");
	fp=fopen("/tmp/adsl-stats","r");
	if(fp==NULL)
		return -1;

	fread(tmp,2048,1,fp);
	fclose(fp);
	if((pt=strstr(tmp,"Status:"))==NULL){
		return -1;
	}

	sscanf(pt,"%*[^:]: %s ",adsl->status);

	if(strcmp(adsl->status,"Showtime")!=0){//2006.11.22
		return 0;
	}

	system("/usr/sbin/adslctl info --stats > /tmp/adsl-stats");
	fp=fopen("/tmp/adsl-stats","r");
	if(!fp)
		return -1;
	fread(tmp,2048,1,fp);
	fclose(fp);

	/* get detial status */
	/* format like:Max:    Upstream rate = 1116 Kbps, Downstream rate = 25460 Kbps */
	if((pt=strstr(tmp,"Max:"))==NULL){
		return -1;
	}

    /*goto next lin:
    format like:Bearer: 0,   Upstream rate = 1116 Kbps, Downstream rate = 25460 Kbpse*/
	if((pt=strchr(pt+4,':'))==NULL){
		return -1;
	}
	if((pt=strchr(pt,'='))==NULL){
		return -1;
	}
	pt += 2;
	sscanf(pt, "%d", &adsl->up_stream);

	if((pt=strchr(pt,'='))==NULL){
		return -1;
	}
	pt += 2;
	sscanf(pt, "%d", &adsl->down_stream);

	if((pt=strstr(tmp,"SNR (dB):"))==NULL){
		return -1;
	}

	sscanf(pt,"%*[^:]: %s %s",adsl->d_margin,adsl->u_margin);

	if((pt=strstr(tmp,"Attn(dB):"))==NULL){
		return -1;
	}
	sscanf(pt,"%*[^:]: %s %s",adsl->line_d_att,adsl->line_u_att);

	return 0;
}
/*******************************************************************************
 * NAME: get_wan_port_link_status
 *
 * DESCRIPTION:
 *       get wan port link status
 *
 * INPUTS:
 *
 * OUTPUTS:
 *       status --- the wan interface status including link/up_rate/down_rate
 *
 * COMMENTS:
 *
 *******************************************************************************/
struct port_link_status_s *get_wan_port_link_status(void) {
#ifdef DUAL_WAN
    struct port_link_status_s *p = NULL;
    cgi_adsl_info_t adsl ;
    static struct port_link_status_s p_status ;
    
    if(strcmp(nvram_safe_get(WAN_ACTYPE), L2_DSL) == 0)
    {
	    modem_status_cgi(&adsl);
	    p_status.link = (*adsl.status == 'S')?LINK_UP:LINK_DOWN;
	    p_status.up_rate = adsl.up_stream * 1000;
	    p_status.down_rate = adsl.down_stream * 1000;
	    return &p_status;    	
    }
#ifdef _3G_FEATURE_
    else if(strcmp(nvram_safe_get(WAN_ACTYPE), L2_DONGLE_3G) ==0)
    {
        p_status.link = (get_3gmodem_status() == MODEMREADY && get_dongle_status() == DONGLE_CONNECT) ? LINK_UP : LINK_DOWN;
        p_status.up_rate = 0;
        p_status.down_rate = 0;
        return &p_status;
    }
#endif
    else
    {
    	p = get_ports_link_status();
    	return p+WAN_PORT_INDEX;
    }

#else
    cgi_adsl_info_t adsl ;
    static struct port_link_status_s p ;
    
    modem_status_cgi(&adsl);
    p.link = (*adsl.status == 'S')?LINK_UP:LINK_DOWN;
    p.up_rate = adsl.up_stream * 1000;
    p.down_rate = adsl.down_stream * 1000;
    return &p;
#endif
}

/*******************************************************************************
 * NAME: get_eth_wan_port_link_status
 *
 * DESCRIPTION:
 *       get wan port link status
 *
 * INPUTS:
 *
 * OUTPUTS:
 *       status --- the wan interface status including link/up_rate/down_rate
 *
 * COMMENTS:
 *
 *******************************************************************************/
struct port_link_status_s *get_eth_wan_port_link_status(void) {
    struct port_link_status_s *p = get_ports_link_status();
    return p+WAN_PORT_INDEX;
}

/*******************************************************************************
 * NAME: get_dsl_wan_port_link_status
 *
 * DESCRIPTION:
 *       get wan port link status
 *
 * INPUTS:
 *
 * OUTPUTS:
 *       status --- the wan interface status including link/up_rate/down_rate
 *
 * COMMENTS:
 *
 *******************************************************************************/
struct port_link_status_s *get_dsl_wan_port_link_status(void) {
    cgi_adsl_info_t adsl ;
    static struct port_link_status_s p ;
    
    modem_status_cgi(&adsl);
    p.link = (*adsl.status == 'S')?LINK_UP:LINK_DOWN;
    p.up_rate = adsl.up_stream * 1000;
    p.down_rate = adsl.down_stream * 1000;
    return &p;
}

#ifdef _3G_FEATURE_
struct port_link_status_s *get_3g_wan_port_link_status(void)
{
    static struct port_link_status_s p_status;
    memset(&p_status, 0, sizeof(p_status));
    p_status.link = (get_3gmodem_status() == MODEMREADY && get_dongle_status() == DONGLE_CONNECT) ? LINK_UP : LINK_DOWN;
    p_status.up_rate = 0;
    p_status.down_rate = 0;
    return &p_status;
}
#endif

/*******************************************************************************
 * NAME: reset_lan_switch
 *
 * DESCRIPTION:
 *       Pull-low LAN switch for 3 seconds then pull-high
 * INPUTS:
 *
 * OUTPUTS:
 *
 * COMMENTS:
 *          Still dont know why need to set bit 3 of phyaddr 1~4 reg 0x1E.
 *          Got this information from broadcom borg.
 *******************************************************************************/
#define PROC_PHY_POWER   "/proc/phyPower"
#define WAN_PORT_MASK    (0x10)
#define LAN_PORT_MASK    (0x0F)
#define COMMAND_BUF_SIZE (256)

void reset_lan_switch(void) {
#ifdef RTL_BSP
    char cmdbuf[COMMAND_BUF_SIZE];
    snprintf(cmdbuf, sizeof(cmdbuf), "/bin/echo '%d 0' > %s", LAN_PORT_MASK, PROC_PHY_POWER);
    system(cmdbuf);
    sleep(3);

    snprintf(cmdbuf, sizeof(cmdbuf), "/bin/echo '%d 1' > %s", LAN_PORT_MASK, PROC_PHY_POWER);
    system(cmdbuf);

#else
#ifdef BCM_BSP
    int i;
    char cmdbuf[COMMAND_BUF_SIZE];
    for(i=0; i<MAX_PHY_PORTS; i++) {
        if(i == WAN_PORT_INDEX)
            continue;
#ifdef EXTERNAL_SWITCH

/* broadcom mdkshell command. */
#define MDKSHELL(__cmd)  do { \
	char buff[512]; \
    snprintf(buff, sizeof(buff), "/bin/echo \"%s\" >> /var/mdksh", __cmd); \
	system(buff); \
} while(0)
#define MDKSHELL_START() do { \
	system("/bin/rm -f /var/mdksh 2>/dev/null"); \
} while(0)
#define MDKSHELL_END() do { \
	int wait = 0;	\
	MDKSHELL("quit"); \
	system("/bin/echo SHELL > /var/mdkshell"); \
	while (access("/var/mdksh", F_OK) == 0 && ++wait < 10)	\
		sleep(1);	\
} while(0)

	MDKSHELL_START();
	MDKSHELL("1:");
    snprintf(cmdbuf, sizeof(cmdbuf), "phy %d 0 0 0x8000", i);
	MDKSHELL(cmdbuf);
    snprintf(cmdbuf, sizeof(cmdbuf), "seti reg 0x3600 0x3");
	MDKSHELL(cmdbuf);
    snprintf(cmdbuf, sizeof(cmdbuf), "seti reg 0x3610 1");
	MDKSHELL(cmdbuf);
	MDKSHELL_END();
#else
    snprintf(cmdbuf, sizeof(cmdbuf), "/usr/sbin/ethctl eth%d phy-reset", i);
    system(cmdbuf);
#endif
    }
#else
#error "reset_lan_switch not supported!"
#endif
#endif
	return;
}

void
get_dev_fields(char *bp, struct interface *ife) 
{
    sscanf(bp,
	     "%lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
	     &ife->rx_bytes, &ife->rx_packets, &ife->rx_errors,
	     &ife->rx_dropped, &ife->rx_fifo_errors, &ife->rx_frame_errors,
	     &ife->rx_compressed, &ife->rx_multicast, &ife->tx_bytes,
	     &ife->tx_packets, &ife->tx_errors, &ife->tx_dropped,
	     &ife->tx_fifo_errors, &ife->collisions,
	     &ife->tx_carrier_errors, &ife->tx_compressed);
} 

char *
get_name(char *name, char *p) 
{
    while (isspace(*p))
	p++;
    while (*p)
    {
	if (isspace(*p))
	    break;
	if (*p == ':')
	{			/* could be an alias */
	    char *dot = p, *dotname = name;
	    *name++ = *p++;
	    while (isdigit(*p))
		*name++ = *p++;
	    if (*p != ':')
	    {			/* it wasn't, backup */
		p = dot;
		name = dotname;
	    }
	    if (*p == '\0')
		return NULL;
	    p++;
	    break;
	}
	*name++ = *p++;
    }
    *name++ = '\0';
    return p;
}

void
if_readlist_proc(struct interface *ife) 
{
    FILE * fh;
    char buf[256];
    fh = fopen(_PATH_PROCNET_DEV, "r");
    fgets(buf, sizeof(buf), fh);	/* eat line */
    fgets(buf, sizeof(buf), fh);
    while (fgets(buf, sizeof(buf), fh))
    {
	char *s;
	if (strstr(buf, ife->name))
	    
	{
	    s = get_name(ife->name, buf);
	    get_dev_fields(s, ife);
	}
    }
    fclose(fh);
}
