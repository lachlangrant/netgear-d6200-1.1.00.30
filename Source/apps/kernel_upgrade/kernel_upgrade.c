#include <linux/module.h>
#include <linux/ctype.h>
#include <linux/fs.h>
#include <linux/fd.h>
#include <linux/nfs_fs.h>
#include <asm/uaccess.h>
#include <linux/file.h>
#include <linux/proc_fs.h>
#include <linux/syscalls.h>
#include <linux/sched.h>


//#define DEBUG
#ifdef KU_DEBUG
#define debug printk
#else
#define debug(...)
#endif

#define UPGRADE_FILE  "upgrade_file"


static char line[1024];
static char file_name[128];
static char mtd_name[128];
static int offset, kfs_size, lang_size;


#include "unzip.c"

#define Erase_All_MAGIC_CONST           0x77
#define ZIP3G_MAGIC_CONST               0x11

#ifdef KU_DEBUG
void kernel_reboot(void)
{
    printk("<0>""debug, do not Call reboot function\n");
}
#error "KU_DEBUG defined!"
#else

void machine_restart(char *command);
void kernel_reboot(void)
{
    printk("<0>""Call reboot function\n");
    machine_restart(NULL);
}

#endif

int upgrade_open(char *filename)
{
    int                     fd;

    fd = sys_open(filename, O_RDWR, 0);

    if(fd < 0)
    {
        printk("Failed to open file %s\n",filename);
        return -1;
    }

    return fd;
}

int write_fw_to_mtd(int fw_fd, int mtd_fd, unsigned long offset)
{

    /* jump over image header: 512 bytes */
    sys_lseek(fw_fd, 512, 0);
    printk("<0>""write_fw_to_mtd, offset=%lx\n", offset);
    unzip(fw_fd, mtd_fd, offset);
    return 0;
}

void upgrade_fw(char *fw_name, char *mtd_name, int offset)
{
    unsigned char magic;
    int len;
    int fw_fd;
    int flash_fd;
    mm_segment_t            fs = get_fs();

    set_fs(get_ds());
    printk("<0>""Prepare to open fd\n");

    if(((fw_fd = upgrade_open(fw_name)) < 0))
    {
        set_fs(fs);
        kernel_reboot();
        return ;
    }

    /* Read flag of PID */
    sys_lseek(fw_fd, 72, 0);
    len = sys_read(fw_fd,  &magic, sizeof(magic));

    flash_fd = upgrade_open(mtd_name);
    if (flash_fd < 0) {
        set_fs(fs);
        kernel_reboot();
        return ;
    }

    printk("<0>""Open fd sucess\n");
    write_fw_to_mtd(fw_fd, flash_fd, offset);
    printk("<0>""Upgrade fw sucess, prepare close fd\n");
    sys_close(flash_fd);
    sys_close(fw_fd);

    set_fs(fs);
    if (magic != ZIP3G_MAGIC_CONST)
        kernel_reboot();
    return;
}

/* sizeof task_struct.comm is 16 */
#define NOTIFY_PROCESS  "upgrade_driver"
static int proc_write_upgrade_file(struct file *file, const char __user * buffer,
        unsigned long count, void *data)
{
    struct task_struct *p;

    copy_from_user(line, buffer, count);
    if(sscanf(line, "%s %s %d %d %d\n", file_name, mtd_name, &offset , &kfs_size, &lang_size)!=5) {
        return count;
    }
    printk("<0>Upgrading %s to %s offset %d\n", file_name, mtd_name, offset);

    upgrade_fw(file_name, mtd_name, offset);

    /* notify user process */
    read_lock(&tasklist_lock);
    for_each_process(p){
        if (strstr(p->comm, NOTIFY_PROCESS)) {
            send_sig(SIGUSR1, p, 0);
        }
    }
    read_unlock(&tasklist_lock);

    return count;
}

static int __init upgrade_init(void)
{
    struct proc_dir_entry *block_file;

    block_file = create_proc_entry(UPGRADE_FILE, 0666, NULL);
//    block_file->owner = THIS_MODULE;
    block_file->read_proc = NULL;
    block_file->write_proc = proc_write_upgrade_file;
    printk("<0>Upgrading module insert successfully.\n");

    return 0;
}

static void __exit upgrade_exit(void)
{
    remove_proc_entry(UPGRADE_FILE,  NULL);
}

module_init(upgrade_init);
module_exit(upgrade_exit);

MODULE_LICENSE("GPL");


