/* This is a generated file, don't edit */

#define NUM_APPLETS 66

const char applet_names[] ALIGN1 = ""
"ash" "\0"
"awk" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"cat" "\0"
"chmod" "\0"
"cp" "\0"
"cut" "\0"
"date" "\0"
"dd" "\0"
"df" "\0"
"dmesg" "\0"
"echo" "\0"
"expr" "\0"
"false" "\0"
"fdisk" "\0"
"find" "\0"
"flash_eraseall" "\0"
"free" "\0"
"ftpget" "\0"
"grep" "\0"
"gzip" "\0"
"halt" "\0"
"head" "\0"
"ifconfig" "\0"
"init" "\0"
"insmod" "\0"
"kill" "\0"
"killall" "\0"
"ln" "\0"
"login" "\0"
"ls" "\0"
"lsmod" "\0"
"md5sum" "\0"
"mkdir" "\0"
"mknod" "\0"
"more" "\0"
"mount" "\0"
"mv" "\0"
"passwd" "\0"
"pidof" "\0"
"ping" "\0"
"ping6" "\0"
"poweroff" "\0"
"ps" "\0"
"realpath" "\0"
"reboot" "\0"
"renice" "\0"
"rm" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"sendarp" "\0"
"sh" "\0"
"sleep" "\0"
"tail" "\0"
"top" "\0"
"touch" "\0"
"traceroute" "\0"
"true" "\0"
"umount" "\0"
"unzip" "\0"
"vconfig" "\0"
"vi" "\0"
"wc" "\0"
"wget" "\0"
;

int (*const applet_main[])(int argc, char **argv) = {
ash_main,
awk_main,
bunzip2_main,
bunzip2_main,
cat_main,
chmod_main,
cp_main,
cut_main,
date_main,
dd_main,
df_main,
dmesg_main,
echo_main,
expr_main,
false_main,
fdisk_main,
find_main,
flash_eraseall_main,
free_main,
ftpgetput_main,
grep_main,
gzip_main,
halt_main,
head_main,
ifconfig_main,
init_main,
insmod_main,
kill_main,
kill_main,
ln_main,
login_main,
ls_main,
lsmod_main,
md5_sha1_sum_main,
mkdir_main,
mknod_main,
more_main,
mount_main,
mv_main,
passwd_main,
pidof_main,
ping_main,
ping6_main,
halt_main,
ps_main,
realpath_main,
halt_main,
renice_main,
rm_main,
rmmod_main,
route_main,
sed_main,
sendarp_main,
ash_main,
sleep_main,
tail_main,
top_main,
touch_main,
traceroute_main,
true_main,
umount_main,
unzip_main,
vconfig_main,
vi_main,
wc_main,
wget_main,
};
const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x2004,
0x0008,
0x0010,
0x3016,
0x201a,
0x2020,
0x2023,
0x0027,
0x202c,
0x002f,
0x0032,
0x3038,
0x003d,
0x3042,
0x0048,
0x204e,
0x8053,
0x0062,
0x0067,
0x006e,
0x0073,
0x0078,
0x007d,
0x0082,
0x008b,
0x0090,
0x0097,
0x009c,
0x20a4,
0x80a7,
0x20ad,
0x00b0,
0x00b6,
0x30bd,
0x00c3,
0x00c9,
0x00ce,
0x00d4,
0x80d7,
0x00de,
0x40e4,
0x00e9,
0x00ef,
0x00f8,
0x00fb,
0x0104,
0x010b,
0x3112,
0x0115,
0x011b,
0x0121,
0x0125,
0x012d,
0x3130,
0x0136,
0x013b,
0x313f,
0x4145,
0x3150,
0x0155,
0x015c,
0x0162,
0x016a,
0x016d,
0x0170,
};
#define MAX_APPLET_NAME_LEN 14
