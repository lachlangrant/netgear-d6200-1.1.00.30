#define _XOPEN_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <netdb.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pwd.h>
#include <sys/types.h>

#include "rc.h"
#include "nvram.h"
#include "lib_samba.h"

static int SysActGroup (group_info_t *pGroup,user_info_t *user, int action);
static int AddGroup (group_info_t *pGroup,user_info_t *user);
static int GetGroupId(char *name);
static int GetLineFromFile(char **lbuf,FILE *fp);
static int GetLineFromFile(char **lbuf,FILE *fp);
static void DBCS_SpecialChar(char *old, char *new, char *str);
static int AddShare(share_info_t *sh,group_share_t * group_share);
static int change_servicename_workgroupname(char *servicename,char *workgroupname,char *netbios_name);
static int Share_all_partition(group_share_t *group_share);
static int UserAdd(u_info *pUser);
static int AddUser(u_info *pUser);
/*the below function will parse the group,user and share info which have been save in nvram*/
static int read_group_from_nvram(group_info_t **group);
static int read_user_from_nvram(user_info_t **user);
static int read_share_from_nvram(share_info_t **share);
static int read_group_share_from_nvram(group_share_t **group_share);
static void FreeGroupListt(group_info_t *pList);
static void FreeUserListt(user_info_t *pList);
static void FreeGroupShareListt(group_share_t *pList);
static void FreeShareListt(share_info_t *pList);
static int create_smb_config(void);

extern char *crypt(const char *key, const char *salt);
extern int mkstemp(char *template);

/*
 * Add or Del the group from system.
 *    int actions:	0--Add the group(name)
 *			1--Del the group(name)
 */
/* The format of group file is:"groupname:passwd:group_ID:username1,username2" */
static int SysActGroup (group_info_t *pGroup,user_info_t *user, int action)
{
	//FILE *fh;
	FILE *ft;
	//char tmpfile[64] = "/tmp/tmpfile.XXXXXX";
	//char *buf, tmp_buf[128], *pstr;
	//char *temp;
	//char tempg[1000]={0};
	int i, id = 0;
	//int  ret,fd;
	int nflag=0;
	int gid[GROUP_NUM]={0};
	//group_info_t *ptempg;
	user_info_t *puser;
	user_info_t *p;
	if(!pGroup||(action!=0&&action!=1))
		return INPUTPARM_ERR;
/*
	if(!access("/share/hdd/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/hdd/conf/tmp/tmpfile.XXXXXX");
	else if(!access("/share/flash/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/flash/conf/tmp/tmpfile.XXXXXX");

	fd=mkstemp(tmpfile);*/
	//if(fd==-1)
	//	return CREATE_TMPFILE_ERR;
	//close(fd);
	/*fh = fopen (FSH_SYS_GROUP, "r");
	if (!fh)
		return (FILE_OPEN_ERR);
		*/
	ft = fopen (FSH_SYS_GROUP, "w");
	if (!ft){
		//fclose (fh);
		return (FILE_OPEN_ERR);
	}
	memset (&gid[0], 0, sizeof (gid));
	puser = user;
	fprintf(ft,"default:x:2064:");
	p = puser;
	while(puser){
        	if(p!=puser)
        	{
        		if(strcmp(p->name,puser->name)==0)
        		{
        			p = puser;
        			puser = puser->next;
        			continue;
        		}
        		p = puser;
        	}
        	fprintf(ft,"%s",puser->name);
        	if(puser->next)
        		fprintf(ft,",");
        	puser = puser->next;
	}
	fprintf(ft,"\n");
	i = 0;
	while(pGroup){
	   
		id = i + GID_Start;
		i = i + 1;
		if (i >= GROUP_NUM - 2){
			fclose (ft);
			//remove (tmpfile);
			return (FSH_GRP_TOOMANYGRP_ERR);//too many groups
		}
		fprintf (ft, "%s:x:%d:", pGroup->name, id);//write the new group in temporary file
		puser = user;
		nflag = 0;
		while(puser){
			if(strcmp(puser->group,pGroup->name)==0)
			{
				if(nflag == 0)
				{
					fprintf(ft,"%s",puser->name);
					nflag = 1;
				}
				else
					fprintf(ft,",%s",puser->name);
			}
			if( puser->next == NULL )
				fprintf(ft,"\n");
			puser = puser->next;
		}
		pGroup=pGroup->next;
	}
//ret1:
	fflush(ft);
	fclose(ft);
	//sprintf (tmp_buf, "/bin/cp -f %s %s 2>/dev/null", tmpfile, FSH_SYS_GROUP);
	//system (tmp_buf);
	//ret = system (tmp_buf);
	//remove (tmpfile);
	//if (ret)
	//	return (action?FSH_DEL_GROUP_ERR:FSH_ADD_GROUP_ERR);
	return FSH_OK;
}

/* According to groupname get group_id */
static int GetGroupId(char *name)
{
	FILE *fh;
	char *buf, *p1,*p2;
	int i=0,flag=0;
	
	if(!name||!name[0])
		return(FSH_GET_GROUP_ID_ERR);
	
	fh = fopen (FSH_SYS_GROUP, "r");
	if (!fh)
		return (FSH_GET_GROUP_ID_ERR);
	
	while (!GetLineFromFile (&buf, fh)){
		if (buf[0] == '\n' || buf[0] == '\r')
			continue;
		p1 = strchr (buf, ':');
		if(!p1)
			goto err;
		*p1=0;
		p1++;
		if(strcmp (buf, name)){
			free(buf);
			continue;
		}
		flag = 1;
		p2 = strchr (p1, ':');
		if(!p2)
			goto err;
		p2++;
		p1 = strchr (p2, ':');
		if (!p1)
			goto err;
		*p1=0;
		i = atoi(p2);
		free(buf);
		break;
	}
	
	fclose(fh);
	if(flag)
		return i;
	else
		return FSH_GET_GROUP_ID_ERR;
err:
	free(buf);
	fclose(fh);
	return FSH_GET_GROUP_ID_ERR;
}


static int AddGroup (group_info_t *pGroup,user_info_t *user)
{
	int  ret;
	if(!pGroup)
		return INPUTPARM_ERR;
	ret = SysActGroup (pGroup, user,0);
	if (ret)
		goto err;

	return FSH_OK;
err:
	return ret;
}


static int GetLineFromFile(char **lbuf,FILE *fp)
{
	int	c,sz=128;
	char *p,*buf;

	buf=(char *) malloc(sz);
	if(!buf){
		*lbuf=NULL;
		return(READ_FILE_ERR);
	}
	p=buf;
	while ((c=fgetc(fp)) != '\n' && c!=EOF) {
		if (p-buf ==sz-2) {
			buf=(char *) realloc(buf, sz*2);
			if(!buf){
				*lbuf=NULL;
				return 1;
			}
			p=buf+sz-2;
			sz*=2;
		}
		*p++=c;
	}
	if(c=='\n')
	     *p++=c;
	if (c==EOF &&p==buf) {
		free(buf);
		*lbuf=NULL;
		return(READ_FILE_ERR);
	}
	*p++='\0';
	*lbuf=buf;
	return(FSH_OK);
}

#if 0
//------------------------------------------------------
//purpose: check if the pItem exist in file f
//input: pItem - search name
//       pFile - search file (with format pItem:...)
//return: 1 - name exist
//        0 - name not found
// NOTE: both passwd and group can use this function
//------------------------------------------------------
static int ItemExist (char *pItem, char *pFile)
{
	char *buf, *pStr=NULL;
	FILE *fh;
	int f_exist = 0;

	if (!pItem||!pItem[0]||!pFile||!pFile[0])
		return 0;

	fh = fopen (pFile, "rt");
	if (!fh)
		return 0;
	while (!GetLineFromFile (&buf, fh)){
		pStr = strtok (buf, ":");
		if (pStr&&!strcasecmp(pStr, pItem))	{
			f_exist = 1;
			free (buf);
			break;
		}
		free (buf);
	}
	fclose (fh);
	return f_exist;
}
#endif

#if 0
//-------------------------------------------------------------
// read group name, password and id (data before group members)
// input: buf - buffer to save read data
//        len - buffer size
//        fh  - group file handle
// output: *eol - 1=end of line
//                2=end of file
//                0=else
// return: how many bytes are readed in buf
//-------------------------------------------------------------
static int ReadGroup(char *buf,int len,FILE *fh,int *eol)
{
	int c,i=0,j=0;

	*eol=0;
	c=fgetc(fh);
	while(c!=EOF && c!='\n' && c!='\r' && i<(len-1)){
	buf[i++]=c;
	if(c==':'){
		j++;
		if(j>=3){
			buf[i]=0;
			return(i);
		}
	}
	c=fgetc(fh);
	}
	buf[i]=0;
	if(c==EOF)
		*eol=2;
	else
		*eol=1;
	return(i);
}
#endif

//--------------------------------------------
// purpose: add '\\' before special characters
// input: old - source string
//        str - special character string
// output: new - dest string
//--------------------------------------------
static void DBCS_SpecialChar(char *old, char *new, char *str)
{
	char *p, *p1;
	int len, i;
	
	len = strlen(str);
	p = old;
	p1 = new;
	while (*p) {
		for (i = 0; i < len; i++) {
			if (*p == str[i])
				break;
		}
		if (i < len)
			*p1++ = '\\';
		*p1++ = *p++;
	}
	*p1 = 0;
}

#if 0
//------------------------------------------------------
//purpose: check if the pItem exist in file f
//input: pItem1 - search name
//         pItem2 - search name
//       pFile - search file (with format pItem:...)
//return: 1 - name exist
//        0 - name not found
// NOTE: both passwd and group can use this function
//------------------------------------------------------
static int Item2Exist (char *pItem1, char *pItem2,char *pFile)
{
	char *buf, *pStr=NULL;
	FILE *fh;
	int f_exist = 0;

	if (!pItem1||!pItem1[0]||!pFile||!pFile[0])
		return 0;

	fh = fopen (pFile, "rt");
	if (!fh)
		return 0;
	while (!GetLineFromFile (&buf, fh)){
		if((pStr = strstr (buf, pItem1)))
          		if( (pStr = strstr (buf, pItem2))){
				f_exist = 1;
				free (buf);
				break;
			}
		free (buf);
	}
	fclose (fh);
	return f_exist;
}
#endif

static void FreeGroupListt(group_info_t *pList)
{
	group_info_t *pCurrent;

	pCurrent=pList;
	while(pCurrent){
		pList=pList->next;
		free(pCurrent);
		pCurrent=pList;
	}
}

static void FreeUserListt(user_info_t *pList)
{
	user_info_t *pCurrent;

	pCurrent=pList;
	while(pCurrent){
		pList=pList->next;
		free(pCurrent);
		pCurrent=pList;
	}
}

static void FreeGroupShareListt(group_share_t *pList)
{
	group_share_t *pCurrent;

	pCurrent=pList;
	while(pCurrent){
		pList=pList->next;
		free(pCurrent);
		pCurrent=pList;
	}
}

static void FreeShareListt(share_info_t *pList)
{
	share_info_t *pCurrent;

	pCurrent=pList;
	while(pCurrent){
		pList=pList->next;
		free(pCurrent);
		pCurrent=pList;
	}
}

static int read_group_from_nvram(group_info_t **group)
{
	char temp[STRING_LEN], *p,*q;
	//char groupname[MAXGROUPLEN];
	group_info_t *pNew=NULL,*pTail=NULL;
	int i=0,group_num=0,ccode;
	
	char *storage_group=nvram_safe_get("storage_group");
	
	if(!group){
		ccode=INPUTPARM_ERR;
		goto err;
	}
	*group=NULL;
	
	strcpy(temp,storage_group);
	free(storage_group);
	
	while   (temp[i]==' ') i++;
	q=p=temp+i;
	if(!q){
		ccode=0;
		goto err;
	}
	while(p){//here we will parse the group name
		q=p;
		p=strstr (q, ",");
		if(p){
			*p='\0';
			p++;
  		 }  
  		 pNew=(group_info_t *)malloc(sizeof(group_info_t));
		if(!pNew){
			ccode=ALLOCATE_MEM_ERR;
			goto err;
		}
		strcpy(pNew->name,q);
		pNew->next=NULL;
		if(*group==NULL)
			*group=pTail=pNew;
		else{
			pTail->next=pNew;
			pTail=pNew;
		}
		group_num++;
	}
	return  group_num;
err:
	FreeGroupListt(*group);
	return ccode;
}


static int read_user_from_nvram(user_info_t **user)
{
	char temp[STRING_LEN];
	char  *p,*q,*t;
	char username[MAXUSERLEN],passwd[MAXPASSLEN],groupname[MAXGROUPLEN];
	char user_info[MAXUSERLEN+MAXPASSLEN+MAXGROUPLEN+10];
	user_info_t *pNew=NULL,*pTail=NULL;
	int user_num=0,ccode;
	char *storage_user=nvram_safe_get("storage_user");
	
	if(!user){
		ccode=INPUTPARM_ERR;
		goto err;
	}
	*user=NULL;
	
	strcpy(temp,storage_user);
	free(storage_user);
	if (!temp){
		ccode=0;
		goto err;
	}
	
	p=q=temp;
	while (*p){
		q=strstr(p,"{");
		if(q)
			q++;
		else
		{
			ccode=READ_ERROR;	
			goto err;
		}
		p=strstr(q,"}");
		if(p){
			*p='\0';
			p++;//point to the next user_info struct
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(user_info,q);//now we get one user info in array user_info and we will parse it
		t=q=user_info;
		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(username,t);//here we get the username
		t=q;
		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(passwd,t);//here we get the passwd
		if(q){
			strcpy(groupname,q);
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		pNew=(user_info_t *)malloc(sizeof(user_info_t));//malloc a user_info struct to storage the user info
		if(!pNew){
			ccode=ALLOCATE_MEM_ERR;
			goto err;
		}
		strcpy(pNew->name,username);
		strcpy(pNew->passwd,passwd);
		strcpy(pNew->group,groupname);
		pNew->next=NULL;
		if(*user==NULL)
			*user=pTail=pNew;
		else{
			pTail->next=pNew;
			pTail=pNew;
		}
		user_num++;
	}
	return user_num;
err:
	FreeUserListt(*user);
	return ccode;
}


static int read_group_share_from_nvram(group_share_t ** group_share)
{
	char temp[STRING_LEN], *p,*q,*t;
	char sharename[MAXSHARELEN],groupname[MAXGROUPLEN];
	int  permission;
	char group_share_temp[MAXSHARELEN+MAXPASSLEN+10];
	group_share_t *pNew=NULL,*pTail=NULL;
	int group_share_num=0,ccode;
	char *storage_gshare=nvram_safe_get("storage_gshare");
	
	if(!group_share){
		ccode=INPUTPARM_ERR;
		goto err;
	}
	*group_share=NULL;
	
	strcpy(temp,storage_gshare);
	free(storage_gshare);
	if (!temp){
		ccode=0;
		goto err;
	}
	
	p=q=temp;
	while (*p){
		q=strstr(p,"{");
		if(q)
			q++;
		else
		{
			ccode=READ_ERROR;	
			goto err;
		}
		p=strstr(q,"}");
		if(p){
			*p='\0';
			p++;//point to the next user_info struct
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(group_share_temp,q);//now we get one group_share info in array user_info and we will parse it
		t=q=group_share_temp;
		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(sharename,t);//here we get the sharename
		t=q;
		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(groupname,t);//here we get the groupname
		if(q){
			permission=atoi(q);
			if(permission==0||permission==1){ }//the value of permission should been 0 or 1
			else{
				ccode=READ_ERROR;
				goto err;
			}
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		pNew=(group_share_t *)malloc(sizeof(group_share_t));//malloc a group_share struct to storage the user info
		if(!pNew){
			ccode=ALLOCATE_MEM_ERR;
			goto err;
		}
		strcpy(pNew->sharename,sharename);
		strcpy(pNew->groupname,groupname);
		pNew->permission=permission;
		pNew->next=NULL;
		if(*group_share==NULL)
			*group_share=pTail=pNew;
		else{
			pTail->next=pNew;
			pTail=pNew;
		}
		group_share_num++;
	}
	return group_share_num;
err:
	FreeGroupShareListt(*group_share);
	return ccode;
}

static int read_share_from_nvram(share_info_t **share)
{
	char temp[STRING_LEN], *p,*q,*t;
	char sharename[MAXSHARELEN],partition[MAXPARTITIONLEN];
	int  total_space;
	char share_info[MAXSHARELEN+MAXPARTITIONLEN+10];
	share_info_t *pNew=NULL,*pTail=NULL;
	int share_num=0,ccode;
	char *storage_share=nvram_safe_get("storage_share");
	
	if(!share){
		ccode=INPUTPARM_ERR;
		goto err;
	}
	*share=NULL;
	
	strcpy(temp,storage_share);
	free(storage_share);
	if (!temp){
		ccode=0;
		goto err;
	}
	
	p=q=temp;
	while (*p)
	{
		q = strstr(p,"{");
		if(q)
			q++;
		else
		{
			ccode=READ_ERROR;	
			goto err;
		}

		p=strstr(q,"}");
		if(p){
			*p='\0';
			p++;//point to the next share_info struct
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		
		strcpy(share_info,q);//now we get one share info in array user_info and we will parse it
		t=q=share_info;

		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		strcpy(sharename,t);//here we get the sharename
		t=q;

		q=strstr(q,",");
		if(q){
			*q='\0';
			q++;	
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		
		strcpy(partition,t);//here we get the partition
		if(q){
			total_space=atoi(q);
		}
		else
		{
			ccode=READ_ERROR;
			goto err;
		}
		pNew=(share_info_t *)malloc(sizeof(share_info_t));//malloc a user_info struct to storage the user info
		if(!pNew){
			ccode=ALLOCATE_MEM_ERR;
			goto err;
		}
		strcpy(pNew->sharename,sharename);
		strcpy(pNew->partition,partition);
		pNew->total_space=total_space;
		pNew->next=NULL;
		if(*share==NULL)
			*share=pTail=pNew;
		else{
			pTail->next=pNew;
			pTail=pNew;
		}
		share_num++;
	}
	return share_num;
  err:
	FreeShareListt(*share);
	return ccode;
}

#if 0
/*judge if this share session already in /etc/samba/smb.conf file*/
/*return value 1:this session already in config file*/
/*return value 0:this session have't in config file we can add it to config file*/
static int ShareExist (char *name)
{
	char *buf,*share_file=NULL, tmp[MAXSHARELEN + 64];
	FILE *fh;
	int j,len,find=0;

	if(!name||!name[0])
		return 0;
	share_file=FSH_SMB_CONF;		
	fh = fopen (share_file, "r");
	if (!fh){
		return FILE_OPEN_ERR;
	}
	while (!GetLineFromFile (&buf, fh)){
		if (buf[0] == '\n' || buf[0] == '\r'){
					continue;
		}
		len = strlen (buf);
		j = strchr (buf, ']') - buf;
		if (buf[0] == '[' && buf[j] == ']'){
			strcpy (tmp, buf);
			buf[j] = '\0';
			if (strcmp (buf + 1, name) == 0) {
				find = 1;
				break ;
			}
		}
		free (buf);
	}
	fclose (fh);
	
	if(find==1)
		return 1;
	else
		return 0;
}
#endif

/*add share info in file /etc/samba/smb.conf*/
static int AddShare(share_info_t *sh,group_share_t * group_share)
{
	char temp[1024] ;
	FILE *fp;
	int nflag = 0;
	group_share_t *p=NULL;
	
	if(!sh)
		return (INPUTPARM_ERR);
	/*if(ShareExist(sh->sharename)==1)
		return FSH_SHARE_DUP_ERR;*/
/*if the share session has already in config file then return FSH_SHARE_DUP_ERR else add the share session to config file */
	fp = fopen (FSH_SMB_CONF, "a+");
	p = group_share;
  
	while(sh)
	{
		if (!fp)
			return FILE_OPEN_ERR;
		fprintf(fp,"\n");
		sprintf(temp,"\n[%s]",sh->sharename);
		fprintf(fp,temp);
		sprintf(temp,"\n\tpath = /mnt/%s",sh->partition);
		fprintf(fp,temp);
		fprintf(fp,"\n\tvalid users = \"default\"");
		
		while(group_share)
		{
			if(strcmp(sh->sharename,group_share->sharename)==0)
			{
				fprintf(fp,",\"%s\"",group_share->groupname);    
			}
			group_share=group_share->next;
		}
		fprintf(fp,"\n\tread only = yes");
		fprintf(fp,"\n\twrite list = ");
		nflag = 0;
		group_share = p;
		while(group_share)
		{
			if(strcmp(group_share->sharename,sh->sharename) == 0)
			{   
				if(group_share->permission!=1)
				{
					group_share = group_share->next;
					continue;
				}
				if( nflag == 0 )
				{
					fprintf(fp,"\"%s\"",group_share->groupname);
					nflag = 1;
				}
				else
					fprintf(fp,",\"%s\"",group_share->groupname);
			 }
			 group_share = group_share->next;
		}
		p = p->next;
		group_share = p;
		fprintf(fp,"\n");
		sh = sh->next;
	}
	
	fclose(fp);
	return F_OK;
}

#if 0
static int AddGroupToShare(group_share_t *group_share)
{
	char *buf,*buf1,tmp[1024],tmp2[512];
	char tmpfile[64] = "/tmp/tmpfile.XXXXXX";
	char *p;
	int fd,len,j,find,ret;
	FILE *fh,*ft;
	int group_add=0,permission_add=0;
	if(!group_share)
		return (INPUTPARM_ERR);
	
	sbup("in function AddGroupToShare\n");
	
	if(!access("/share/hdd/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/hdd/conf/tmp/tmpfile.XXXXXX");
	else if(!access("/share/flash/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/flash/conf/tmp/tmpfile.XXXXXX");
	fd=mkstemp(tmpfile);
	if(fd==-1)
		return CREATE_TMPFILE_ERR;
	close(fd);
	fh = fopen (FSH_SMB_CONF, "rt");
	if (!fh)
		return FILE_OPEN_ERR;
	ft = fopen (tmpfile, "wt");
	if (!ft) {
		fclose (fh);
		return (FILE_OPEN_ERR);
	}
	
	while (!GetLineFromFile (&buf, fh)){
		if (buf[0] == '\n' || buf[0] == '\r'){
			fputs (buf, ft);
			continue;
		}
		len = strlen (buf);
		j = strchr (buf, ']') - buf;
		if (buf[0] == '[' && buf[j] == ']'){
			strcpy (tmp, buf);
			buf[j] = 0;
			if (strcmp (buf + 1, group_share->sharename) == 0) {
				find = 1;
				fputs (tmp, ft);
				while (!GetLineFromFile (&buf1, fh)){		//skip original section and add group to valid user
					if (buf1[0] == '['){
						fputs (buf1, ft);
						break;//jump out thi while
					}
					else{
						if((p=strstr(buf1,"valid"))){
							if((p=strstr(p,"users")))//we should find the command:"valid user" the we add the group to valide user
								if(!(p=strstr(p,group_share->groupname))){
                                                               len = strlen (buf1);
                                                               buf1[len-1]='\0';
                                                               strcpy (tmp, buf1);
									sprintf(tmp2,",\"%s\"\n",group_share->groupname);
                                                               strcat(tmp,tmp2);
									fputs(tmp,ft);
									group_add=1;
								}
							if(group_add!=1)
								fputs(buf1,ft);
						}
						else if((p=strstr(buf1,"write"))){
							if(group_share->permission==1){//add write permission
								if((p=strstr(p,"list"))) {//we should find the command:"write list" the we add the group's write permission
									if(!(p=strstr(p,group_share->groupname))){
										len = strlen (buf1);
										buf1[len-1]='\0';
										strcpy (tmp, buf1);
										p=strstr(buf1,"=");
										if(!*(p+2)){/*if is the first group we shouldn't add comma before it*/	
											sprintf(tmp2,"\"%s\"\n",group_share->groupname);
										}
										else
											sprintf(tmp2,",\"%s\"\n",group_share->groupname);	
										strcat(tmp,tmp2);
										fputs(tmp,ft);
										permission_add=1;
									}
								}
							}
							if(permission_add!=1)
								fputs(buf1,ft);
						}
						else {
							fputs(buf1,ft);
						}
					}
					free (buf1);
				}
			}
			else { // don't need to change this share
				fputs (tmp, ft);
			}
		}
		else { //if(buf[0]=='[' && buf[j]==']')
			fputs (buf, ft);
		}
		free (buf);
	}
	fclose (fh);
	fclose (ft);
	if(buf)
		free (buf);
	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", tmpfile, FSH_SMB_CONF);
	ret = system(tmp);
	if (ret){
		remove (tmpfile);
		return FSH_WRITE_SMB_CONF_ERR;
	}
	sleep(2);
	remove (tmpfile);
	return FSH_OK;
}
#endif

static int change_servicename_workgroupname(char *servicename,char *workgroupname,char *netbios_name)
{
	char *buf,*p,tmp[1024],tmp2[1024];
	char tmpfile[64] = "/tmp/tmpfile.XXXXXX";
	int fd,ret;
	FILE *fh,*ft;

	/* XXX */
	if(!access("/share/hdd/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/hdd/conf/tmp/tmpfile.XXXXXX");
	else if(!access("/share/flash/conf/.dongle",F_OK))
		strcpy(tmpfile,"/share/flash/conf/tmp/tmpfile.XXXXXX");
	
	fd=mkstemp(tmpfile);
	if(fd==-1)
		return CREATE_TMPFILE_ERR;
	close(fd);
	fh = fopen (FSH_SMB_CONF, "rt");
	if (!fh)
		return FILE_OPEN_ERR;
	ft = fopen (tmpfile, "wt");
	if (!ft) {
		fclose (fh);
		return (FILE_OPEN_ERR);
	}

	/* Parse and write /etc/samba/smb.cong file */
       while (!GetLineFromFile (&buf, fh)){
		if (buf[0] == '\n' || buf[0] == '\r'){
			fputs (buf, ft);
			continue;
		}
		if((p=strstr(buf,"workgroup"))){
			if((p=strstr(p,"="))){
				p++;
				*p='\0';
				sprintf(tmp2,"%s\n",workgroupname);
				strcat(buf,tmp2);
			}
			fputs(buf,ft);                   
		}else if((p=strstr(buf,"server"))){
			if((p=strstr(p,"string"))) {
				if((p=strstr(p,"="))){		
					p++;
					*p='\0';
					sprintf(tmp2,"%s\n",servicename);
					strcat(buf,tmp2);
				}
			}
			fputs(buf,ft);   
		}else if((p=strstr(buf,"netbios"))){
			if((p=strstr(p,"name"))) {
				if((p=strstr(p,"="))){
					p++;
					*p='\0';
					sprintf(tmp2,"%s\n",netbios_name);
					strcat(buf,tmp2);
				}
			}
			fputs(buf,ft);                   
		}else {
		   	fputs (buf, ft);
		}
		free (buf);
	}
	fclose (fh);
	fclose (ft);
	if(buf)
		free (buf);
	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", tmpfile, FSH_SMB_CONF);
	ret = system(tmp);
	if (ret){
		remove (tmpfile);
		return FSH_WRITE_SMB_CONF_ERR;
	}
	sleep(2);
	remove (tmpfile);
	return FSH_OK;
}

static int Share_all_partition(group_share_t *group_share)
{
	FILE *fp;
	int nflag =0 ;
	group_share_t *p;
//	if(ShareExist("All_Partitions")==1)
//		return FSH_SHARE_DUP_ERR;
/*if the share session has already in config file then return FSH_SHARE_DUP_ERR else add the share session to config file */
	fp = fopen (FSH_SMB_CONF, "a+");
	if (!fp)
		return FILE_OPEN_ERR;
	fprintf(fp,"\n");
	fprintf(fp,"\n[All_Partitions]");
	fprintf(fp,"\n\tpath = /mnt");
	fprintf(fp,"\n\tvalid users = ");
	p = group_share;
	while(group_share)
	{
		if(strcmp("All_Partitions",group_share->sharename)==0)
		{
			if( nflag == 0 )
			{
				fprintf(fp,"\"%s\"",group_share->groupname);
				nflag = 1;
			}
			else
				fprintf(fp,",\"%s\"",group_share->groupname);   
		}
		group_share=group_share->next;
	}
	fprintf(fp,"\n\tread only = yes");
	fprintf(fp,"\n\twrite list = ");
	nflag = 0;
	group_share = p;
	while(group_share)
	{
        	if(strcmp(group_share->sharename,"All_Partitions") == 0)
		{   
			if(group_share->permission!=1)
			{
				group_share = group_share->next;
				continue;
			}
			if( nflag == 0 )
			{
			 	fprintf(fp,"\"%s\"",group_share->groupname);
			 	nflag = 1;
			}
			else
				fprintf(fp,",\"%s\"",group_share->groupname);
		}
		group_share = group_share->next;
	}
	fprintf(fp,"\n");
	fclose(fp);
	return F_OK;
}


#if 0
//-------------------------------------------------
// purpose: add user to group member in /etc/group
// input: name - group name
//        user - user name name array
//        no - total number of users in user array
//-------------------------------------------------
static int AddUsersToSysGroup (char *pGroup,char **ppUser,int no)
{
	char buf[MAXGROUPLEN+MAXUSERLEN+1],buf1[128];
	char tmp[MAXGROUPLEN+MAXUSERLEN+1];
	FILE *fh,*fh1;
	int  len,i,c,eol=0;
	char TmpFile[64]="/tmp/tmpfile.XXXXXX";
	int  added[USER_NUM],updated=0,users=0,ret=0;
	int fd;

	if(!pGroup||!*pGroup||!no||!ppUser)
		return(INPUTPARM_ERR);

	if(!access("/share/hdd/conf/.dongle",F_OK))
		strcpy(TmpFile,"/share/hdd/conf/tmp/tmpfile.XXXXXX");
	else if(!access("/share/flash/conf/.dongle",F_OK))
		strcpy(TmpFile,"/share/flash/conf/tmp/tmpfile.XXXXXX");
	fd=mkstemp(TmpFile);
	if(fd==-1)
		return CREATE_TMPFILE_ERR;
	close(fd);
	fh=fopen(FSH_SYS_GROUP,"rt");
	if(!fh)
		return(FILE_OPEN_ERR);
	fh1=fopen(TmpFile,"wt");
	if (!fh1) {
		fclose(fh);
		return(FILE_OPEN_ERR);
	}
	sprintf(tmp,"%s:",pGroup);
	len=strlen(tmp);
	while(eol!=2) {
		i=ReadGroup(buf,sizeof(buf),fh,&eol);
		fputs(buf,fh1);
		if(strncmp(buf,tmp,len)==0) { //find the group
			//now put the new group members
			memset(&added,0,sizeof(added));
next_user:
			c=fgetc(fh);
			i=0;
			buf[0]=0;
			while(c!='\n' && c!='\r' && c!=EOF && c!=',') {
				if(i>=sizeof(buf))
					goto err1;
				if (i || c!=' ')
					buf[i++]=c;
				c=fgetc(fh);
			}
			if(i)
				i--;
			while(i && buf[i]==' ') i--;
			buf[i+1]=0;    //buf contain a user name
			if(strlen(buf)>0 && updated!=no) {
				for (i=0;i<no;i++) {
					if(!added[i] && strcmp(ppUser[i],buf)==0){
						added[i]=1;
						updated++;
						break;
					}
				}
			}
			if(buf[0]) {
				users++;
				fputs(buf,fh1);
			}
			if(c==',') {
				fputc(c,fh1);
				goto next_user;
			}
			//now append new user
			for(i=0;(i<no && updated<no);i++) {
				if(added[i]==0){
					if(users)
						fputs(",",fh1);
					fputs(ppUser[i],fh1);
					users++;
					updated++;
				}
			}
		 	if(c==EOF)
				goto ret0;
			fputs("\n",fh1);
			//save rest data
			while(fgets(buf,sizeof(buf),fh)!=0)
				fputs(buf,fh1);
			goto ret0;
		}
		else{
			if(!eol) {
				c=fgetc(fh);
				while(c!='\n' && c!='\r' && c!=EOF){
					fputc(c,fh1);
					c=fgetc(fh);
				}
				if(c==EOF)
					goto ret0;
				fputc(c,fh1);
			}
		}
   	}
ret0:
	fclose(fh);
	fflush(fh1);
	fclose(fh1);
	sprintf(buf1,"/bin/cp -f %s %s 2>/dev/null",TmpFile,FSH_SYS_GROUP);
	system(buf1);
	ret=system(buf1);
	if(ret){
		remove(TmpFile);
		return(FSH_ADD_GRP_MEMB_ERR);
	}
	sleep(2);
	remove(TmpFile);
	return(FSH_OK);
err1:
	fclose(fh);
	fflush(fh1);
	fclose(fh1);
	remove(TmpFile);
	return(FSH_FILE_FORMAT_ERR);
}
#endif

/**thie function used to add user info to file /etc/passwd**/
static int UserAdd(u_info *pUser)
{
	uid_t id;
	struct passwd *user=0;
	char *pw=NULL,*pw1=NULL,pass0[15]={0},pass1[15]={0},tmp[64];
	FILE *fh;
	int group_id;
	u_info *p = NULL;
	
	if(!pUser||!strlen(pUser->name))
		return(INPUTPARM_ERR);
	p = pUser;
	while(pUser){
		fh=fopen(FSH_SYS_PASSWD,"at");
		if(!fh)
			return(FSH_FILE_OPEN_ERR);
		if(p!=pUser)
		{
			if(strcmp(p->name,pUser->name)==0)
			{
				p = pUser;
				pUser = pUser->next;
				fflush(fh);
				fsync(fileno(fh));
				fclose(fh);
				continue;
			}
			p = pUser;
		}    
		id=UID_Start;
		user=getpwuid(id);
		while(user){
			id++;
			user=getpwuid(id);
		}
		//Now id is the first usable user id
		if(strlen(pUser->passwd)>8){
			strncpy(pass0,pUser->passwd,8);
			pass0[8]=0;
			strncpy(pass1,pUser->passwd+8,strlen(pUser->passwd)-8);
			pass1[strlen(pUser->passwd)-8]=0;
		}
		else{
			strcpy(pass0,pUser->passwd);
			pass1[0]=0;
		}
		pw=crypt(pass0,FSH_PASSWD_SALT);
		strcpy(pass0,pw);
		if(strlen(pass1)){
			pw1=crypt(pass1,FSH_PASSWD_SALT);
			strcpy(pass1, pw1);
		}  
		group_id=GetGroupId(pUser->group);
		if((group_id<GID_Start)||(group_id>(GID_Start+GROUP_NUM))){
			return FSH_GET_GROUP_ID_ERR;
		}
		sprintf(tmp,":%d\n",group_id);	
		fprintf(fh,"%s:%s:%d:%d::%s:/dev/null\n",pUser->name,pass0,id,group_id,pass1);
		pUser = pUser->next;
		fflush(fh);
		fsync(fileno(fh));
		fclose(fh);
	}
	return(FSH_OK);
}

//---------------------------------------------------------
// purpose: add user to server
// input:  pUser - user to add
// return: FSH_OK - success
//         other - fail
//---------------------------------------------------------
/*this function will add the user info to files: /etc/group,/etc/passwd,/etc/samba/smbpasswd*/
static int AddUser(u_info *pUser)
{
	char buf[MAXUSERLEN*2+MAXPASSLEN*2+52],name[MAXUSERLEN*2+1],pwd[MAXPASSLEN*2+1];
	int ccode;
	u_info *p;
	
	if(!pUser||!pUser->name[0])
		return(INPUTPARM_ERR);
	p = pUser;
	ccode=UserAdd(pUser);/*add the user in file /etc/passwd*/
	if(ccode){
		goto err;
	}
	pUser = p;
	while(pUser)
	{
		if(p!=pUser)
		{
			if(strcmp(p->name,pUser->name)==0)
			{
				p = pUser;
				pUser = pUser->next;
				continue;
			}
			p = pUser;
		}
		DBCS_SpecialChar(pUser->name,name,FSH_SPECIAL_CHAR) ;/*judge if there is some special char in username*/
		if (!pUser->passwd[0]){
			sprintf(buf,"/usr/sbin/smbpasswd -a -n -- \"%s\" 2>/dev/null",name); 
		}else{
			DBCS_SpecialChar(pUser->passwd,pwd,FSH_SPECIAL_CHAR) ;
			sprintf(buf,"/usr/sbin/smbpasswd -a -- \"%s\" \"%s\" 2>/dev/null",pUser->name,pwd); 
		}
		ccode=system(buf);//add user to samba user :/etc/samba/smbpasswd
		pUser = pUser->next;
	}
	return(FSH_OK);
err:	
    return(ccode);
}


static int start_smb(void)
{
	char *smb_server_enable=nvram_safe_get("smb_server_enable");
	
	SYSTEM("/usr/bin/killall -9 smbd");
	SYSTEM("/usr/bin/killall -9 nmbd");

	SMB_SETSTATUS(SMB_STOPPED);
	
	if(*smb_server_enable=='1' && usb_check_exist() == 1)
	{
		create_smb_config();
		/* Now we start the smb */
		SYSTEM("/usr/sbin/smbd -D");
		SYSTEM("/usr/sbin/nmbd -D");
		SMB_SETSTATUS(SMB_STARTED);
	}
	return 0;
}

static int stop_smb(void)
{
	SYSTEM("/usr/bin/killall smbd");
	SYSTEM("/usr/bin/killall nmbd");
	SMB_SETSTATUS(SMB_STOPPED);
	return 0;
}

static int create_smb_config(void)
{
	int group_num,user_num,share_num,group_share_num;
	
	group_info_t *group = NULL;
	group_info_t *group_current = NULL;
	
	user_info_t *user = NULL;
	user_info_t *user_current = NULL;
	
	group_share_t *group_share = NULL;
	//group_share_t *group_share_current = NULL;
	
	share_info_t *share = NULL;
	share_info_t *share_current = NULL;
	int ret,share_all_partition;
    	char tmp[1024];
    	char *service_name,*workgroup_name,*all_partitions_share_enable;

    	/* Copy smb's configure file to /etc and /etc/samba */
    	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", FSH_SYS_GROUP_BAK, FSH_SYS_GROUP);
	SYSTEM (tmp);
    	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", FSH_SYS_PASSWD_BAK, FSH_SYS_PASSWD);
	SYSTEM (tmp);
    	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", FSH_SMB_PASSWD_BAK, FSH_SMB_PASSWD);
	SYSTEM (tmp);
    	sprintf (tmp, "/bin/cp -f %s %s 2>/dev/null", FSH_SMB_CONF_BAK, FSH_SMB_CONF);
	SYSTEM (tmp);
    	/* Get service name and workgroup name */   
    	service_name=nvram_safe_get("storage_machine_name");
    	workgroup_name=nvram_safe_get("storage_new_workgroup");
#if 0
    	netbios_name=nvram_safe_get("storage_machine_des");
    	change_servicename_workgroupname(service_name,workgroup_name,netbios_name);
    	free(netbios_name);
#else
    	change_servicename_workgroupname(service_name,workgroup_name,service_name);
#endif
    	free(service_name);
    	free(workgroup_name);
	
    	/* Read config info from nvram and parse them */		
	group_num=read_group_from_nvram(&group);
	user_num=read_user_from_nvram(&user);
	share_num=read_share_from_nvram(&share);
	group_share_num=read_group_share_from_nvram(&group_share);
    
    	/*
     	 * After parse and save the config info in appropriate struct, we
     	 * should use them to change the config file such as
     	 * :/etc/group,/etc/passwd
     	 */
   
	seep("group_num:%d,user_num:%d,share_num:%d,group_share_num:%d\n",group_num,user_num,share_num,group_share_num);
   
	if ( group_num > 0 ) { /* Add group in file /etc/group */
		group_current=group;
        	AddGroup(group_current,user);
	}

    	/*
     	 * Add the user info to files:
     	 * /etc/group,/etc/passwd,/etc/samba/smbpasswd
     	 */
	if( user_num > 0 ) {
		user_current=user;
		AddUser(user_current);
	}
    
    	/* First ,we should judge if need to share all partition */	
    	all_partitions_share_enable=nvram_safe_get("all_partitions_share_enable");
    	share_all_partition=atoi(all_partitions_share_enable);
    	free(all_partitions_share_enable);
    	if ( share_all_partition ) {
        	Share_all_partition(group_share);
   	}
    
    	/* Add the share info to files: /etc/samba/smb.conf */
    	if ( share_num > 0 ) {
		share_current = share;
		ret = AddShare(share_current,group_share);
	}

    	FreeGroupListt(group);
    	FreeUserListt(user);
    	FreeGroupShareListt(group_share);
    	FreeShareListt(share);

	return 0;
}

/* call this when usb rootdir updated : 
   1. all usb directories disappear, it means all usb disks were umount,
      it's time to stop this smb server.
   2. at lease one usb directory was detected, it means there was usb disk mount,
      it's time to start this smb server( if not start yet ).
*/
int smb_update(void)
{
	char *smb_enable = nvram_safe_get("smb_server_enable");
	int  status;
	
	if ( *smb_enable == '0' || usb_check_exist() != 1 ) {
		stop_smb();
	} else {
		SMB_GETSTATUS(status);
		if ( status != SMB_STARTED ) {
			start_smb();
		}
	}
	return 0;
}

int samba_main(int argc, char *argv[])
{
	if (Strcmp(argv[0], "start")) {
		start_smb();
	} else if (Strcmp(argv[0], "stop")) {
		stop_smb();
	} else if (Strcmp(argv[0], "restart")) {
		stop_smb();
		start_smb();
	} else if (Strcmp(argv[0], "update")) {
		smb_update();
	} else if (Strcmp(argv[0], "create")) {
		create_smb_config();
	}
	return 0;
}

