#ifndef _LIB_SAMBA_H_
#define _LIB_SAMBA_H_

#define GROUP_NUM 64
#define USER_NUM 128
#define SHARE_NUM 64
#define MAXGROUPLEN 256
#define MAXUSERLEN 256
#define MAXPASSLEN 256
#define MAXPARTITIONLEN 256
#define MAXSHARELEN 256
#define STRING_LEN 1024*8

/*the configuration of group save in nvram:storage_group=group1,group2,group3........*/
/*the configuration of user save in nvram :storage_user={username1,passwd1,group1},{username2,passwd2,group2}........*/
/*the configuration of share in nvram :storage_share={name1,partition1 total_space1,{name2,partition2,total_space2}.........*/
/*the configuration of group_share in nvram :storage_gshare={sharename,groupnaem,permission}...*/
	
typedef struct group_info{
	char name[MAXGROUPLEN];
	struct group_info *next;
}group_info_t;


typedef struct user_info{
	char name[MAXUSERLEN];//the name user
	char passwd[MAXPASSLEN];// the passwork of user
	char group[MAXGROUPLEN];// the group that the user belong to	
	struct user_info *next;
}user_info_t;
typedef user_info_t u_info;

typedef struct group_share_info{
	char sharename[MAXSHARELEN];//the name of directory that you can see from samba
	char groupname[MAXGROUPLEN];//the group name in file /etc/samba/smb.conf
	int     permission;//0:only read,1:can write
	struct group_share_info *next;
}group_share_t;


typedef struct share_info{
	char sharename[MAXSHARELEN];//the name of directory that you can see from samba
	char partition[MAXPARTITIONLEN];//the share folder
	int     total_space;//the size of the share folder
	struct share_info *next;
}share_info_t;


#define	UID_Start       2000
#define	GID_Start       2000
#define FSH_OK 0

#define	FSH_SPECIAL_CHAR				"\"$\\`"

#define CREATE_TMPFILE_ERR 20
#define FILE_OPEN_ERR				21
#define READ_FILE_ERR				25
//Group 
#define	FSH_SYS_GROUP			"/etc/group"
#define	FSH_SYS_GROUP_BAK			"/etc/group.smb"
#define	FSH_FILE_FORMAT_ERR				24 		// Format of file is wong.
#define	FSH_ADD_GRP_MEMB_ERR			34		// Fail to add user  to group.
#define FSH_ADD_GROUP_ERR				37		// Fail to add the group.
#define FSH_DEL_GROUP_ERR				38		// Fail to delete the group.
#define FSH_GRP_EXIST_ERR				43		// The group has been existing.
#define FSH_GRP_TOOMANYGRP_ERR			41		// Groups are too many.
#define	FSH_GET_GROUP_ID_ERR			-5			// Fail to get id of the group.
//User
#define	FSH_PASSWD_SALT			"sc"//for crypt the passwd?
#define	FSH_SYS_PASSWD			"/etc/passwd"
#define	FSH_SYS_PASSWD_BAK			"/etc/passwd.smb"
#define	FSH_USER_DUP_ERR				26		// The user has been existing already.

//Common Error
#define READ_ERROR -1
#define INPUTPARM_ERR -2
#define ALLOCATE_MEM_ERR -3
#define	FSH_FILE_OPEN_ERR				21		// Fail to open file.
//share
#define FSH_SMB_PASSWD              "/etc/samba/smbpasswd"
#define FSH_SMB_PASSWD_BAK      "/etc/samba.conf/smbpasswd"
#define FSH_SMB_CONF			"/etc/samba/smb.conf"
#define FSH_SMB_CONF_BAK          "/etc/samba.conf/smb.conf"
#define FSH_WRITE_SMB_CONF_ERR			45		// Fail to write file smb.conf
#define	FSH_SHARE_DUP_ERR				48		// The share has been existing.

#define LIB_SMB_ST "/var/run/lib_smb_st"

#define SMB_STOPPED	0
#define SMB_STARTED	1

#define SMB_SETSTATUS(_st) do { \
	int __s = _st; \
	set_file_value(LIB_SMB_ST, 0, &__s, sizeof(__s)); \
} while(0)

#define SMB_GETSTATUS(_st) do { \
	int __s; \
	if ( get_file_value(LIB_SMB_ST, 0, &__s, sizeof(__s)) == sizeof(__s) ) { \
		_st = __s; \
	} else { \
		_st = -1; \
	} \
} while (0)

	
#endif
