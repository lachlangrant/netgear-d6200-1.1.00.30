#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "ipt_http_string.h"

#define DEFAULT_CONF_FILE "./acl"

static void usage(char *name)
{
    printf("Usage: %s [options] {dump|set|add|del} [pattern]\n\n", name);
    printf("Commands are:\n");
    printf("\t%-10s\t dump the pattern contents\n", "dump");
    
    printf("\t%-10s\n", "set {hex value}");
    printf("\t%s\n", "set {java|activex|cookie|proxy} {enable|disable}");
    printf("\t%-10s\t enable/disable http filter features\n", " ");
    printf("\t%-10s\t add a pattern, type is indicated by '-t'\n", "add [pattern]");
    printf("\t%-10s\t delete a pattern, type is indicated by '-t'\n", "del [pattern]");
    printf("\n");
    printf("Options are:\n");
    printf("\t%-10s\t show help information\n", "-h");
    printf("\t%-10s\t configue file name\n", "-f config-file");
    printf("\t%-10s\t indicate the type of following pattern\n", "-t pattern-type");
    printf("\t%-10s\t pattern-type = {uri|host|content}\n", " ");
    
    exit(0);
}

static struct ipt_type_mapping {
    int type;
    char *string;
} ipt_type_mapping[] = {
    {
        IPT_PATTERN_TYPE_UNKNOWN,
        "unknown",
    },
    {
        IPT_PATTERN_TYPE_CONTENT,
        "content",
    },
    {
        IPT_PATTERN_TYPE_HOST,
        "host",
    },
    {
        IPT_PATTERN_TYPE_URI,
        "uri",
    },
    {
        -1,
        "unknown",
    }
};

static char *iptpat_type2str(int type)
{
    struct ipt_type_mapping *ipttm;

    ipttm = &ipt_type_mapping[0];
    while(ipttm->type != -1)
    {
        if(ipttm->type == type)
            return ipttm->string;
        ipttm++;
    }

    return ipttm->string;
}

static int iptpat_str2type(char *string)
{
    struct ipt_type_mapping *ipttm;

    ipttm = &ipt_type_mapping[0];
    while(ipttm->type != -1)
    {
        if(!strcmp(ipttm->string, string))
            return ipttm->type;
        ipttm++;
    }

    return ipttm->type;
}

static void dump_iptinfo(struct ipt_http_string_info *iptinfo)
{
	int i;

    printf("FLAGS: 0x%08x [ENABLE: %s%s%s%s]\n",
           iptinfo->flags,
           iptinfo->flags&IPT_HTTP_FLAG_JAVA?"java":"",
           iptinfo->flags&IPT_HTTP_FLAG_ACTIVEX?" activex":"",
           iptinfo->flags&IPT_HTTP_FLAG_COOKIE?" cookie":"",
           iptinfo->flags&IPT_HTTP_FLAG_PROXY?" proxy":"");
    
	printf("PATTERN LIST:\n");

    printf("%-8s %-8s %-8s %-16s\n", "#", "TYPE", "PATLEN", "PATTERN");
    for(i = 0; i < IPT_PATTERN_NUMBER; i++)
    {
        if(iptinfo->iptpat[i].patlen != 0)
        {
            printf("%-8d %-8s %-8d %-16s\n",
                   i,
                   iptpat_type2str(iptinfo->iptpat[i].type),
                   iptinfo->iptpat[i].patlen,
                   iptinfo->iptpat[i].pattern);
        }
    }    
}

int main(int argc, char *argv[])
{
    const char *sopts = "hf:t:";
    int rc, nopt;    
    char *file = NULL;
    FILE *fp;
    struct ipt_http_string_info iptinfo;
    int pattern_type = 0;

    do {
        nopt = getopt(argc, argv, sopts);
        switch(nopt)
        {
            case 'h':
                usage(argv[0]);
                break;
            case 'f':
                file = strdup(optarg);
                break;                
            case 't':
                pattern_type = iptpat_str2type(optarg);
                break;
            case '?':
            case -1:
                break;
            default:
                break;                
        }
    }while(nopt != -1);

    if(argc - optind < 1)
    {
        usage(argv[0]);
    }
    
    if(file == NULL) file = DEFAULT_CONF_FILE;

    if((fp = fopen(file, "a+")) == NULL)
        return -1;
    if((rc = fread(&iptinfo, 1, sizeof(iptinfo), fp)) != sizeof(iptinfo))
    {
        if(rc != sizeof(iptinfo))
        {
            memset(&iptinfo, 0, sizeof(iptinfo));
        }
    }
    fclose(fp);
    
    if(strcmp(argv[optind], "dump") == 0)
    {
        dump_iptinfo(&iptinfo);
    }
    else if(strcmp(argv[optind], "set") == 0)
    {
        int enable = 0;

        optind++;
        if(argc < optind+1)
        {
            usage(argv[0]);
        }
        
        if((argc > optind+1) && (argv[optind+1][0] == 'e'))
        {
            enable = 1;
        }
        
        if(strcmp(argv[optind], "java") == 0)
        {
            iptinfo.flags = enable
                ?iptinfo.flags|IPT_HTTP_FLAG_JAVA
                :iptinfo.flags&(~IPT_HTTP_FLAG_JAVA);
        }
        else if(strcmp(argv[optind], "activex") == 0)
        {
            iptinfo.flags = enable
                ?iptinfo.flags|IPT_HTTP_FLAG_ACTIVEX
                :iptinfo.flags&(~IPT_HTTP_FLAG_ACTIVEX);
        }
        else if(strcmp(argv[optind], "cookie") == 0)
        {
            iptinfo.flags = enable
                ?iptinfo.flags|IPT_HTTP_FLAG_COOKIE
                :iptinfo.flags&(~IPT_HTTP_FLAG_COOKIE);
        }
        else if(strcmp(argv[optind], "proxy") == 0)
        {
            iptinfo.flags = enable
                ?iptinfo.flags|IPT_HTTP_FLAG_PROXY
                :iptinfo.flags&(~IPT_HTTP_FLAG_PROXY);
        }
        else
        {
            sscanf(argv[optind], "%x", &iptinfo.flags);
            /* iptinfo.flags = atoi(argv[optind]); */
        }
        
        if((fp = fopen(file, "w")) == NULL)
            return -1;

        if((rc = fwrite(&iptinfo, 1, sizeof(iptinfo), fp)) != sizeof(iptinfo))
        {
            fclose(fp);
            return -1;
        }

        fclose(fp);
    }    
    else if(strcmp(argv[optind], "del") == 0)
    {
        int i = atoi(argv[++optind]);
        
        memset(&iptinfo.iptpat[i], 0, sizeof(iptinfo.iptpat[i]));

        if((fp = fopen(file, "w")) == NULL)
            return -1;

        if((rc = fwrite(&iptinfo, 1, sizeof(iptinfo), fp)) != sizeof(iptinfo))
        {
            fclose(fp);
            return -1;
        }

        fclose(fp);
    }
    else if(strcmp(argv[optind], "add") == 0)
    {
        int i;

        for(i = 0; i < IPT_PATTERN_NUMBER; i++)
        {
            if(iptinfo.iptpat[i].patlen == 0)
            {
                break;
            }
        }

        if(i == IPT_PATTERN_NUMBER)
        {
            printf("no free space to add\n");
            return -1;
        }

        iptinfo.iptpat[i].type = pattern_type;
        strncpy(iptinfo.iptpat[i].pattern, argv[++optind],
                sizeof(iptinfo.iptpat[i].pattern));
        iptinfo.iptpat[i].patlen = strlen(iptinfo.iptpat[i].pattern);

        if((fp = fopen(file, "w")) == NULL)
            return -1;

        if((rc = fwrite(&iptinfo, 1, sizeof(iptinfo), fp)) != sizeof(iptinfo))
        {
            fclose(fp);
            return -1;
        }

        fclose(fp);
    }
    
    return 0;
}
