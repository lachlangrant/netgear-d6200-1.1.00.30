
/***************************************************************************
 File info: Netgear logging implementation based on log queue.
 Author: Sercomm Corp.
 Date: Jul.16 2011
****************************************************************************/

#include "log_queue.h"

static unsigned int log_queue_map[LOG_QUEUE_NUM] = {
   (1<<14),
   ((1<<14) | (1<<15)),
   ((1<<10) | (1<<11) | (1<<12) | (1<<13)),
   ((1<<1) | (1<<2) | (1<<3) | (1<<4) | (1<<5) | (1<<7) | (1<<8) | (1<<9) | (1<<16) | (1<<17) | (1<<21) | (1<<22)),
   (1<<19),
   (1<<20),
   ((1<<6) | (1<<18)),
   ((1<<23) | (1<<24) | (1<<25) | (1<<26)),
   (1<<27),
   (1<<28),
   (1<<29),
   (1<<31),
};

/* Check if this queue should be in log window. */
static int queue_in_window(int qid)
{
    int bits = log_queue_map[qid];
    
    if (!(bits & conf.log_event))
        return 0;
    if (qid == 0) {
        if ((conf.log_event & (1<<14)) && (conf.log_minor_event & (1<<1)))
            return 1;
        return 0;
    }
    if (qid == 1) {
        if (conf.log_event & (1<<15))
            return 1;
        if ((conf.log_event & (1<<14)) && (conf.log_minor_event & (1<<2)))
            return 1;
        return 0;
    }
    return 1;
}

/* Match the log event to queue. */
static int log_event_queue(int major, int minor)
{
    int i;

    if (major == 14) {
        if (minor == 1)
            return 0;
        else
            return 1;
    }
    for (i = 0; i < LOG_QUEUE_NUM; i++) {
        if (log_queue_map[i] & (1<<major))
            return i;
    }
    return -1;
}

static int NTGR_log_isfull(void)
{
    int count = lq_get_entries_in_window();
#if (_LOG_FULL_PERCENT_ < 100)
    int major = (count * 100) / LOG_WINDOW_SIZE;
    int minor = (count * 100) % LOG_WINDOW_SIZE;

    major += (minor > 0)?1:0;
    return (major > _LOG_FULL_PERCENT_);
#else
    return (count == LOG_WINDOW_SIZE);
#endif
}

/* check if previous log is the same as input log `msg'. */
static int NTGR_log_isduplicated(int major, int minor, const char *msg)
{
    int qid = log_event_queue(major, minor);
    struct log_queue_t *q;
    struct log_entity_t *plog;

    if (qid < 0)
        return 0;

    q = lq_get_queue(qid);
    if (!q || q_empty(q))
        return 0;

    /* Only check the first log in queue. */
    plog = &q->lq[q->head];

    return (strncmp(msg, plog->msg, strlen(msg)) == 0);
}

/* format the log message. */
static char * NTGR_log_print1(struct log_entity_t *plog)
{
    static char out_msg[PER_LOG_BUF_SIZE + 128];
    char time_buf[128];
    struct tm *resume_tm = NULL;
    char *p;

    strcpy(out_msg, plog->msg);

    if (plog->abs_timestamp > 0) {
        resume_tm = localtime(&plog->abs_timestamp);
        sprintf(time_buf," %s, %s %02d,%02d %02d:%02d:%02d"
            ,wday[resume_tm->tm_wday]
            ,month[resume_tm->tm_mon]
            ,resume_tm->tm_mday
            ,resume_tm->tm_year+1900
            ,resume_tm->tm_hour
            ,resume_tm->tm_min
            ,resume_tm->tm_sec);

        p = strstr(out_msg, EMPTY_TIMESTAMP);
        if (p)
            strncpy(p, time_buf, strlen(time_buf));
    }
    return out_msg;
}

static void NTGR_log_print(void)
{
    lq_print_window(logFilePath, NTGR_log_print1);
    /* notify somebody the log is now up to date. */
    unlink("/tmp/log_printing");
}

static void NTGR_message (int major, int minor, char *fmt, ...)
{
    struct log_queue_t *q;
    va_list arguments;
    char b[1024];
    int i;

    va_start (arguments, fmt);
    vsnprintf (b, sizeof(b)-1, fmt, arguments);
    va_end (arguments);

    lq_debug("entering <%s>\n", b);

    i = log_event_queue(major, minor);
    if (i < 0) {
        lq_debug("invalid queue id \n");
        return ;
    }
    lq_debug("log_queue = %d \n", i);

    q = lq_get_queue(i);
    if (!q)
        return;

    lq_enqueue(q, b);

    if (NTGR_log_isfull() && conf.mail_log_full==1) {
        lq_debug("send mail due to log is full.\n");
        do_send_mail();
    }

    return ;
}

/* refill log window from log queue due to external configuration changed. */
static int NTGR_log_refill(void)
{
    static int log_event = 0;
    static int log_minor_event = 0;
    struct log_queue_t *q;
    int i;

    lq_debug("entering \n");

    /* do nothing when configuration not changed. */
    if (log_event == conf.log_event
        && log_minor_event == conf.log_minor_event)
        return 0;

    log_event = conf.log_event;
    log_minor_event = conf.log_minor_event;

    lq_cleanup_window();

    for (i = 0; i < LOG_QUEUE_NUM; i++) {

        if (!queue_in_window(i))
            continue;

        q = lq_get_queue(i);
        if (q)
            lq_refill_window(q);
    }

    return 0;
}

/* Cleanup all log entries. Or only cleanup the queue in window ? */
static void NTGR_log_cleanup(void)
{
    lq_cleanup();
}

static void NTGR_log_init(void)
{
    lq_init();
}
