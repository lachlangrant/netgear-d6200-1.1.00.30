#!/bin/sh
# pid.Function length is 4+4. 
# 		Function_ID  	save major	(hex)
#		Function_Mask	save minor	(hex)
MAX_LENGTH=4
OFFSET_MAJOR=92
OFFSET_MINOR=96

if [ ! -f "zip3g.par" ]; then
	echo ""
	echo "You can't run this script in this PATH"
	echo ""
	exit 1
fi

PATH_3G_TRUNK="../../apps/3g_trunk/src"

if [ ! -f $PATH_3G_TRUNK/include/dongle_monitor.h ]; then
	echo "Can not found file $PATH_3G_TRUNK/include/dongle_monitor.h"
	echo ""
	exit 1
fi
if [ ! -f $PATH_3G_TRUNK/usb_modeswitch-0.9.7/libusb-0.1.12/Makefile ]; then
	echo "Can not found file $PATH_3G_TRUNK/usb_modeswitch-0.9.7/libusb-0.1.12/Makefile"
	echo ""
	exit 1
fi

# Major from dongle_monitor.h
MAJOR=`cat $PATH_3G_TRUNK/include/dongle_monitor.h | grep 'DONGLE_MAJOR_VER' | awk '{print $3}'`
# Minor from ./usb_modeswitch-0.9.7/libusb-0.1.12/Makefile
#MINOR=`cat $PATH_3G_TRUNK/usb_modeswitch-0.9.7/libusb-0.1.12/Makefile | grep 'DONGLE_MINOR_VER'  | awk -F'DONGLE_MINOR_VER=' '{print $2}'`
MINOR=`cat $PATH_3G_TRUNK/include/dongle_monitor.h | grep 'DONGLE_MINOR_VER' | awk '{print $3}'`

HEX_MAJOR=`printf "%04x" $MAJOR`
HEX_MINOR=`printf "%04x" $MINOR`
LEN_MAJOR=`echo $HEX_MAJOR | wc -L`
LEN_MINOR=`echo $HEX_MINOR | wc -L`

VER_INFO=`echo "$MAJOR.$MINOR"`
OUTFILE=`echo zip3g_$MAJOR\_$MINOR`

echo $OUTFILE

if [ $LEN_MAJOR -gt $MAX_LENGTH -o $LEN_MINOR -gt $MAX_LENGTH ]; then
	echo "Error: Version information $VER_INFO"
	echo ""
fi


# fill zero
sed "s/\(.\{$OFFSET_MAJOR\}\)\(.\{8\}\)/\100000000/" ../DGND4000.pid > zip3g.pid
# replace Major
sed -i "s/\(.\{$OFFSET_MAJOR\}\)\(.\{$LEN_MAJOR\}\)/\1$HEX_MAJOR/" zip3g.pid
# replace Minor
sed -i "s/\(.\{$OFFSET_MINOR\}\)\(.\{$LEN_MINOR\}\)/\1$HEX_MINOR/" zip3g.pid

# create bin file
../scBind -i zip3g.par -o $OUTFILE.bin
# create zip image
../tools/src/zipImage/zipImage $OUTFILE.bin zip3g
chmod +r $OUTFILE.img

# print version information
echo ""
echo "#### Version information from dongle_monitor.h ####"
echo "#### Create Upgrade3G" $VER_INFO "     ####"
echo "#### Image File:" $OUTFILE.img        "####"
echo ""

#rm filesystem temp file.
rm zip3g.img $OUTFILE.zip

