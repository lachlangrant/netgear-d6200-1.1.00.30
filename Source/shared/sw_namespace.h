
#ifndef __SW_NAMESPACE_H__
#  define __SW_NAMESPACE_H__

#  define ENV_PATH "/proc/ticfg/env"
#  define NVRAM_PROV_DOMAIN_STR					"prov_tftp_domain"
#  define NVRAM_TFTP_DEFAULT_PORT_NUM_STR		"prov_tftp_default_port"
#  define NVRAM_TFTP_PRIMARY_PORT_NUM_STR		"prov_tftp_primary_port"
#  define NVRAM_TFTP_SECONDARY_PORT_NUM_STR	"prov_tftp_secondary_port"
#  define NVRAM_RC4_KEY_STR				"prov_rc_key"
#  define NVRAM_RELOAD_PERIOD_STR			"prov_period_inteval"
#  define NVRAM_CFG_FILE_VERSION			"prov_cfg_fileversion"
#  define NVRAM_CFG_FILE_NAME				"prov_cfg_filename"
#  define NVRAM_FW_UPDATE_FILE_NAME		"prov_fw_filename"
#  define NVRAM_VOIP_WEB_HIDDEN				"voip_web_hidden"
#  define	NVRAM_TELNET_ACTIVE_STATUS		"telnet_active_status"

#  define ENV_DEFAULT_RC4_KEY				"RC4_KEY"
#  define ENV_TFTP_PERIOD_INTERVAL		"TFTP_PERIOD_INTERVAL"
#  define ENV_DEFAULT_TFTP_SERVER_IP		"TFTP_SERVER_DOMAIN"
#  define ENV_TFTP_DEFAULT_PORT			"TFTP_DEFAULT_PORT"
#  define ENV_TFTP_PRIMARY_PORT			"TFTP_PRIMARY_PORT"
#  define ENV_TFTP_SECONDARY_PORT			"TFTP_SECONDARY_PORT"
#  define ENV_TFTP_CFG_FILE_VERSION		"TFTP_CFG_FILE_VERSION"
#  define ENV_TFTP_CFG_FILE_NAME			"TFTP_CFG_FILE_NAME"
#  define ENV_FW_UPDATE_FILE_NAME			"FW_UPDATE_FILE_NAME"
#  define ENV_VOIP_WEB_HIDDEN			"VOIP_WEB_HIDDEN"
#  define ENV_TELNET_ACTIVE_STATUS		"TELNET_ACTIVE_STATUS"

#endif /* __SW_NAMESPACE_H__ */
