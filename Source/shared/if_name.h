#ifndef _IF_NAME_H_
#define _IF_NAME_H_

/* LAN phycial interface name */
#define LAN_PHY_IFNAME          "eth0"

/* WLAN phycial interface name */
#define WLAN_PHY_IFNAME         "wl0"
#define WLAN_5G_PHY_IFNAME      "wl1"
/* Guest iface */
#define WLAN_GUEST1_PHY_IFNAME  "wlan0-va0"

#ifdef _ADSL_
#define WAN_PHY_IFNAME          "nas1"
#else
#define WAN_PHY_IFNAME          "eth1"
#endif
#define WAN_ETH_PHY_IFNAME          "eth4"

/* Logic LAN interface name */
#define LAN_LOG_IFNAME          "group1"

/* SYSTEM ethernet interface name */
#define SYS_ETH_IFNAME          "eth0"


/* VLAN start num when QOS enable */
#define VLAN_START_NUM_QOS_ENABLE          11
#define VLAN_START_NUM_QOS_DISABLE          1


#endif/*_IF_NAME_H_*/

