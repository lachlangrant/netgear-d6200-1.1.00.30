/***************************************************************
 *             Copyright (C) 2003 by SerComm Corp.
 *                    All Rights Reserved.
 *
 *      Use of this software is restricted to the terms and
 *      conditions of SerComm's software license agreement.
 *
 *                        www.sercomm.com
 ****************************************************************/
#ifndef _UPGRADE_FLASH_H_
#define _UPGRADE_FLASH_H_

#define FILESYSTEM	    "/dev/mtd5"
#define KERNEL_PATH	    "/dev/mtd5"
#define LOADER	 	    "/dev/mtd1"
#define FLAG_PATH	 	"/dev/mtd6"
#define PCBASN_PATH	 	"/dev/mtd7"
#define LANGUAGE_DEV 	"/dev/mtd13"
#define KFS_SIZE        0x02400000
#define LANG_SIZE       0x00500000
#define PCBASN_MTD_SIZE     0x00100000

#ifdef _ZIP_3G_
#define DONGLE_DRIVER_PATH "/dev/mtd14"
#define DONGLE_DRIVER_SIZE 0x100000
int dongle_driver_version_check(char *file);
int dongle_driver_image_is_correct(char *file);
void dongle_driver_update(char *img);
#endif

#define MAGIC_SIGN      "sErCoMm"
#define Erase_All_MAGIC_CONST           0x77
#define ZIP3G_MAGIC_CONST               0x11

#define UPGRADE_TIMEOUT 15
typedef unsigned long UINT32;
#define ERROR_REPORT(fmt, args...)  SC_XCFPRINTF("\033[31m" fmt "\033[0m", ##args)
#define LOG_REPORT(fmt, args...)    SC_XCFPRINTF("\033[32m" fmt "\033[0m", ##args)
#define TMP_FW          "/tmp/fw.img"
#define UPGRADING_FILE	"/tmp/upgrading"
#define KERNEL_FS_OFFSET 0x00200000

#define PID_LEN		    70
#define MIN_UPLOAD_LEN  (1024*1024)

#ifdef FLASH_4M
#define FLASH_SIZE      (4096*1024)
#endif

#ifdef FLASH_8M
#define FLASH_SIZE      (8192*1024)
#endif

#ifdef FLASH_16M
#define FLASH_SIZE      (16384*1024)
#endif

#ifdef FLASH_32M
#define FLASH_SIZE      (2*16384*1024)
#endif

#ifdef FLASH_128M
#define FLASH_SIZE      (0x08000000)
#endif

#define NVRAM_BOOTLINE_LEN              256
#define NVRAM_BOARD_ID_STRING_LEN       16
#define NVRAM_MAC_ADDRESS_LEN           6

#define NVRAM_GPON_SERIAL_NUMBER_LEN    13
#define NVRAM_GPON_PASSWORD_LEN         11

#define NVRAM_WLAN_PARAMS_LEN      256
#define NVRAM_WPS_DEVICE_PIN_LEN   8

#define THREAD_NUM_ADDRESS_OFFSET       (NVRAM_DATA_OFFSET + 4 + NVRAM_BOOTLINE_LEN + NVRAM_BOARD_ID_STRING_LEN)
#define THREAD_NUM_ADDRESS              (0x80000000 + THREAD_NUM_ADDRESS_OFFSET)

#define NP_BOOT             0
#define NP_ROOTFS_1         1
#define NP_ROOTFS_2         2
#define NP_DATA             3
#define NP_BBT              4
#define NP_TOTAL            5

#define SERIAL_NUMBER_LEN		16
#define PIN_CODE_LEN        8
#define SERCOMM_MAC_NUM     16
#define SERCOMM_PSI_SIZE    DEFAULT_PSI_SIZE
#define PCBA_SERIAL_NUM_LEN     12
#define REMOTESCFGMGRMAGIC_LEN     4
#define REGION_LEN          2
#define DOMAIN_LEN          2
#define LANGUAGEID_LEN      2
#define CSN_LEN             42
#define SSID_LEN            20
#define WPAKEY_LEN          64

typedef struct
{
    unsigned long ulVersion;
    char szBootline[NVRAM_BOOTLINE_LEN];
    char szBoardId[NVRAM_BOARD_ID_STRING_LEN];
    unsigned long ulMainTpNum;
    unsigned long ulPsiSize;
    unsigned long ulNumMacAddrs;
    unsigned char ucaBaseMacAddr[NVRAM_MAC_ADDRESS_LEN];
    char pad;
    char backupPsi;  /**< if 0x01, allocate space for a backup PSI */
    unsigned long ulCheckSumV4;
    char gponSerialNumber[NVRAM_GPON_SERIAL_NUMBER_LEN];
    char gponPassword[NVRAM_GPON_PASSWORD_LEN];
    char wpsDevicePin[NVRAM_WPS_DEVICE_PIN_LEN];
    char wlanParams[NVRAM_WLAN_PARAMS_LEN];
    unsigned long ulSyslogSize; /**< number of KB to allocate for persistent syslog */
    unsigned long ulNandPartOfsKb[NP_TOTAL];
    unsigned long ulNandPartSizeKb[NP_TOTAL];
    char szVoiceBoardId[NVRAM_BOARD_ID_STRING_LEN];
    unsigned long afeId[2];
    char chUnused[364];
    unsigned long ulCheckSum;
} NVRAM_DATA, *PNVRAM_DATA;

#define NVRAM_DATA_OFFSET               0x0580
#define offset(para) ((unsigned int)&(((NVRAM_DATA*)0x0580)->para))


#define PID_OFFSET	    (0x4000 - PID_LEN) /* from .par file */

#define MAC_OFFSET	    offset(ucaBaseMacAddr)
#define MAC_LEN         NVRAM_MAC_ADDRESS_LEN

#if 0
#define SN_OFFSET       offset(ucaSerialNum)
#define WPSPIN_OFFSET   offset(ucaPinCode)
#define REGION_OFFSET   offset(ulRegion)
#define LANG_ID_OFFSET  offset(ulLanguageId)
#define DOMAIN_OFFSET   offset(ulDomain)
#define PCBA_SN_OFFSET  offset(pcbaSerialNum)
#else
/*Bootloader R0.20: 0x3EB0-0x3FB0 is protect area,
 we have enough space for following configurations*/
#define CONF_OFFSET     0x3EB0              /*defined by Bootloader, R0.20*/
#define SN_OFFSET       CONF_OFFSET + 0x00
#define WPSPIN_OFFSET   CONF_OFFSET + 0x10
#define REGION_OFFSET   CONF_OFFSET + 0x20
#define LANG_ID_OFFSET  CONF_OFFSET + 0x30
#define DOMAIN_OFFSET   CONF_OFFSET + 0x40
#define PCBA_SN_OFFSET  CONF_OFFSET + 0x50     
/*PCBA length from 16 byte to 12 byte,the 4 byte is used for scfgmgr magic*/
#define REMOTESCFGMGRMAGIC_OFFSET  CONF_OFFSET + 0x5c 
#define CSN_OFFSET      CONF_OFFSET + 0x60  /*CSN_LEN is 42, next use 0x90*/
#define SSID_OFFSET      CONF_OFFSET + 0x90 /* SSID_LEN is 20, next use 0xB0*/
#define WPAKEY_OFFSET    CONF_OFFSET + 0xB0  /*WPAKEY_LEN is 63, next use 0xf0*/
#endif

#define SN_LEN          SERIAL_NUMBER_LEN
#define WPSPIN_LEN      PIN_CODE_LEN
#define LANG_ID_LEN     LANGUAGEID_LEN
#define PCBA_SN_LEN     PCBA_SERIAL_NUM_LEN

typedef struct sercomm_pid
{
    unsigned char	magic_s[7];	/* sErCoMm */
    unsigned char	ver_control[2];	/* version control */
    unsigned char	download[2];	/* download control */
    unsigned char	hw_id[32];  	/* H/W ID */
    unsigned char	hw_ver[2];  	/* H/W version */
    unsigned char	p_id[4];    	/* Product ID */
    unsigned char	protocol_id[2];	/* Protocol ID */
    unsigned char	fun_id[6];	/* Function ID */
    unsigned char	fw_ver[2];	/* F/W version */
    unsigned char	start[2];	/* Starting code segment */
    unsigned char	c_size[2];	/* Code size (kbytes) */
    unsigned char	magic_e[7];	/* sErCoMm */
}sercomm_pid_t;

int is_downgrade(char *file);
//void show_and_exit(int index);
int image_is_correct(char *file);
void gw_fw_update(char *img);
void upgrade_download_init(void);
void upgrade_failed(void);

#endif /* _UPGRADE_FLASH_H_ */

