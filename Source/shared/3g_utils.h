#ifndef _3G_UTILS_H_
#define _3G_UTILS_H_
enum
{
	IDLE3G = 0,
	
	NOUSBDEV3G,
	
	IDENTIFYING = 2,
	UNSURPPORTDONGLE,
	SURPPORTEDDONGLE,
	
	INITINGDONGLE = 5,
	INITIALIZED,
	
	MODEMREADY,
	
	SIMCARDCHECKING = 8,
	NOSIMCARD,
	SIMCARDOK,
	
	PINCHECKING = 11,
	PININPUT,
	PUKINPUT,
	PINOK,
	PINFAIL,
	
	NETWORKSCANNING = 16,
	NONETWORK,
	NETWORKOK,
	
	CONNECTING3G,
	CONNECTED3G = 21,
	
	CONNECTIONFAIL,

	STARTING3G, /* when "rc 3g start" is running, STARTING3G state. */
};

#define IDENTIFY_TIME		5
#define INITIALLIZING_TIME	25
#define NETWORK_TIME		60

#define DONGLE_INIT				0
#define	DONGLE_SELECT			1
#define DONGLE_CONNECT			2
#define DONGLE_UNKNOWN_ERROR	200
#define	DONGLE_SIM_ERROR		201
#define	DONGLE_SYS_ERROR		202
#define	DONGLE_PIN_UNKNOWN		203
#define	DONGLE_PIN_INPUT		204

#define DONGLE_SIM_INIT			0
#define	DONGLE_SIM				1
#define	DONGLE_USIM				2
#define	DONGLE_SIM_CHECK		3
#define DONGLE_NO_SIM   		255

#define PIN_INIT				0
#define	PIN_READY				1
#define	PIN_INPUT				2
#define	PIN_ERROR       		3
#define	PIN_PAUSE				4
#define	PUK_INPUT				5

#define NETWORK_M_NOSIGNAL		0
#define NETWORK_M_GSM			3
#define NETWORK_M_WCDMA			5
#define NETWORK_M_TD_SCDMA		15

#define NETWORK_SINGAL_NOSIGNAL	0
#define NETWORK_SINGAL_LOW		1
#define NETWORK_SINGAL_MEDIUM	2
#define NETWORK_SINGAL_GOOD		3
#define NETWORK_SINGAL_STRONG	4

int get_hsdpa_wan_status(void);

void get_3g_dataif(char *data_port);
int is_modem_init();
void get_modem_info(char *name, char *value);
int get_status_from_rpt(char *item);
char *id2operator(int id);
int get_dongle_status(void);
int get_3gmodem_status(void);

#include "nvram_var.h"
static inline int is_hsdpa_mode(void)
{
    return (strcmp(nvram_safe_get_r(WAN_ACTYPE), L2_DONGLE_3G) == 0);
}

#endif
