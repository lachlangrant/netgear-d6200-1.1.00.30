#ifndef _FLASH_H_
#define _FLASH_H_

/*sc:
char* sc_get_pin( char* buf, int len);

	Get the {PIN code} string to the buf
	The length of {PIN code} should be 8

Parameters:
	buf: return buf
	len: buf length (include the '\0' char)

Return:
	return buf if success, NULL if fail

*/
#define BUF_LEN_PIN 10	// length of pin =8, +1 ('\0')
char* sc_get_pin( char* buf, int len);



/*sc:
char* sc_get_csn( char* buf, int len);

	Get the {csn} string to the buf
	The max length of {sn/serial no} is 42

Parameters:
	buf: return buf
	len: buf length (include the '\0' char)

Return:
	return buf if success, NULL if fail

*/
#define BUF_LEN_CSN 43
char* sc_get_csn( char* buf, int len);

/*sc:
char* sc_get_sn( char* buf, int len);

	Get the {sn/serial no} string to the buf
	The max length of {sn/serial no} is 13

Parameters:
	buf: return buf
	len: buf length (include the '\0' char)

Return:
	return buf if success, NULL if fail

*/
#define BUF_LEN_SN 14
char* sc_get_sn( char* buf, int len);

/*sc:
char* sc_get_mac( char* buf, int len);

	Get the mac string to the buf
	The max length of {sn/serial no} is 6

Parameters:
	buf: return buf, return mac format: 00:c0:02:11:22:33
	len: buf length (include the '\0' char)

Return:
	return buf if success, NULL if fail

*/
#define BUF_LEN_MAC 17
char* sc_get_mac( char* buf, int len);

/*sc:
int sc_get_mac_if( char *ifname, char mac[6]);

	Get the {mac addr} of the interface {ifname}
	The length of {mac} is 6

Parameters:
	ifname: interface name
	mac: returned mac address, length=6

Return:
	return 0 if success, -1 if fail

*/
int sc_get_mac_if( char *ifname, char mac[6]);



/*sc:
int sc_get_pid(char* buf, int len);

	Get the {PID date} from flash to buf
	The length of {PID date} is 70 // include "sErCoMm"

Parameters:
	buf: return buf
	len: buf length

Return:
	return 0 if success, -1 if fail

*/
int sc_get_pid(char* buf, int len);


struct region_related_s {
    unsigned short region;
    char *wiz_language;
    char *wifi_region;
    char *fw_time_zone;
    /*Add more related values for ADSL gateway*/
    char *encap;
    char *vpi;
    char *vci;
    char *wan_mode;
    char *mtu;
    char *region_code;
};

/*
 * Get DUT Region related struct point
 * Return NULL if error
 */
struct region_related_s *sc_get_region(void);

/* get domain from flash to buf
 * return 0 if success; return -1 if fail
 */
#define BUF_LEN_DOMAIN 3
char* sc_get_domain(char* buf, int len);

/* get Language ID from flash to buf
 * return 0 if success; return -1 if fail
 */
#define BUF_LEN_LANG_ID 3
char* sc_get_country(char* buf, int len);

/* get PCBA SN from flash to buf
 * return 0 if success; return -1 if fail
 */
#define BUF_LEN_PCBA_SN 17
char* sc_get_pcba_sn(char* buf, int len);


/* open mtd block */
int sc_set_close();

/* close mtd block */
int sc_set_open();

/* assign MAC to device
 * return 1 if success; return -1 if fail
 */
int sc_set_mac(char *buf);

/* assign SN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_sn(char *buf);

/* assign CSN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_csn(char *buf);
/* assign PIN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_pin(char *buf);

/* assign domain to device
 * return 1 if success; return -1 if fail
 */
int sc_set_domain(char *buf);

/* assign language ID to device
 * return 1 if success; return -1 if fail
 */
int sc_set_country(char *buf);

/* assign PCBA SN to device
 * return 1 if success; return -1 if fail
 */
int sc_set_pcba_sn(char *buf);

/* assign REGION to device
 * return 1 if success; return -1 if fail
 */
int sc_set_region(char *buf);


#define BUF_LEN_SSID 21
#define BUF_LEN_WPAKEY 65
char* sc_get_def_ssid( char* buf, int len);
char* sc_get_def_wpakey( char* buf, int len);
int sc_set_ssid(char *buf);
int sc_set_wpakey(char *buf);

int region_is_NA();

int region_is_exist();

int region_is_US();


int sc_set_remotescfgmgrmagic(unsigned char *buf);
int sc_clear_remotescfgmgmagic();
int sc_check_remotescfgmgrmagic();
#endif /*_LIBCOMM_SC_H_*/
