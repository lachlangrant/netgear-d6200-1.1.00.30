#ifndef _LIBWL_H_
#define _LIBWL_H_

#define WSC_STATUS_FILE "/tmp/wscd_status"

enum {  PROTOCOL_START=0	,
		PROTOCOL_PBC_OVERLAPPING=1	,
		PROTOCOL_TIMEOUT=2,
		PROTOCOL_SM2=4	,
		PROTOCOL_SM7=5	,
		PROTOCOL_PIN_NUM_ERR=6	,
		PROTOCOL_SUCCESS=7
	 };

/* wps_proc_status value meanings */
typedef enum {
	WPS_INIT = 0,
	WPS_ASSOCIATED, //now it is waiting meanning
	WPS_OK,
	WPS_TIMEOUT,
	WPS_MSG_ERR,
	WPS_SENDM2,
	WPS_SENDM7,
	WPS_MSGDONE,
	WPS_PBCOVERLAP,
	WPS_FIND_PBC_AP,
	WPS_ASSOCIATING
} EWPS_STATE;

struct wlan_info {
    unsigned char w_mac_e[18];
    int wlan_iface; /* 0: 2.4G, 1: 5G, -1: invalid and stands for the last entry */
    int ap_index; /* 0: main SSID, 1,2,3: guest SSIDs */
	char rssi; /* RSSI */
	unsigned int signal; /* signal strength, calculated by RSSI */
	unsigned int link_rate;
};


EWPS_STATE get_wsc_status();
void get_all_assoc_mac(char *assoc_wl_mac, int max_entry);

#endif /* _LIBWL_H_ */
