#ifndef _SCQOS_H_
#define _SCQOS_H_

#define MAX_KBPS        9999
#define ONEM            1024

int get_actual_up_rate(int up_rate, int wan_mtu);
#endif /* _SCQOS_H_ */

