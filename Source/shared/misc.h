#ifndef _G_MISC_H_
#define _G_MISC_H_
/*
 * Please define global defines no including to filenames/if_name/nv_var
 */
#define NETGEAR_SYSLOG(fmt, args...)    syslog(LOG_INFO, fmt, ##args)

#ifdef ANNEXB
#define _PRJ_DIR_       "d6200b"
#define PNPX_HW_ID       "VEN_01f2&DEV_3008&REV_01 VEN_01f2&DEV_7000&SUBSYS_01&REV_01 VEN_01f2&DEV_7000&REV_01"
#define _URI_DIR_   "d6200b"  //used in ca.c 
#else
#define _PRJ_DIR_       "d6200"
#define PNPX_HW_ID       "VEN_01f2&DEV_3007&REV_01 VEN_01f2&DEV_7000&SUBSYS_01&REV_01 VEN_01f2&DEV_7000&REV_01"
#define _URI_DIR_   "d6200"  //used in ca.c 
#endif

#endif /* _G_MISC_H_ */


