#ifndef _SCMISC_H_
#define _SCMISC_H_
char *get_filter_str(char *buf, char token1, char token2);

/*
 * tuple struct for convert from name to value or from value to name
 */
struct tuple_s {
    char *name;
    char *value;
};

char *get_tuple_val(char *name, struct tuple_s *tuples);
char *get_tuple_name(char *value, struct tuple_s *tuples);
int myPipe(char *command, char **output);
int COMMAND(const char *format, ...);

#endif /* _SCMISC_H_ */

