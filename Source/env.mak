# DIR setting

export TARGET_HOME = $(ROOT)

export BINDIR=$(TARGET_DIR)/usr/sbin
export LIBDIR=$(TARGET_DIR)/usr/lib/
export SQUASHDIR=$(ROOT)/bsp/src/router/squashfs
export COMPRESSDIR=$(ROOT)/bsp/src/router/compressed



export KERNEL_DIR = $(TARGET_HOME)/kernel
export KERNEL_SRC=$(KERNEL_DIR)
export KERNEL_INC=$(KERNEL_DIR)/include
export SHARED_DIR=$(ROOT)/shared
export APPS_DIR  = $(ROOT)/apps
export UI_DIR=$(ROOT)/UI
export IMAGE_DIR=$(ROOT)/image
export TARGET_DIR=$(ROOT)/target
export SC_LIBS_DIR=$(APPS_DIR)/sc_libs
export THIRD_PARTY_DIR=$(ROOT)/third_party

export LOADER_DIR = $(TARGET_HOME)/cfe
export CFERAM_DIR = $(TARGET_HOME)/cferam
export DPF_DIR = $(TARGET_HOME)/dpf
export DRIVER_DIR   = $(TARGET_HOME)/driver
export FS_DIR  = $(TARGET_HOME)/target
export FIRMWARE_DIR =$(TARGET_HOME)/tools/makeimage.gv4
export TOOLS_DIR  = $(TARGET_HOME)/tools
export HOSTTOOLS_DIR = $(TARGET_HOME)/../Kernel/bcm963xx/hostTools
export MAKE_BIN_DIR = $(TOOLS_DIR)/make_bin
export FUNC = 00

export ROOTFS = $(FS_DIR)

# filename setting
export ROOT_IMG :=target.squashfs

# platfrom setting
export ARCH=mips
export CROSS_COMPILE=$(CROSS)
export CC=$(CROSS)gcc
export CXX=$(CROSS)g++
export LD=$(CROSS)ld
export AR=$(CROSS)ar
export AS=$(CROSS)as
export RANLIB=$(CROSS)ranlib
export STRIP=$(CROSS)strip
CROSS_DIR = /opt/toolchains/uclibc-crosstools-gcc-4.4.2-1-with-ftw/usr/bin/
TOOLS_DIR = $(ROOT)/tools/
export PATH := $(CROSS_DIR):$(TOOLS_DIR):$(PATH)

#export CFLAGS += -Wno-strict-aliasing


